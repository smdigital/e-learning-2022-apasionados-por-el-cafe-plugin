<?php

/**
 * Fired during plugin activation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Elearning_Apasionados
 * @subpackage Elearning_Apasionados/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Elearning_Apasionados
 * @subpackage Elearning_Apasionados/includes
 * @author     Cristian Torres <catorres@smdigital.com.co>
 */
class Elearning_Apasionados_Activator {

	public static $learners_table_name   		 = 'scorm_learners';
	public static $tests_table_name 	    	   = 'scorm_tests';
	public static $learner_test_table_name 	   = 'scorm_learner_test';
  public static $progress_table_name  		   = 'scorm_progress';
	public static $country_states_table_name   = 'country_states_co';
	public static $country_cities_table_name   = 'country_cities';
  public static $countries_table_name        = 'cn_countries';
  public static $cities_table_name           = 'cn_cities';
  public static $states_table_name           = 'cn_states';


	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		Elearning_Apasionados_Activator::createTables();
		Elearning_Apasionados_Activator::fillData();
		Elearning_Apasionados_Activator::createPages();

	}


	public static function createTables() {
		
		global $wpdb;
		$charset_collate = $wpdb->get_charset_collate();

		$learners_table = $wpdb->prefix . Elearning_Apasionados_Activator::$learners_table_name;
		$tests_table = $wpdb->prefix . Elearning_Apasionados_Activator::$tests_table_name;
		$learner_test_table = $wpdb->prefix . Elearning_Apasionados_Activator::$learner_test_table_name;
		$progress_table = $wpdb->prefix . Elearning_Apasionados_Activator::$progress_table_name;
		$country_states_table = $wpdb->prefix . Elearning_Apasionados_Activator::$country_states_table_name;
		$country_cities_table = $wpdb->prefix . Elearning_Apasionados_Activator::$country_cities_table_name;
	
		$sql_learners = "CREATE TABLE IF NOT EXISTS $learners_table (
								id int(10) unsigned NOT NULL AUTO_INCREMENT,
								name varchar(100) DEFAULT NULL,
								email varchar(100) NOT NULL,
								password varchar(255) NOT NULL,
								created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
								updated_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
								PRIMARY KEY (id)
		) $charset_collate;";
		
		$sql_tests = "CREATE TABLE IF NOT EXISTS $tests_table (
							id int(10) unsigned NOT NULL AUTO_INCREMENT,
							name varchar(100) NOT NULL,
							created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
							updated_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
							PRIMARY KEY (id)
		) $charset_collate;";

		$sql_learner_test = "CREATE TABLE IF NOT EXISTS $learner_test_table (
										id int(10) unsigned NOT NULL AUTO_INCREMENT,
										learner_id int(10) unsigned NOT NULL,
										test_id int(10) unsigned NOT NULL,
										note double(8,2) NOT NULL DEFAULT '0.00',
										created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
										updated_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
										PRIMARY KEY (id),
										KEY scorm_learner_test_learner_id_foreign (learner_id),
										KEY scorm_learner_test_test_id_foreign (test_id),
										CONSTRAINT scorm_learner_test_learner_id_foreign FOREIGN KEY (learner_id) REFERENCES $learners_table (id),
										CONSTRAINT scorm_learner_test_test_id_foreign FOREIGN KEY (test_id) REFERENCES $tests_table (id)
		) $charset_collate;";

		$sql_progress = "CREATE TABLE IF NOT EXISTS $progress_table (
									id int(10) unsigned NOT NULL AUTO_INCREMENT,
									learner_id int(10) unsigned NOT NULL,
									lesson int(11) NOT NULL DEFAULT '0',
									section int(11) NOT NULL DEFAULT '0',
									created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
									updated_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
									PRIMARY KEY (id),
									KEY scorm_progress_learner_id_foreign (learner_id),
									CONSTRAINT scorm_progress_learner_id_foreign FOREIGN KEY (learner_id) REFERENCES $learners_table (id)
		) $charset_collate;";

		$sql_country_states = "CREATE TABLE IF NOT EXISTS $country_states_table (
										id int(10) unsigned NOT NULL AUTO_INCREMENT,
										name varchar(255) NOT NULL,
										PRIMARY KEY (id)
		) $charset_collate;";

		$sql_country_cities = "CREATE TABLE IF NOT EXISTS $country_cities_table (
										   	id int(10) unsigned NOT NULL AUTO_INCREMENT,
										   name varchar(255) NOT NULL,
										    state varchar(255) NOT NULL,
										   state_id varchar(255) NOT NULL,
										   PRIMARY KEY (id)
		) $charset_collate;";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql_learners );
		dbDelta( $sql_tests );
		dbDelta( $sql_learner_test );
		dbDelta( $sql_progress );
		dbDelta( $sql_country_states );
		dbDelta( $sql_country_cities );
	}

	private static function fillData() {
		
		global $wpdb;

		$country_states_table = $wpdb->prefix . Elearning_Apasionados_Activator::$country_states_table_name;
		$country_cities_table = $wpdb->prefix . Elearning_Apasionados_Activator::$country_cities_table_name;
    
    /**
     * New tables countries, cities, states
    */
    $countries_table = $wpdb->prefix . Elearning_Apasionados_Activator::$countries_table_name;
    $states_table = $wpdb->prefix . Elearning_Apasionados_Activator::$states_table_name;
    $cities_table = $wpdb->prefix . Elearning_Apasionados_Activator::$cities_table_name;


    $country_data = $wpdb->get_results($wpdb->prepare( "SELECT count(*) as 'total' FROM {$countries_table}", OBJECT));
    $states_data = $wpdb->get_results($wpdb->prepare( "SELECT count(*) as 'total' FROM {$states_table}", OBJECT));
		$cities_data = $wpdb->get_results($wpdb->prepare( "SELECT count(*) as 'total' FROM {$cities_table}", OBJECT));
    /******************** */
    
    
		$country_states_data = $wpdb->get_results($wpdb->prepare( "SELECT count(*) as 'total' FROM {$country_states_table}", OBJECT));
		$country_cities_data = $wpdb->get_results($wpdb->prepare( "SELECT count(*) as 'total' FROM {$country_cities_table}", OBJECT));
		
		if(  (int)$country_states_data[0]->total == 0 ) {
			$states = array(
				array('name' => "Amazonas"),
				array('name' => "Antioquia"),
				array('name' => "Arauca"),
				array('name' => "Atlantico"),
				array('name' => "Bogota"),
				array('name' => "Bolivar"),
				array('name' => "Boyaca"),
				array('name' => "Caldas"),
				array('name' => "Caqueta"),
				array('name' => "Casanare"),
				array('name' => "Cauca"),
				array('name' => "Cesar"),
				array('name' => "Choco"),
				array('name' => "Cordoba"),
				array('name' => "Cundinamarca"),
				array('name' => "Guainia"),
				array('name' => "Guaviare"),
				array('name' => "Huila"),
				array('name' => "La Guajira"),
				array('name' => "Magdalena"),
				array('name' => "Meta"),
				array('name' => "Narino"),
				array('name' => "Norte de Santander"),
				array('name' => "Putumayo"),
				array('name' => "Quindio"),
				array('name' => "Risaralda"),
				array('name' => "San Andres y Providencia"),
				array('name' => "Santander"),
				array('name' => "Sucre"),
				array('name' => "Tolima"),
				array('name' => "Valle del Cauca"),
				array('name' => "Vaupes"),
				array('name' => "Vichada"),
			);
		
			foreach ($states as $state) {
				$wpdb->insert($country_states_table, $state, array('%s'));
			}
		}

		if( (int)$country_cities_data[0]->total == 0 ) {
			$cities = array(
				array('name' => "Leticia",'state_id' => 1, 'state' => 1),
				array('name' => "Puerto Narino",'state_id' => 1, 'state' => 1),
				array('name' => "Abejorral",'state_id' => 2, 'state' => 2),
				array('name' => "Abriaqui",'state_id' => 2, 'state' => 2),
				array('name' => "Alejandria",'state_id' => 2, 'state' => 2),
				array('name' => "Amaga",'state_id' => 2, 'state' => 2),
				array('name' => "Amalfi",'state_id' => 2, 'state' => 2),
				array('name' => "Andes",'state_id' => 2, 'state' => 2),
				array('name' => "Angelopolis",'state_id' => 2, 'state' => 2),
				array('name' => "Angostura",'state_id' => 2, 'state' => 2),
				array('name' => "Anori",'state_id' => 2, 'state' => 2),
				array('name' => "Antioquia",'state_id' => 2, 'state' => 2),
				array('name' => "Anza",'state_id' => 2, 'state' => 2),
				array('name' => "Apartado",'state_id' => 2, 'state' => 2),
				array('name' => "Arboletes",'state_id' => 2, 'state' => 2),
				array('name' => "Argelia",'state_id' => 2, 'state' => 2),
				array('name' => "Armenia",'state_id' => 2, 'state' => 2),
				array('name' => "Barbosa",'state_id' => 2, 'state' => 2),
				array('name' => "Bello",'state_id' => 2, 'state' => 2),
				array('name' => "Belmira",'state_id' => 2, 'state' => 2),
				array('name' => "Betania",'state_id' => 2, 'state' => 2),
				array('name' => "Betulia",'state_id' => 2, 'state' => 2),
				array('name' => "Bolivar",'state_id' => 2, 'state' => 2),
				array('name' => "Briceno",'state_id' => 2, 'state' => 2),
				array('name' => "Buritica",'state_id' => 2, 'state' => 2),
				array('name' => "Caceres",'state_id' => 2, 'state' => 2),
				array('name' => "Caicedo",'state_id' => 2, 'state' => 2),
				array('name' => "Caldas",'state_id' => 2, 'state' => 2),
				array('name' => "Campamento",'state_id' => 2, 'state' => 2),
				array('name' => "Canasgordas",'state_id' => 2, 'state' => 2),
				array('name' => "Caracoli",'state_id' => 2, 'state' => 2),
				array('name' => "Caramanta",'state_id' => 2, 'state' => 2),
				array('name' => "Carepa",'state_id' => 2, 'state' => 2),
				array('name' => "Carmen de Viboral",'state_id' => 2, 'state' => 2),
				array('name' => "Carolina",'state_id' => 2, 'state' => 2),
				array('name' => "Caucasia",'state_id' => 2, 'state' => 2),
				array('name' => "Chigorodo",'state_id' => 2, 'state' => 2),
				array('name' => "Cisneros",'state_id' => 2, 'state' => 2),
				array('name' => "Cocorna",'state_id' => 2, 'state' => 2),
				array('name' => "Concepcion",'state_id' => 2, 'state' => 2),
				array('name' => "Concordia",'state_id' => 2, 'state' => 2),
				array('name' => "Copacabana",'state_id' => 2, 'state' => 2),
				array('name' => "Dabeiba",'state_id' => 2, 'state' => 2),
				array('name' => "Don Matias",'state_id' => 2, 'state' => 2),
				array('name' => "Ebejico",'state_id' => 2, 'state' => 2),
				array('name' => "El Bagre",'state_id' => 2, 'state' => 2),
				array('name' => "Entrerrios",'state_id' => 2, 'state' => 2),
				array('name' => "Envigado",'state_id' => 2, 'state' => 2),
				array('name' => "Fredonia",'state_id' => 2, 'state' => 2),
				array('name' => "Frontino",'state_id' => 2, 'state' => 2),
				array('name' => "Giraldo",'state_id' => 2, 'state' => 2),
				array('name' => "Girardota",'state_id' => 2, 'state' => 2),
				array('name' => "Gomez Plata",'state_id' => 2, 'state' => 2),
				array('name' => "Granada",'state_id' => 2, 'state' => 2),
				array('name' => "Guadalupe",'state_id' => 2, 'state' => 2),
				array('name' => "Guarne",'state_id' => 2, 'state' => 2),
				array('name' => "Guatape",'state_id' => 2, 'state' => 2),
				array('name' => "Heliconia",'state_id' => 2, 'state' => 2),
				array('name' => "Hispania",'state_id' => 2, 'state' => 2),
				array('name' => "Itagui",'state_id' => 2, 'state' => 2),
				array('name' => "Ituango",'state_id' => 2, 'state' => 2),
				array('name' => "Jardin",'state_id' => 2, 'state' => 2),
				array('name' => "Jerico",'state_id' => 2, 'state' => 2),
				array('name' => "La Ceja",'state_id' => 2, 'state' => 2),
				array('name' => "La Estrella",'state_id' => 2, 'state' => 2),
				array('name' => "La Pintada",'state_id' => 2, 'state' => 2),
				array('name' => "La Union",'state_id' => 2, 'state' => 2),
				array('name' => "Liborina",'state_id' => 2, 'state' => 2),
				array('name' => "Maceo",'state_id' => 2, 'state' => 2),
				array('name' => "Marinilla",'state_id' => 2, 'state' => 2),
				array('name' => "Medellin",'state_id' => 2, 'state' => 2),
				array('name' => "Montebello",'state_id' => 2, 'state' => 2),
				array('name' => "Murindo",'state_id' => 2, 'state' => 2),
				array('name' => "Mutata",'state_id' => 2, 'state' => 2),
				array('name' => "Narino",'state_id' => 2, 'state' => 2),
				array('name' => "Nechi",'state_id' => 2, 'state' => 2),
				array('name' => "Necocli",'state_id' => 2, 'state' => 2),
				array('name' => "Olaya",'state_id' => 2, 'state' => 2),
				array('name' => "Penol",'state_id' => 2, 'state' => 2),
				array('name' => "Peque",'state_id' => 2, 'state' => 2),
				array('name' => "Pueblorrico",'state_id' => 2, 'state' => 2),
				array('name' => "Puerto Berrio",'state_id' => 2, 'state' => 2),
				array('name' => "Puerto Nare",'state_id' => 2, 'state' => 2),
				array('name' => "Puerto Triunfo",'state_id' => 2, 'state' => 2),
				array('name' => "Remedios",'state_id' => 2, 'state' => 2),
				array('name' => "Retiro",'state_id' => 2, 'state' => 2),
				array('name' => "Rionegro",'state_id' => 2, 'state' => 2),
				array('name' => "Sabanalarga",'state_id' => 2, 'state' => 2),
				array('name' => "Sabaneta",'state_id' => 2, 'state' => 2),
				array('name' => "Salgar",'state_id' => 2, 'state' => 2),
				array('name' => "San Andres",'state_id' => 2, 'state' => 2),
				array('name' => "San Carlos",'state_id' => 2, 'state' => 2),
				array('name' => "San Francisco",'state_id' => 2, 'state' => 2),
				array('name' => "San Jeronimo",'state_id' => 2, 'state' => 2),
				array('name' => "San Jose de la Montana",'state_id' => 2, 'state' => 2),
				array('name' => "San Juan de Uraba",'state_id' => 2, 'state' => 2),
				array('name' => "San Luis",'state_id' => 2, 'state' => 2),
				array('name' => "San Pedro",'state_id' => 2, 'state' => 2),
				array('name' => "San Pedro de Uraba",'state_id' => 2, 'state' => 2),
				array('name' => "San Rafael",'state_id' => 2, 'state' => 2),
				array('name' => "San Roque",'state_id' => 2, 'state' => 2),
				array('name' => "San Vicente",'state_id' => 2, 'state' => 2),
				array('name' => "Santa Barbara",'state_id' => 2, 'state' => 2),
				array('name' => "Santa Rosa de Osos",'state_id' => 2, 'state' => 2),
				array('name' => "Santo Domingo",'state_id' => 2, 'state' => 2),
				array('name' => "Santuario",'state_id' => 2, 'state' => 2),
				array('name' => "Segovia",'state_id' => 2, 'state' => 2),
				array('name' => "Sonson",'state_id' => 2, 'state' => 2),
				array('name' => "Sopetran",'state_id' => 2, 'state' => 2),
				array('name' => "Tamesis",'state_id' => 2, 'state' => 2),
				array('name' => "Taraza",'state_id' => 2, 'state' => 2),
				array('name' => "Tarso",'state_id' => 2, 'state' => 2),
				array('name' => "Titiribi",'state_id' => 2, 'state' => 2),
				array('name' => "Toledo",'state_id' => 2, 'state' => 2),
				array('name' => "Turbo",'state_id' => 2, 'state' => 2),
				array('name' => "Uramita",'state_id' => 2, 'state' => 2),
				array('name' => "Urrao",'state_id' => 2, 'state' => 2),
				array('name' => "Valdivia",'state_id' => 2, 'state' => 2),
				array('name' => "Valparaiso",'state_id' => 2, 'state' => 2),
				array('name' => "Vegachi",'state_id' => 2, 'state' => 2),
				array('name' => "Venecia",'state_id' => 2, 'state' => 2),
				array('name' => "Vigia del Fuerte",'state_id' => 2, 'state' => 2),
				array('name' => "Yali",'state_id' => 2, 'state' => 2),
				array('name' => "Yarumal",'state_id' => 2, 'state' => 2),
				array('name' => "Yolombo",'state_id' => 2, 'state' => 2),
				array('name' => "Yondo",'state_id' => 2, 'state' => 2),
				array('name' => "Zaragoza",'state_id' => 2, 'state' => 2),
				array('name' => "Arauca",'state_id' => 3, 'state' => 3),
				array('name' => "Arauquita",'state_id' => 3, 'state' => 3),
				array('name' => "Cravo Norte",'state_id' => 3, 'state' => 3),
				array('name' => "Fortul",'state_id' => 3, 'state' => 3),
				array('name' => "Puerto Rondon",'state_id' => 3, 'state' => 3),
				array('name' => "Saravena",'state_id' => 3, 'state' => 3),
				array('name' => "Tame",'state_id' => 3, 'state' => 3),
				array('name' => "Baranoa",'state_id' => 4, 'state' => 4),
				array('name' => "Barranquilla",'state_id' => 4, 'state' => 4),
				array('name' => "Campo de la Cruz",'state_id' => 4, 'state' => 4),
				array('name' => "Candelaria",'state_id' => 4, 'state' => 4),
				array('name' => "Galapa",'state_id' => 4, 'state' => 4),
				array('name' => "Juan de Acosta",'state_id' => 4, 'state' => 4),
				array('name' => "Luruaco",'state_id' => 4, 'state' => 4),
				array('name' => "Malambo",'state_id' => 4, 'state' => 4),
				array('name' => "Manati",'state_id' => 4, 'state' => 4),
				array('name' => "Palmar de Varela",'state_id' => 4, 'state' => 4),
				array('name' => "Piojo",'state_id' => 4, 'state' => 4),
				array('name' => "Polo Nuevo",'state_id' => 4, 'state' => 4),
				array('name' => "Ponedera",'state_id' => 4, 'state' => 4),
				array('name' => "Puerto Colombia",'state_id' => 4, 'state' => 4),
				array('name' => "Repelon",'state_id' => 4, 'state' => 4),
				array('name' => "Sabanagrande",'state_id' => 4, 'state' => 4),
				array('name' => "Sabanalarga",'state_id' => 4, 'state' => 4),
				array('name' => "Santa Lucia",'state_id' => 4, 'state' => 4),
				array('name' => "Santo Tomas",'state_id' => 4, 'state' => 4),
				array('name' => "Soledad",'state_id' => 4, 'state' => 4),
				array('name' => "Suan",'state_id' => 4, 'state' => 4),
				array('name' => "Tubara",'state_id' => 4, 'state' => 4),
				array('name' => "Usiacuri",'state_id' => 4, 'state' => 4),
				array('name' => "Bogota",'state_id' => 5, 'state' => 5),
				array('name' => "Achi",'state_id' => 6, 'state' => 6),
				array('name' => "Altos del Rosario",'state_id' => 6, 'state' => 6),
				array('name' => "Arenal",'state_id' => 6, 'state' => 6),
				array('name' => "Arjona",'state_id' => 6, 'state' => 6),
				array('name' => "Arroyohondo",'state_id' => 6, 'state' => 6),
				array('name' => "Barranco de Loba",'state_id' => 6, 'state' => 6),
				array('name' => "Calamar",'state_id' => 6, 'state' => 6),
				array('name' => "Cantagallo",'state_id' => 6, 'state' => 6),
				array('name' => "Cartagena",'state_id' => 6, 'state' => 6),
				array('name' => "Cicuco",'state_id' => 6, 'state' => 6),
				array('name' => "Clemencia",'state_id' => 6, 'state' => 6),
				array('name' => "Cordoba",'state_id' => 6, 'state' => 6),
				array('name' => "El Carmen de Bolivar",'state_id' => 6, 'state' => 6),
				array('name' => "El Guamo",'state_id' => 6, 'state' => 6),
				array('name' => "El Penon",'state_id' => 6, 'state' => 6),
				array('name' => "Hatillo de Loba",'state_id' => 6, 'state' => 6),
				array('name' => "Magangue",'state_id' => 6, 'state' => 6),
				array('name' => "Mahates",'state_id' => 6, 'state' => 6),
				array('name' => "Margarita",'state_id' => 6, 'state' => 6),
				array('name' => "Maria la Baja",'state_id' => 6, 'state' => 6),
				array('name' => "Mompos",'state_id' => 6, 'state' => 6),
				array('name' => "Montecristo",'state_id' => 6, 'state' => 6),
				array('name' => "Morales",'state_id' => 6, 'state' => 6),
				array('name' => "Pinillos",'state_id' => 6, 'state' => 6),
				array('name' => "Regidor",'state_id' => 6, 'state' => 6),
				array('name' => "Rio Viejo",'state_id' => 6, 'state' => 6),
				array('name' => "San Cristobal",'state_id' => 6, 'state' => 6),
				array('name' => "San Estanislao",'state_id' => 6, 'state' => 6),
				array('name' => "San Fernando",'state_id' => 6, 'state' => 6),
				array('name' => "San Jacinto",'state_id' => 6, 'state' => 6),
				array('name' => "San Jacinto del Cauca",'state_id' => 6, 'state' => 6),
				array('name' => "San Juan Nepomuceno",'state_id' => 6, 'state' => 6),
				array('name' => "San Martin de Loba",'state_id' => 6, 'state' => 6),
				array('name' => "San Pablo",'state_id' => 6, 'state' => 6),
				array('name' => "Santa Catalina",'state_id' => 6, 'state' => 6),
				array('name' => "Santa Rosa",'state_id' => 6, 'state' => 6),
				array('name' => "Santa Rosa del Sur",'state_id' => 6, 'state' => 6),
				array('name' => "Simiti",'state_id' => 6, 'state' => 6),
				array('name' => "Soplaviento",'state_id' => 6, 'state' => 6),
				array('name' => "Talaigua Nuevo",'state_id' => 6, 'state' => 6),
				array('name' => "Tiquisio",'state_id' => 6, 'state' => 6),
				array('name' => "Turbaco",'state_id' => 6, 'state' => 6),
				array('name' => "Turbana",'state_id' => 6, 'state' => 6),
				array('name' => "Villanueva",'state_id' => 6, 'state' => 6),
				array('name' => "Zambrano",'state_id' => 6, 'state' => 6),
				array('name' => "Almeida",'state_id' => 7, 'state' => 7),
				array('name' => "Aquitania",'state_id' => 7, 'state' => 7),
				array('name' => "Arcabuco",'state_id' => 7, 'state' => 7),
				array('name' => "Belen",'state_id' => 7, 'state' => 7),
				array('name' => "Berbeo",'state_id' => 7, 'state' => 7),
				array('name' => "Beteitiva",'state_id' => 7, 'state' => 7),
				array('name' => "Boavita",'state_id' => 7, 'state' => 7),
				array('name' => "Boyaca",'state_id' => 7, 'state' => 7),
				array('name' => "Briceno",'state_id' => 7, 'state' => 7),
				array('name' => "Buenavista",'state_id' => 7, 'state' => 7),
				array('name' => "Busbanza",'state_id' => 7, 'state' => 7),
				array('name' => "Caldas",'state_id' => 7, 'state' => 7),
				array('name' => "Campohermoso",'state_id' => 7, 'state' => 7),
				array('name' => "Cerinza",'state_id' => 7, 'state' => 7),
				array('name' => "Chinavita",'state_id' => 7, 'state' => 7),
				array('name' => "Chiquinquira",'state_id' => 7, 'state' => 7),
				array('name' => "Chiquiza",'state_id' => 7, 'state' => 7),
				array('name' => "Chiscas",'state_id' => 7, 'state' => 7),
				array('name' => "Chita",'state_id' => 7, 'state' => 7),
				array('name' => "Chitaraque",'state_id' => 7, 'state' => 7),
				array('name' => "Chivata",'state_id' => 7, 'state' => 7),
				array('name' => "Chivor",'state_id' => 7, 'state' => 7),
				array('name' => "Cienega",'state_id' => 7, 'state' => 7),
				array('name' => "Combita",'state_id' => 7, 'state' => 7),
				array('name' => "Coper",'state_id' => 7, 'state' => 7),
				array('name' => "Corrales",'state_id' => 7, 'state' => 7),
				array('name' => "Covarachia",'state_id' => 7, 'state' => 7),
				array('name' => "Cubara",'state_id' => 7, 'state' => 7),
				array('name' => "Cucaita",'state_id' => 7, 'state' => 7),
				array('name' => "Cuitiva",'state_id' => 7, 'state' => 7),
				array('name' => "Duitama",'state_id' => 7, 'state' => 7),
				array('name' => "El Cocuy",'state_id' => 7, 'state' => 7),
				array('name' => "El Espino",'state_id' => 7, 'state' => 7),
				array('name' => "Firavitoba",'state_id' => 7, 'state' => 7),
				array('name' => "Floresta",'state_id' => 7, 'state' => 7),
				array('name' => "Gachantiva",'state_id' => 7, 'state' => 7),
				array('name' => "Gameza",'state_id' => 7, 'state' => 7),
				array('name' => "Garagoa",'state_id' => 7, 'state' => 7),
				array('name' => "Guacamayas",'state_id' => 7, 'state' => 7),
				array('name' => "Guateque",'state_id' => 7, 'state' => 7),
				array('name' => "Guayata",'state_id' => 7, 'state' => 7),
				array('name' => "Guican",'state_id' => 7, 'state' => 7),
				array('name' => "Iza",'state_id' => 7, 'state' => 7),
				array('name' => "Jenesano",'state_id' => 7, 'state' => 7),
				array('name' => "Jerico",'state_id' => 7, 'state' => 7),
				array('name' => "La Capilla",'state_id' => 7, 'state' => 7),
				array('name' => "La Uvita",'state_id' => 7, 'state' => 7),
				array('name' => "La Victoria",'state_id' => 7, 'state' => 7),
				array('name' => "Labranzagrande",'state_id' => 7, 'state' => 7),
				array('name' => "Leiva",'state_id' => 7, 'state' => 7),
				array('name' => "Macanal",'state_id' => 7, 'state' => 7),
				array('name' => "Maripi",'state_id' => 7, 'state' => 7),
				array('name' => "Miraflores",'state_id' => 7, 'state' => 7),
				array('name' => "Mongua",'state_id' => 7, 'state' => 7),
				array('name' => "Mongui",'state_id' => 7, 'state' => 7),
				array('name' => "Moniquira",'state_id' => 7, 'state' => 7),
				array('name' => "Motavita",'state_id' => 7, 'state' => 7),
				array('name' => "Muzo",'state_id' => 7, 'state' => 7),
				array('name' => "Nobsa",'state_id' => 7, 'state' => 7),
				array('name' => "Nuevo Colon",'state_id' => 7, 'state' => 7),
				array('name' => "Oicata",'state_id' => 7, 'state' => 7),
				array('name' => "Otanche",'state_id' => 7, 'state' => 7),
				array('name' => "Pachavita",'state_id' => 7, 'state' => 7),
				array('name' => "Paez",'state_id' => 7, 'state' => 7),
				array('name' => "Paipa",'state_id' => 7, 'state' => 7),
				array('name' => "Pajarito",'state_id' => 7, 'state' => 7),
				array('name' => "Panqueba",'state_id' => 7, 'state' => 7),
				array('name' => "Pauna",'state_id' => 7, 'state' => 7),
				array('name' => "Paya",'state_id' => 7, 'state' => 7),
				array('name' => "Paz del Rio",'state_id' => 7, 'state' => 7),
				array('name' => "Pesca",'state_id' => 7, 'state' => 7),
				array('name' => "Pisba",'state_id' => 7, 'state' => 7),
				array('name' => "Puerto Boyaca",'state_id' => 7, 'state' => 7),
				array('name' => "Quipama",'state_id' => 7, 'state' => 7),
				array('name' => "Ramiriqui",'state_id' => 7, 'state' => 7),
				array('name' => "Raquira",'state_id' => 7, 'state' => 7),
				array('name' => "Rondon",'state_id' => 7, 'state' => 7),
				array('name' => "Saboya",'state_id' => 7, 'state' => 7),
				array('name' => "Sachica",'state_id' => 7, 'state' => 7),
				array('name' => "Samaca",'state_id' => 7, 'state' => 7),
				array('name' => "San Eduardo",'state_id' => 7, 'state' => 7),
				array('name' => "San Jose de Pare",'state_id' => 7, 'state' => 7),
				array('name' => "San Luis de Gaceno",'state_id' => 7, 'state' => 7),
				array('name' => "San Mateo",'state_id' => 7, 'state' => 7),
				array('name' => "San Miguel de Sema",'state_id' => 7, 'state' => 7),
				array('name' => "San Pablo de Borbur",'state_id' => 7, 'state' => 7),
				array('name' => "Santa Maria",'state_id' => 7, 'state' => 7),
				array('name' => "Santa Rosa de Viterbo",'state_id' => 7, 'state' => 7),
				array('name' => "Santa Sofia",'state_id' => 7, 'state' => 7),
				array('name' => "Santana",'state_id' => 7, 'state' => 7),
				array('name' => "Sativanorte",'state_id' => 7, 'state' => 7),
				array('name' => "Sativasur",'state_id' => 7, 'state' => 7),
				array('name' => "Siachoque",'state_id' => 7, 'state' => 7),
				array('name' => "Soata",'state_id' => 7, 'state' => 7),
				array('name' => "Socha",'state_id' => 7, 'state' => 7),
				array('name' => "Socota",'state_id' => 7, 'state' => 7),
				array('name' => "Sogamoso",'state_id' => 7, 'state' => 7),
				array('name' => "Somondoco",'state_id' => 7, 'state' => 7),
				array('name' => "Sora",'state_id' => 7, 'state' => 7),
				array('name' => "Soraca",'state_id' => 7, 'state' => 7),
				array('name' => "Sotaquira",'state_id' => 7, 'state' => 7),
				array('name' => "Susacon",'state_id' => 7, 'state' => 7),
				array('name' => "Sutamarchan",'state_id' => 7, 'state' => 7),
				array('name' => "Sutatenza",'state_id' => 7, 'state' => 7),
				array('name' => "Tasco",'state_id' => 7, 'state' => 7),
				array('name' => "Tenza",'state_id' => 7, 'state' => 7),
				array('name' => "Tibana",'state_id' => 7, 'state' => 7),
				array('name' => "Tibasosa",'state_id' => 7, 'state' => 7),
				array('name' => "Tinjaca",'state_id' => 7, 'state' => 7),
				array('name' => "Tipacoque",'state_id' => 7, 'state' => 7),
				array('name' => "Toca",'state_id' => 7, 'state' => 7),
				array('name' => "Togui",'state_id' => 7, 'state' => 7),
				array('name' => "Topaga",'state_id' => 7, 'state' => 7),
				array('name' => "Tota",'state_id' => 7, 'state' => 7),
				array('name' => "Tunja",'state_id' => 7, 'state' => 7),
				array('name' => "Tunungua",'state_id' => 7, 'state' => 7),
				array('name' => "Turmeque",'state_id' => 7, 'state' => 7),
				array('name' => "Tuta",'state_id' => 7, 'state' => 7),
				array('name' => "Tutasa",'state_id' => 7, 'state' => 7),
				array('name' => "Umbita",'state_id' => 7, 'state' => 7),
				array('name' => "Ventaquemada",'state_id' => 7, 'state' => 7),
				array('name' => "Viracacha",'state_id' => 7, 'state' => 7),
				array('name' => "Zetaquira",'state_id' => 7, 'state' => 7),
				array('name' => "Aguadas",'state_id' => 8, 'state' => 8),
				array('name' => "Anserma",'state_id' => 8, 'state' => 8),
				array('name' => "Aranzazu",'state_id' => 8, 'state' => 8),
				array('name' => "Belalcazar",'state_id' => 8, 'state' => 8),
				array('name' => "Chinchina",'state_id' => 8, 'state' => 8),
				array('name' => "Filadelfia",'state_id' => 8, 'state' => 8),
				array('name' => "La Dorada",'state_id' => 8, 'state' => 8),
				array('name' => "La Merced",'state_id' => 8, 'state' => 8),
				array('name' => "Manizales",'state_id' => 8, 'state' => 8),
				array('name' => "Manzanares",'state_id' => 8, 'state' => 8),
				array('name' => "Marmato",'state_id' => 8, 'state' => 8),
				array('name' => "Marquetalia",'state_id' => 8, 'state' => 8),
				array('name' => "Marulanda",'state_id' => 8, 'state' => 8),
				array('name' => "Neira",'state_id' => 8, 'state' => 8),
				array('name' => "Norcasia",'state_id' => 8, 'state' => 8),
				array('name' => "Pacora",'state_id' => 8, 'state' => 8),
				array('name' => "Palestina",'state_id' => 8, 'state' => 8),
				array('name' => "Pensilvania",'state_id' => 8, 'state' => 8),
				array('name' => "Riosucio",'state_id' => 8, 'state' => 8),
				array('name' => "Risaralda",'state_id' => 8, 'state' => 8),
				array('name' => "Salamina",'state_id' => 8, 'state' => 8),
				array('name' => "Samana",'state_id' => 8, 'state' => 8),
				array('name' => "San Jose",'state_id' => 8, 'state' => 8),
				array('name' => "Supia",'state_id' => 8, 'state' => 8),
				array('name' => "Victoria",'state_id' => 8, 'state' => 8),
				array('name' => "Villamaria",'state_id' => 8, 'state' => 8),
				array('name' => "Viterbo",'state_id' => 8, 'state' => 8),
				array('name' => "Albania",'state_id' => 9, 'state' => 9),
				array('name' => "Belen Andaquies",'state_id' => 9, 'state' => 9),
				array('name' => "Cartagena del Chaira",'state_id' => 9, 'state' => 9),
				array('name' => "Curillo",'state_id' => 9, 'state' => 9),
				array('name' => "El Doncello",'state_id' => 9, 'state' => 9),
				array('name' => "El Paujil",'state_id' => 9, 'state' => 9),
				array('name' => "Florencia",'state_id' => 9, 'state' => 9),
				array('name' => "La Montanita",'state_id' => 9, 'state' => 9),
				array('name' => "Milan",'state_id' => 9, 'state' => 9),
				array('name' => "Morelia",'state_id' => 9, 'state' => 9),
				array('name' => "Puerto Rico",'state_id' => 9, 'state' => 9),
				array('name' => "San Jose de Fragua",'state_id' => 9, 'state' => 9),
				array('name' => "San Vicente del Caguan",'state_id' => 9, 'state' => 9),
				array('name' => "Solano",'state_id' => 9, 'state' => 9),
				array('name' => "Solita",'state_id' => 9, 'state' => 9),
				array('name' => "Valparaiso",'state_id' => 9, 'state' => 9),
				array('name' => "Aguazul",'state_id' => 10, 'state' => 10),
				array('name' => "Chameza",'state_id' => 10, 'state' => 10),
				array('name' => "Hato Corozal",'state_id' => 10, 'state' => 10),
				array('name' => "La Salina",'state_id' => 10, 'state' => 10),
				array('name' => "Mani",'state_id' => 10, 'state' => 10),
				array('name' => "Monterrey",'state_id' => 10, 'state' => 10),
				array('name' => "Nunchia",'state_id' => 10, 'state' => 10),
				array('name' => "Orocue",'state_id' => 10, 'state' => 10),
				array('name' => "Paz de Ariporo",'state_id' => 10, 'state' => 10),
				array('name' => "Pore",'state_id' => 10, 'state' => 10),
				array('name' => "Recetor",'state_id' => 10, 'state' => 10),
				array('name' => "Sabanalarga",'state_id' => 10, 'state' => 10),
				array('name' => "Sacama",'state_id' => 10, 'state' => 10),
				array('name' => "San Luis de Palenque",'state_id' => 10, 'state' => 10),
				array('name' => "Tamara",'state_id' => 10, 'state' => 10),
				array('name' => "Tauramena",'state_id' => 10, 'state' => 10),
				array('name' => "Trinidad",'state_id' => 10, 'state' => 10),
				array('name' => "Villanueva",'state_id' => 10, 'state' => 10),
				array('name' => "Yopal",'state_id' => 10, 'state' => 10),
				array('name' => "Almaguer",'state_id' => 11, 'state' => 11),
				array('name' => "Argelia",'state_id' => 11, 'state' => 11),
				array('name' => "Balboa",'state_id' => 11, 'state' => 11),
				array('name' => "Bolivar",'state_id' => 11, 'state' => 11),
				array('name' => "Buenos Aires",'state_id' => 11, 'state' => 11),
				array('name' => "Cajibio",'state_id' => 11, 'state' => 11),
				array('name' => "Caldono",'state_id' => 11, 'state' => 11),
				array('name' => "Caloto",'state_id' => 11, 'state' => 11),
				array('name' => "Corinto",'state_id' => 11, 'state' => 11),
				array('name' => "El Bordo",'state_id' => 11, 'state' => 11),
				array('name' => "El Tambo",'state_id' => 11, 'state' => 11),
				array('name' => "Florencia",'state_id' => 11, 'state' => 11),
				array('name' => "Guapi",'state_id' => 11, 'state' => 11),
				array('name' => "Inza",'state_id' => 11, 'state' => 11),
				array('name' => "Jambalo",'state_id' => 11, 'state' => 11),
				array('name' => "La Sierra",'state_id' => 11, 'state' => 11),
				array('name' => "La Vega",'state_id' => 11, 'state' => 11),
				array('name' => "Lopez",'state_id' => 11, 'state' => 11),
				array('name' => "Mercaderes",'state_id' => 11, 'state' => 11),
				array('name' => "Miranda",'state_id' => 11, 'state' => 11),
				array('name' => "Morales",'state_id' => 11, 'state' => 11),
				array('name' => "Padilla",'state_id' => 11, 'state' => 11),
				array('name' => "Paez",'state_id' => 11, 'state' => 11),
				array('name' => "Piamonte",'state_id' => 11, 'state' => 11),
				array('name' => "Piendamo",'state_id' => 11, 'state' => 11),
				array('name' => "Popayan",'state_id' => 11, 'state' => 11),
				array('name' => "Puerto Tejada",'state_id' => 11, 'state' => 11),
				array('name' => "Purace",'state_id' => 11, 'state' => 11),
				array('name' => "Rosas",'state_id' => 11, 'state' => 11),
				array('name' => "San Sebastian",'state_id' => 11, 'state' => 11),
				array('name' => "Santa Rosa",'state_id' => 11, 'state' => 11),
				array('name' => "Santander de Quilichao",'state_id' => 11, 'state' => 11),
				array('name' => "Silvia",'state_id' => 11, 'state' => 11),
				array('name' => "Sotara",'state_id' => 11, 'state' => 11),
				array('name' => "Suarez",'state_id' => 11, 'state' => 11),
				array('name' => "Sucre",'state_id' => 11, 'state' => 11),
				array('name' => "Timbio",'state_id' => 11, 'state' => 11),
				array('name' => "Timbiqui",'state_id' => 11, 'state' => 11),
				array('name' => "Toribio",'state_id' => 11, 'state' => 11),
				array('name' => "Totoro",'state_id' => 11, 'state' => 11),
				array('name' => "Villa Rica",'state_id' => 11, 'state' => 11),
				array('name' => "Aguachica",'state_id' => 12, 'state' => 12),
				array('name' => "Agustin Codazzi",'state_id' => 12, 'state' => 12),
				array('name' => "Astrea",'state_id' => 12, 'state' => 12),
				array('name' => "Becerril",'state_id' => 12, 'state' => 12),
				array('name' => "Bosconia",'state_id' => 12, 'state' => 12),
				array('name' => "Chimichagua",'state_id' => 12, 'state' => 12),
				array('name' => "Chiriguana",'state_id' => 12, 'state' => 12),
				array('name' => "Curumani",'state_id' => 12, 'state' => 12),
				array('name' => "El Copey",'state_id' => 12, 'state' => 12),
				array('name' => "El Paso",'state_id' => 12, 'state' => 12),
				array('name' => "Gamarra",'state_id' => 12, 'state' => 12),
				array('name' => "Gonzalez",'state_id' => 12, 'state' => 12),
				array('name' => "La Gloria",'state_id' => 12, 'state' => 12),
				array('name' => "La Jagua Ibirico",'state_id' => 12, 'state' => 12),
				array('name' => "Manaure",'state_id' => 12, 'state' => 12),
				array('name' => "Pailitas",'state_id' => 12, 'state' => 12),
				array('name' => "Pelaya",'state_id' => 12, 'state' => 12),
				array('name' => "Pueblo Bello",'state_id' => 12, 'state' => 12),
				array('name' => "Rio de Oro",'state_id' => 12, 'state' => 12),
				array('name' => "Robles la Paz",'state_id' => 12, 'state' => 12),
				array('name' => "San Alberto",'state_id' => 12, 'state' => 12),
				array('name' => "San Diego",'state_id' => 12, 'state' => 12),
				array('name' => "San Martin",'state_id' => 12, 'state' => 12),
				array('name' => "Tamalameque",'state_id' => 12, 'state' => 12),
				array('name' => "Valledupar",'state_id' => 12, 'state' => 12),
				array('name' => "Acandi",'state_id' => 13, 'state' => 13),
				array('name' => "Alto Baudo",'state_id' => 13, 'state' => 13),
				array('name' => "Atrato",'state_id' => 13, 'state' => 13),
				array('name' => "Bagado",'state_id' => 13, 'state' => 13),
				array('name' => "Bahia Solano",'state_id' => 13, 'state' => 13),
				array('name' => "Bajo Baudo",'state_id' => 13, 'state' => 13),
				array('name' => "Bojaya",'state_id' => 13, 'state' => 13),
				array('name' => "Canton de San Pablo",'state_id' => 13, 'state' => 13),
				array('name' => "Carmen del Darien",'state_id' => 13, 'state' => 13),
				array('name' => "Certegui",'state_id' => 13, 'state' => 13),
				array('name' => "Condoto",'state_id' => 13, 'state' => 13),
				array('name' => "El Carmen",'state_id' => 13, 'state' => 13),
				array('name' => "Istmina",'state_id' => 13, 'state' => 13),
				array('name' => "Jurado",'state_id' => 13, 'state' => 13),
				array('name' => "Litoral del San Juan",'state_id' => 13, 'state' => 13),
				array('name' => "Lloro",'state_id' => 13, 'state' => 13),
				array('name' => "Medio Atrato",'state_id' => 13, 'state' => 13),
				array('name' => "Medio Baudo",'state_id' => 13, 'state' => 13),
				array('name' => "Medio San Juan",'state_id' => 13, 'state' => 13),
				array('name' => "Novita",'state_id' => 13, 'state' => 13),
				array('name' => "Nuqui",'state_id' => 13, 'state' => 13),
				array('name' => "Quibdo",'state_id' => 13, 'state' => 13),
				array('name' => "Rio Iro",'state_id' => 13, 'state' => 13),
				array('name' => "Rio Quito",'state_id' => 13, 'state' => 13),
				array('name' => "Riosucio",'state_id' => 13, 'state' => 13),
				array('name' => "San Jose del Palmar",'state_id' => 13, 'state' => 13),
				array('name' => "Sipi",'state_id' => 13, 'state' => 13),
				array('name' => "Tado",'state_id' => 13, 'state' => 13),
				array('name' => "Unguia",'state_id' => 13, 'state' => 13),
				array('name' => "Union Panamericana",'state_id' => 13, 'state' => 13),
				array('name' => "Ayapel",'state_id' => 14, 'state' => 14),
				array('name' => "Buenavista",'state_id' => 14, 'state' => 14),
				array('name' => "Canalete",'state_id' => 14, 'state' => 14),
				array('name' => "Cerete",'state_id' => 14, 'state' => 14),
				array('name' => "Chima",'state_id' => 14, 'state' => 14),
				array('name' => "Chinu",'state_id' => 14, 'state' => 14),
				array('name' => "Cienaga de Oro",'state_id' => 14, 'state' => 14),
				array('name' => "Cotorra",'state_id' => 14, 'state' => 14),
				array('name' => "La Apartada",'state_id' => 14, 'state' => 14),
				array('name' => "Lorica",'state_id' => 14, 'state' => 14),
				array('name' => "Los Cordobas",'state_id' => 14, 'state' => 14),
				array('name' => "Momil",'state_id' => 14, 'state' => 14),
				array('name' => "Monitos",'state_id' => 14, 'state' => 14),
				array('name' => "Montelibano",'state_id' => 14, 'state' => 14),
				array('name' => "Monteria",'state_id' => 14, 'state' => 14),
				array('name' => "Planeta Rica",'state_id' => 14, 'state' => 14),
				array('name' => "Pueblo Nuevo",'state_id' => 14, 'state' => 14),
				array('name' => "Puerto Escondido",'state_id' => 14, 'state' => 14),
				array('name' => "Puerto Libertador",'state_id' => 14, 'state' => 14),
				array('name' => "Purisima",'state_id' => 14, 'state' => 14),
				array('name' => "Sahagun",'state_id' => 14, 'state' => 14),
				array('name' => "San Andres Sotavento",'state_id' => 14, 'state' => 14),
				array('name' => "San Antero",'state_id' => 14, 'state' => 14),
				array('name' => "San Bernardo Viento",'state_id' => 14, 'state' => 14),
				array('name' => "San Carlos",'state_id' => 14, 'state' => 14),
				array('name' => "San Pelayo",'state_id' => 14, 'state' => 14),
				array('name' => "Tierralta",'state_id' => 14, 'state' => 14),
				array('name' => "Valencia",'state_id' => 14, 'state' => 14),
				array('name' => "Agua de Dios",'state_id' => 15, 'state' => 15),
				array('name' => "Alban",'state_id' => 15, 'state' => 15),
				array('name' => "Anapoima",'state_id' => 15, 'state' => 15),
				array('name' => "Anolaima",'state_id' => 15, 'state' => 15),
				array('name' => "Arbelaez",'state_id' => 15, 'state' => 15),
				array('name' => "Beltran",'state_id' => 15, 'state' => 15),
				array('name' => "Bituima",'state_id' => 15, 'state' => 15),
				array('name' => "Bojaca",'state_id' => 15, 'state' => 15),
				array('name' => "Cabrera",'state_id' => 15, 'state' => 15),
				array('name' => "Cachipay",'state_id' => 15, 'state' => 15),
				array('name' => "Cajica",'state_id' => 15, 'state' => 15),
				array('name' => "Caparrapi",'state_id' => 15, 'state' => 15),
				array('name' => "Caqueza",'state_id' => 15, 'state' => 15),
				array('name' => "Carmen de Carupa",'state_id' => 15, 'state' => 15),
				array('name' => "Chaguani",'state_id' => 15, 'state' => 15),
				array('name' => "Chia",'state_id' => 15, 'state' => 15),
				array('name' => "Chipaque",'state_id' => 15, 'state' => 15),
				array('name' => "Choachi",'state_id' => 15, 'state' => 15),
				array('name' => "Choconta",'state_id' => 15, 'state' => 15),
				array('name' => "Cogua",'state_id' => 15, 'state' => 15),
				array('name' => "Cota",'state_id' => 15, 'state' => 15),
				array('name' => "Cucunuba",'state_id' => 15, 'state' => 15),
				array('name' => "El Colegio",'state_id' => 15, 'state' => 15),
				array('name' => "El Penon",'state_id' => 15, 'state' => 15),
				array('name' => "El Rosal",'state_id' => 15, 'state' => 15),
				array('name' => "Facatativa",'state_id' => 15, 'state' => 15),
				array('name' => "Fomeque",'state_id' => 15, 'state' => 15),
				array('name' => "Fosca",'state_id' => 15, 'state' => 15),
				array('name' => "Funza",'state_id' => 15, 'state' => 15),
				array('name' => "Fuquene",'state_id' => 15, 'state' => 15),
				array('name' => "Fusagasuga",'state_id' => 15, 'state' => 15),
				array('name' => "Gachala",'state_id' => 15, 'state' => 15),
				array('name' => "Gachancipa",'state_id' => 15, 'state' => 15),
				array('name' => "Gacheta",'state_id' => 15, 'state' => 15),
				array('name' => "Gama",'state_id' => 15, 'state' => 15),
				array('name' => "Girardot",'state_id' => 15, 'state' => 15),
				array('name' => "Granada",'state_id' => 15, 'state' => 15),
				array('name' => "Guacheta",'state_id' => 15, 'state' => 15),
				array('name' => "Guaduas",'state_id' => 15, 'state' => 15),
				array('name' => "Guasca",'state_id' => 15, 'state' => 15),
				array('name' => "Guataqui",'state_id' => 15, 'state' => 15),
				array('name' => "Guatavita",'state_id' => 15, 'state' => 15),
				array('name' => "Guayabal de Siquima",'state_id' => 15, 'state' => 15),
				array('name' => "Guayabetal",'state_id' => 15, 'state' => 15),
				array('name' => "Gutierrez",'state_id' => 15, 'state' => 15),
				array('name' => "Jerusalen",'state_id' => 15, 'state' => 15),
				array('name' => "Junin",'state_id' => 15, 'state' => 15),
				array('name' => "La Calera",'state_id' => 15, 'state' => 15),
				array('name' => "La Mesa",'state_id' => 15, 'state' => 15),
				array('name' => "La Palma",'state_id' => 15, 'state' => 15),
				array('name' => "La Pena",'state_id' => 15, 'state' => 15),
				array('name' => "La Vega",'state_id' => 15, 'state' => 15),
				array('name' => "Lenguazaque",'state_id' => 15, 'state' => 15),
				array('name' => "Macheta",'state_id' => 15, 'state' => 15),
				array('name' => "Madrid",'state_id' => 15, 'state' => 15),
				array('name' => "Manta",'state_id' => 15, 'state' => 15),
				array('name' => "Medina",'state_id' => 15, 'state' => 15),
				array('name' => "Mosquera",'state_id' => 15, 'state' => 15),
				array('name' => "Narino",'state_id' => 15, 'state' => 15),
				array('name' => "Nemocon",'state_id' => 15, 'state' => 15),
				array('name' => "Nilo",'state_id' => 15, 'state' => 15),
				array('name' => "Nimaima",'state_id' => 15, 'state' => 15),
				array('name' => "Nocaima",'state_id' => 15, 'state' => 15),
				array('name' => "Ospina Perez",'state_id' => 15, 'state' => 15),
				array('name' => "Pacho",'state_id' => 15, 'state' => 15),
				array('name' => "Paime",'state_id' => 15, 'state' => 15),
				array('name' => "Pandi",'state_id' => 15, 'state' => 15),
				array('name' => "Paratebueno",'state_id' => 15, 'state' => 15),
				array('name' => "Pasca",'state_id' => 15, 'state' => 15),
				array('name' => "Puerto Salgar",'state_id' => 15, 'state' => 15),
				array('name' => "Puli",'state_id' => 15, 'state' => 15),
				array('name' => "Quebradanegra",'state_id' => 15, 'state' => 15),
				array('name' => "Quetame",'state_id' => 15, 'state' => 15),
				array('name' => "Quipile",'state_id' => 15, 'state' => 15),
				array('name' => "Rafael Reyes",'state_id' => 15, 'state' => 15),
				array('name' => "Ricaurte",'state_id' => 15, 'state' => 15),
				array('name' => "San Antonio del Tequendama",'state_id' => 15, 'state' => 15),
				array('name' => "San Bernardo",'state_id' => 15, 'state' => 15),
				array('name' => "San Cayetano",'state_id' => 15, 'state' => 15),
				array('name' => "San Francisco",'state_id' => 15, 'state' => 15),
				array('name' => "San Juan de Rioseco",'state_id' => 15, 'state' => 15),
				array('name' => "Sasaima",'state_id' => 15, 'state' => 15),
				array('name' => "Sesquile",'state_id' => 15, 'state' => 15),
				array('name' => "Sibate",'state_id' => 15, 'state' => 15),
				array('name' => "Silvania",'state_id' => 15, 'state' => 15),
				array('name' => "Simijaca",'state_id' => 15, 'state' => 15),
				array('name' => "Soacha",'state_id' => 15, 'state' => 15),
				array('name' => "Sopo",'state_id' => 15, 'state' => 15),
				array('name' => "Subachoque",'state_id' => 15, 'state' => 15),
				array('name' => "Suesca",'state_id' => 15, 'state' => 15),
				array('name' => "Supata",'state_id' => 15, 'state' => 15),
				array('name' => "Susa",'state_id' => 15, 'state' => 15),
				array('name' => "Sutatausa",'state_id' => 15, 'state' => 15),
				array('name' => "Tabio",'state_id' => 15, 'state' => 15),
				array('name' => "Tausa",'state_id' => 15, 'state' => 15),
				array('name' => "Tena",'state_id' => 15, 'state' => 15),
				array('name' => "Tenjo",'state_id' => 15, 'state' => 15),
				array('name' => "Tibacuy",'state_id' => 15, 'state' => 15),
				array('name' => "Tibirita",'state_id' => 15, 'state' => 15),
				array('name' => "Tocaima",'state_id' => 15, 'state' => 15),
				array('name' => "Tocancipa",'state_id' => 15, 'state' => 15),
				array('name' => "Topaipi",'state_id' => 15, 'state' => 15),
				array('name' => "Ubala",'state_id' => 15, 'state' => 15),
				array('name' => "Ubaque",'state_id' => 15, 'state' => 15),
				array('name' => "Ubate",'state_id' => 15, 'state' => 15),
				array('name' => "Une",'state_id' => 15, 'state' => 15),
				array('name' => "Utica",'state_id' => 15, 'state' => 15),
				array('name' => "Vergara",'state_id' => 15, 'state' => 15),
				array('name' => "Viani",'state_id' => 15, 'state' => 15),
				array('name' => "Villagomez",'state_id' => 15, 'state' => 15),
				array('name' => "Villapinzon",'state_id' => 15, 'state' => 15),
				array('name' => "Villeta",'state_id' => 15, 'state' => 15),
				array('name' => "Viota",'state_id' => 15, 'state' => 15),
				array('name' => "Yacopi",'state_id' => 15, 'state' => 15),
				array('name' => "Zipacon",'state_id' => 15, 'state' => 15),
				array('name' => "Zipaquira",'state_id' => 15, 'state' => 15),
				array('name' => "Inirida",'state_id' => 16, 'state' => 16),
				array('name' => "Calamar",'state_id' => 17, 'state' => 17),
				array('name' => "El Retorno",'state_id' => 17, 'state' => 17),
				array('name' => "Miraflores",'state_id' => 17, 'state' => 17),
				array('name' => "San Jose del Guaviare",'state_id' => 17, 'state' => 17),
				array('name' => "Acevedo",'state_id' => 18, 'state' => 18),
				array('name' => "Agrado",'state_id' => 18, 'state' => 18),
				array('name' => "Aipe",'state_id' => 18, 'state' => 18),
				array('name' => "Algeciras",'state_id' => 18, 'state' => 18),
				array('name' => "Altamira",'state_id' => 18, 'state' => 18),
				array('name' => "Baraya",'state_id' => 18, 'state' => 18),
				array('name' => "Campoalegre",'state_id' => 18, 'state' => 18),
				array('name' => "Colombia",'state_id' => 18, 'state' => 18),
				array('name' => "Elias",'state_id' => 18, 'state' => 18),
				array('name' => "Garzon",'state_id' => 18, 'state' => 18),
				array('name' => "Gigante",'state_id' => 18, 'state' => 18),
				array('name' => "Guadalupe",'state_id' => 18, 'state' => 18),
				array('name' => "Hobo",'state_id' => 18, 'state' => 18),
				array('name' => "Iquira",'state_id' => 18, 'state' => 18),
				array('name' => "Isnos",'state_id' => 18, 'state' => 18),
				array('name' => "La Argentina",'state_id' => 18, 'state' => 18),
				array('name' => "La Plata",'state_id' => 18, 'state' => 18),
				array('name' => "Nataga",'state_id' => 18, 'state' => 18),
				array('name' => "Neiva",'state_id' => 18, 'state' => 18),
				array('name' => "Oporapa",'state_id' => 18, 'state' => 18),
				array('name' => "Paicol",'state_id' => 18, 'state' => 18),
				array('name' => "Palermo",'state_id' => 18, 'state' => 18),
				array('name' => "Palestina",'state_id' => 18, 'state' => 18),
				array('name' => "Pital",'state_id' => 18, 'state' => 18),
				array('name' => "Pitalito",'state_id' => 18, 'state' => 18),
				array('name' => "Rivera",'state_id' => 18, 'state' => 18),
				array('name' => "Saladoblanco",'state_id' => 18, 'state' => 18),
				array('name' => "San Agustin",'state_id' => 18, 'state' => 18),
				array('name' => "Santa Maria",'state_id' => 18, 'state' => 18),
				array('name' => "Suaza",'state_id' => 18, 'state' => 18),
				array('name' => "Tarqui",'state_id' => 18, 'state' => 18),
				array('name' => "Tello",'state_id' => 18, 'state' => 18),
				array('name' => "Teruel",'state_id' => 18, 'state' => 18),
				array('name' => "Tesalia",'state_id' => 18, 'state' => 18),
				array('name' => "Timana",'state_id' => 18, 'state' => 18),
				array('name' => "Villavieja",'state_id' => 18, 'state' => 18),
				array('name' => "Yaguara",'state_id' => 18, 'state' => 18),
				array('name' => "Algarrobo",'state_id' => 20, 'state' => 20),
				array('name' => "Aracataca",'state_id' => 20, 'state' => 20),
				array('name' => "Ariguani",'state_id' => 20, 'state' => 20),
				array('name' => "Cerro San Antonio",'state_id' => 20, 'state' => 20),
				array('name' => "Chivolo",'state_id' => 20, 'state' => 20),
				array('name' => "Cienaga",'state_id' => 20, 'state' => 20),
				array('name' => "Concordia",'state_id' => 20, 'state' => 20),
				array('name' => "El Banco",'state_id' => 20, 'state' => 20),
				array('name' => "El Pinon",'state_id' => 20, 'state' => 20),
				array('name' => "El Reten",'state_id' => 20, 'state' => 20),
				array('name' => "Fundacion",'state_id' => 20, 'state' => 20),
				array('name' => "Guamal",'state_id' => 20, 'state' => 20),
				array('name' => "Nueva Granada",'state_id' => 20, 'state' => 20),
				array('name' => "Pedraza",'state_id' => 20, 'state' => 20),
				array('name' => "Pijino del Carmen",'state_id' => 20, 'state' => 20),
				array('name' => "Pivijay",'state_id' => 20, 'state' => 20),
				array('name' => "Plato",'state_id' => 20, 'state' => 20),
				array('name' => "Puebloviejo",'state_id' => 20, 'state' => 20),
				array('name' => "Remolino",'state_id' => 20, 'state' => 20),
				array('name' => "Sabanas de San Angel",'state_id' => 20, 'state' => 20),
				array('name' => "Salamina",'state_id' => 20, 'state' => 20),
				array('name' => "San Sebastian",'state_id' => 20, 'state' => 20),
				array('name' => "San Zenon",'state_id' => 20, 'state' => 20),
				array('name' => "Santa Ana",'state_id' => 20, 'state' => 20),
				array('name' => "Santa Barbara de Pinto",'state_id' => 20, 'state' => 20),
				array('name' => "Santa Marta",'state_id' => 20, 'state' => 20),
				array('name' => "Sitionuevo",'state_id' => 20, 'state' => 20),
				array('name' => "Tenerife",'state_id' => 20, 'state' => 20),
				array('name' => "Zapayan",'state_id' => 20, 'state' => 20),
				array('name' => "Zona Bananera",'state_id' => 20, 'state' => 20),
				array('name' => "Acacias",'state_id' => 21, 'state' => 21),
				array('name' => "Barranca de Upia",'state_id' => 21, 'state' => 21),
				array('name' => "Cabuyaro",'state_id' => 21, 'state' => 21),
				array('name' => "Castilla la Nueva",'state_id' => 21, 'state' => 21),
				array('name' => "Cubarral",'state_id' => 21, 'state' => 21),
				array('name' => "Cumaral",'state_id' => 21, 'state' => 21),
				array('name' => "El Calvario",'state_id' => 21, 'state' => 21),
				array('name' => "El Castillo",'state_id' => 21, 'state' => 21),
				array('name' => "El Dorado",'state_id' => 21, 'state' => 21),
				array('name' => "Fuente de Oro",'state_id' => 21, 'state' => 21),
				array('name' => "Granada",'state_id' => 21, 'state' => 21),
				array('name' => "Guamal",'state_id' => 21, 'state' => 21),
				array('name' => "La Macarena",'state_id' => 21, 'state' => 21),
				array('name' => "La Uribe",'state_id' => 21, 'state' => 21),
				array('name' => "Lejanias",'state_id' => 21, 'state' => 21),
				array('name' => "Mapiripan",'state_id' => 21, 'state' => 21),
				array('name' => "Mesetas",'state_id' => 21, 'state' => 21),
				array('name' => "Puerto Concordia",'state_id' => 21, 'state' => 21),
				array('name' => "Puerto Gaitan",'state_id' => 21, 'state' => 21),
				array('name' => "Puerto Lleras",'state_id' => 21, 'state' => 21),
				array('name' => "Puerto Lopez",'state_id' => 21, 'state' => 21),
				array('name' => "Puerto Rico",'state_id' => 21, 'state' => 21),
				array('name' => "Restrepo",'state_id' => 21, 'state' => 21),
				array('name' => "San Carlos Guaroa",'state_id' => 21, 'state' => 21),
				array('name' => "San Juan de Arama",'state_id' => 21, 'state' => 21),
				array('name' => "San Juanito",'state_id' => 21, 'state' => 21),
				array('name' => "San Martin",'state_id' => 21, 'state' => 21),
				array('name' => "Villavicencio",'state_id' => 21, 'state' => 21),
				array('name' => "Vista Hermosa",'state_id' => 21, 'state' => 21),
				array('name' => "Alban",'state_id' => 22, 'state' => 22),
				array('name' => "Aldana",'state_id' => 22, 'state' => 22),
				array('name' => "Ancuya",'state_id' => 22, 'state' => 22),
				array('name' => "Arboleda",'state_id' => 22, 'state' => 22),
				array('name' => "Barbacoas",'state_id' => 22, 'state' => 22),
				array('name' => "Belen",'state_id' => 22, 'state' => 22),
				array('name' => "Buesaco",'state_id' => 22, 'state' => 22),
				array('name' => "Chachagui",'state_id' => 22, 'state' => 22),
				array('name' => "Colon",'state_id' => 22, 'state' => 22),
				array('name' => "Consaca",'state_id' => 22, 'state' => 22),
				array('name' => "Contadero",'state_id' => 22, 'state' => 22),
				array('name' => "Cordoba",'state_id' => 22, 'state' => 22),
				array('name' => "Cuaspud",'state_id' => 22, 'state' => 22),
				array('name' => "Cumbal",'state_id' => 22, 'state' => 22),
				array('name' => "Cumbitara",'state_id' => 22, 'state' => 22),
				array('name' => "El Charco",'state_id' => 22, 'state' => 22),
				array('name' => "El Penol",'state_id' => 22, 'state' => 22),
				array('name' => "El Rosario",'state_id' => 22, 'state' => 22),
				array('name' => "El Tablon",'state_id' => 22, 'state' => 22),
				array('name' => "El Tambo",'state_id' => 22, 'state' => 22),
				array('name' => "Funes",'state_id' => 22, 'state' => 22),
				array('name' => "Guachucal",'state_id' => 22, 'state' => 22),
				array('name' => "Guaitarilla",'state_id' => 22, 'state' => 22),
				array('name' => "Gualmatan",'state_id' => 22, 'state' => 22),
				array('name' => "Iles",'state_id' => 22, 'state' => 22),
				array('name' => "Imues",'state_id' => 22, 'state' => 22),
				array('name' => "Ipiales",'state_id' => 22, 'state' => 22),
				array('name' => "La Cruz",'state_id' => 22, 'state' => 22),
				array('name' => "La Florida",'state_id' => 22, 'state' => 22),
				array('name' => "La Llanada",'state_id' => 22, 'state' => 22),
				array('name' => "La Tola",'state_id' => 22, 'state' => 22),
				array('name' => "La Union",'state_id' => 22, 'state' => 22),
				array('name' => "Leiva",'state_id' => 22, 'state' => 22),
				array('name' => "Linares",'state_id' => 22, 'state' => 22),
				array('name' => "Los Andes",'state_id' => 22, 'state' => 22),
				array('name' => "Magui",'state_id' => 22, 'state' => 22),
				array('name' => "Mallama",'state_id' => 22, 'state' => 22),
				array('name' => "Mosquera",'state_id' => 22, 'state' => 22),
				array('name' => "Narino",'state_id' => 22, 'state' => 22),
				array('name' => "Olaya Herrera",'state_id' => 22, 'state' => 22),
				array('name' => "Ospina",'state_id' => 22, 'state' => 22),
				array('name' => "Pasto",'state_id' => 22, 'state' => 22),
				array('name' => "Pizarro",'state_id' => 22, 'state' => 22),
				array('name' => "Policarpa",'state_id' => 22, 'state' => 22),
				array('name' => "Potosi",'state_id' => 22, 'state' => 22),
				array('name' => "Providencia",'state_id' => 22, 'state' => 22),
				array('name' => "Puerres",'state_id' => 22, 'state' => 22),
				array('name' => "Pupiales",'state_id' => 22, 'state' => 22),
				array('name' => "Ricaurte",'state_id' => 22, 'state' => 22),
				array('name' => "Roberto Payan",'state_id' => 22, 'state' => 22),
				array('name' => "Samaniego",'state_id' => 22, 'state' => 22),
				array('name' => "San Bernardo",'state_id' => 22, 'state' => 22),
				array('name' => "San Lorenzo",'state_id' => 22, 'state' => 22),
				array('name' => "San Pablo",'state_id' => 22, 'state' => 22),
				array('name' => "San Pedro de Cartago",'state_id' => 22, 'state' => 22),
				array('name' => "Sandona",'state_id' => 22, 'state' => 22),
				array('name' => "Santa Barbara",'state_id' => 22, 'state' => 22),
				array('name' => "Santacruz",'state_id' => 22, 'state' => 22),
				array('name' => "Sapuyes",'state_id' => 22, 'state' => 22),
				array('name' => "Taminango",'state_id' => 22, 'state' => 22),
				array('name' => "Tangua",'state_id' => 22, 'state' => 22),
				array('name' => "Tumaco",'state_id' => 22, 'state' => 22),
				array('name' => "Tuquerres",'state_id' => 22, 'state' => 22),
				array('name' => "Yacuanquer",'state_id' => 22, 'state' => 22),
				array('name' => "Colon",'state_id' => 24, 'state' => 24),
				array('name' => "Mocoa",'state_id' => 24, 'state' => 24),
				array('name' => "Orito",'state_id' => 24, 'state' => 24),
				array('name' => "Puerto Asis",'state_id' => 24, 'state' => 24),
				array('name' => "Puerto Caycedo",'state_id' => 24, 'state' => 24),
				array('name' => "Puerto Guzman",'state_id' => 24, 'state' => 24),
				array('name' => "Puerto Leguizamo",'state_id' => 24, 'state' => 24),
				array('name' => "San Francisco",'state_id' => 24, 'state' => 24),
				array('name' => "San Miguel",'state_id' => 24, 'state' => 24),
				array('name' => "Santiago",'state_id' => 24, 'state' => 24),
				array('name' => "Sibundoy",'state_id' => 24, 'state' => 24),
				array('name' => "Valle del Guamuez",'state_id' => 24, 'state' => 24),
				array('name' => "Villagarzon",'state_id' => 24, 'state' => 24),
				array('name' => "Armenia",'state_id' => 25, 'state' => 25),
				array('name' => "Buenavista",'state_id' => 25, 'state' => 25),
				array('name' => "Calarca",'state_id' => 25, 'state' => 25),
				array('name' => "Circasia",'state_id' => 25, 'state' => 25),
				array('name' => "Cordoba",'state_id' => 25, 'state' => 25),
				array('name' => "Filandia",'state_id' => 25, 'state' => 25),
				array('name' => "Genova",'state_id' => 25, 'state' => 25),
				array('name' => "La Tebaida",'state_id' => 25, 'state' => 25),
				array('name' => "Montenegro",'state_id' => 25, 'state' => 25),
				array('name' => "Pijao",'state_id' => 25, 'state' => 25),
				array('name' => "Quimbaya",'state_id' => 25, 'state' => 25),
				array('name' => "Salento",'state_id' => 25, 'state' => 25),
				array('name' => "Apia",'state_id' => 26, 'state' => 26),
				array('name' => "Balboa",'state_id' => 26, 'state' => 26),
				array('name' => "Belen de Umbria",'state_id' => 26, 'state' => 26),
				array('name' => "Dos Quebradas",'state_id' => 26, 'state' => 26),
				array('name' => "Guatica",'state_id' => 26, 'state' => 26),
				array('name' => "La Celia",'state_id' => 26, 'state' => 26),
				array('name' => "La Virginia",'state_id' => 26, 'state' => 26),
				array('name' => "Marsella",'state_id' => 26, 'state' => 26),
				array('name' => "Mistrato",'state_id' => 26, 'state' => 26),
				array('name' => "Pereira",'state_id' => 26, 'state' => 26),
				array('name' => "Pueblo Rico",'state_id' => 26, 'state' => 26),
				array('name' => "Quinchia",'state_id' => 26, 'state' => 26),
				array('name' => "Santa Rosa de Cabal",'state_id' => 26, 'state' => 26),
				array('name' => "Santuario",'state_id' => 26, 'state' => 26),
				array('name' => "Aguada",'state_id' => 28, 'state' => 28),
				array('name' => "Albania",'state_id' => 28, 'state' => 28),
				array('name' => "Aratoca",'state_id' => 28, 'state' => 28),
				array('name' => "Barbosa",'state_id' => 28, 'state' => 28),
				array('name' => "Barichara",'state_id' => 28, 'state' => 28),
				array('name' => "Barrancabermeja",'state_id' => 28, 'state' => 28),
				array('name' => "Betulia",'state_id' => 28, 'state' => 28),
				array('name' => "Bolivar",'state_id' => 28, 'state' => 28),
				array('name' => "Bucaramanga",'state_id' => 28, 'state' => 28),
				array('name' => "Cabrera",'state_id' => 28, 'state' => 28),
				array('name' => "California",'state_id' => 28, 'state' => 28),
				array('name' => "Capitanejo",'state_id' => 28, 'state' => 28),
				array('name' => "Carcasi",'state_id' => 28, 'state' => 28),
				array('name' => "Cepita",'state_id' => 28, 'state' => 28),
				array('name' => "Cerrito",'state_id' => 28, 'state' => 28),
				array('name' => "Charala",'state_id' => 28, 'state' => 28),
				array('name' => "Charta",'state_id' => 28, 'state' => 28),
				array('name' => "Chima",'state_id' => 28, 'state' => 28),
				array('name' => "Chipata",'state_id' => 28, 'state' => 28),
				array('name' => "Cimitarra",'state_id' => 28, 'state' => 28),
				array('name' => "Concepcion",'state_id' => 28, 'state' => 28),
				array('name' => "Confines",'state_id' => 28, 'state' => 28),
				array('name' => "Contratacion",'state_id' => 28, 'state' => 28),
				array('name' => "Coromoro",'state_id' => 28, 'state' => 28),
				array('name' => "Curiti",'state_id' => 28, 'state' => 28),
				array('name' => "El Carmen",'state_id' => 28, 'state' => 28),
				array('name' => "El Guacamayo",'state_id' => 28, 'state' => 28),
				array('name' => "El Penon",'state_id' => 28, 'state' => 28),
				array('name' => "El Playon",'state_id' => 28, 'state' => 28),
				array('name' => "Encino",'state_id' => 28, 'state' => 28),
				array('name' => "Enciso",'state_id' => 28, 'state' => 28),
				array('name' => "Florian",'state_id' => 28, 'state' => 28),
				array('name' => "Floridablanca",'state_id' => 28, 'state' => 28),
				array('name' => "Galan",'state_id' => 28, 'state' => 28),
				array('name' => "Gambita",'state_id' => 28, 'state' => 28),
				array('name' => "Giron",'state_id' => 28, 'state' => 28),
				array('name' => "Guaca",'state_id' => 28, 'state' => 28),
				array('name' => "Guadalupe",'state_id' => 28, 'state' => 28),
				array('name' => "Guapota",'state_id' => 28, 'state' => 28),
				array('name' => "Guavata",'state_id' => 28, 'state' => 28),
				array('name' => "Guepsa",'state_id' => 28, 'state' => 28),
				array('name' => "Hato",'state_id' => 28, 'state' => 28),
				array('name' => "Jesus Maria",'state_id' => 28, 'state' => 28),
				array('name' => "Jordan",'state_id' => 28, 'state' => 28),
				array('name' => "La Belleza",'state_id' => 28, 'state' => 28),
				array('name' => "La Paz",'state_id' => 28, 'state' => 28),
				array('name' => "Landazuri",'state_id' => 28, 'state' => 28),
				array('name' => "Lebrija",'state_id' => 28, 'state' => 28),
				array('name' => "Los Santos",'state_id' => 28, 'state' => 28),
				array('name' => "Macaravita",'state_id' => 28, 'state' => 28),
				array('name' => "Malaga",'state_id' => 28, 'state' => 28),
				array('name' => "Matanza",'state_id' => 28, 'state' => 28),
				array('name' => "Mogotes",'state_id' => 28, 'state' => 28),
				array('name' => "Molagavita",'state_id' => 28, 'state' => 28),
				array('name' => "Ocamonte",'state_id' => 28, 'state' => 28),
				array('name' => "Oiba",'state_id' => 28, 'state' => 28),
				array('name' => "Onzaga",'state_id' => 28, 'state' => 28),
				array('name' => "Palmar",'state_id' => 28, 'state' => 28),
				array('name' => "Palmas del Socorro",'state_id' => 28, 'state' => 28),
				array('name' => "Paramo",'state_id' => 28, 'state' => 28),
				array('name' => "Piedecuesta",'state_id' => 28, 'state' => 28),
				array('name' => "Pinchote",'state_id' => 28, 'state' => 28),
				array('name' => "Puente Nacional",'state_id' => 28, 'state' => 28),
				array('name' => "Puerto Parra",'state_id' => 28, 'state' => 28),
				array('name' => "Puerto Wilches",'state_id' => 28, 'state' => 28),
				array('name' => "Rionegro",'state_id' => 28, 'state' => 28),
				array('name' => "Sabana de Torres",'state_id' => 28, 'state' => 28),
				array('name' => "San Andres",'state_id' => 28, 'state' => 28),
				array('name' => "San Benito",'state_id' => 28, 'state' => 28),
				array('name' => "San Gil",'state_id' => 28, 'state' => 28),
				array('name' => "San Joaquin",'state_id' => 28, 'state' => 28),
				array('name' => "San Jose de Miranda",'state_id' => 28, 'state' => 28),
				array('name' => "San Miguel",'state_id' => 28, 'state' => 28),
				array('name' => "San Vicente de Chucuri",'state_id' => 28, 'state' => 28),
				array('name' => "Santa Barbara",'state_id' => 28, 'state' => 28),
				array('name' => "Santa Helena",'state_id' => 28, 'state' => 28),
				array('name' => "Simacota",'state_id' => 28, 'state' => 28),
				array('name' => "Socorro",'state_id' => 28, 'state' => 28),
				array('name' => "Suaita",'state_id' => 28, 'state' => 28),
				array('name' => "Sucre",'state_id' => 28, 'state' => 28),
				array('name' => "Surata",'state_id' => 28, 'state' => 28),
				array('name' => "Tona",'state_id' => 28, 'state' => 28),
				array('name' => "Valle San Jose",'state_id' => 28, 'state' => 28),
				array('name' => "Velez",'state_id' => 28, 'state' => 28),
				array('name' => "Vetas",'state_id' => 28, 'state' => 28),
				array('name' => "Villanueva",'state_id' => 28, 'state' => 28),
				array('name' => "Zapatoca",'state_id' => 28, 'state' => 28),
				array('name' => "Buenavista",'state_id' => 29, 'state' => 29),
				array('name' => "Caimito",'state_id' => 29, 'state' => 29),
				array('name' => "Chalan",'state_id' => 29, 'state' => 29),
				array('name' => "Coloso",'state_id' => 29, 'state' => 29),
				array('name' => "Corozal",'state_id' => 29, 'state' => 29),
				array('name' => "El Roble",'state_id' => 29, 'state' => 29),
				array('name' => "Galeras",'state_id' => 29, 'state' => 29),
				array('name' => "Guaranda",'state_id' => 29, 'state' => 29),
				array('name' => "La Union",'state_id' => 29, 'state' => 29),
				array('name' => "Los Palmitos",'state_id' => 29, 'state' => 29),
				array('name' => "Majagual",'state_id' => 29, 'state' => 29),
				array('name' => "Morroa",'state_id' => 29, 'state' => 29),
				array('name' => "Ovejas",'state_id' => 29, 'state' => 29),
				array('name' => "Palmito",'state_id' => 29, 'state' => 29),
				array('name' => "Sampues",'state_id' => 29, 'state' => 29),
				array('name' => "San Benito Abad",'state_id' => 29, 'state' => 29),
				array('name' => "San Juan de Betulia",'state_id' => 29, 'state' => 29),
				array('name' => "San Marcos",'state_id' => 29, 'state' => 29),
				array('name' => "San Onofre",'state_id' => 29, 'state' => 29),
				array('name' => "San Pedro",'state_id' => 29, 'state' => 29),
				array('name' => "Since",'state_id' => 29, 'state' => 29),
				array('name' => "Sincelejo",'state_id' => 29, 'state' => 29),
				array('name' => "Sucre",'state_id' => 29, 'state' => 29),
				array('name' => "Tolu",'state_id' => 29, 'state' => 29),
				array('name' => "Toluviejo",'state_id' => 29, 'state' => 29),
				array('name' => "Alpujarra",'state_id' => 30, 'state' => 30),
				array('name' => "Alvarado",'state_id' => 30, 'state' => 30),
				array('name' => "Ambalema",'state_id' => 30, 'state' => 30),
				array('name' => "Anzoategui",'state_id' => 30, 'state' => 30),
				array('name' => "Ataco",'state_id' => 30, 'state' => 30),
				array('name' => "Cajamarca",'state_id' => 30, 'state' => 30),
				array('name' => "Carmen de Apicala",'state_id' => 30, 'state' => 30),
				array('name' => "Casabianca",'state_id' => 30, 'state' => 30),
				array('name' => "Chaparral",'state_id' => 30, 'state' => 30),
				array('name' => "Coello",'state_id' => 30, 'state' => 30),
				array('name' => "Coyaima",'state_id' => 30, 'state' => 30),
				array('name' => "Cunday",'state_id' => 30, 'state' => 30),
				array('name' => "Dolores",'state_id' => 30, 'state' => 30),
				array('name' => "Espinal",'state_id' => 30, 'state' => 30),
				array('name' => "Falan",'state_id' => 30, 'state' => 30),
				array('name' => "Flandes",'state_id' => 30, 'state' => 30),
				array('name' => "Fresno",'state_id' => 30, 'state' => 30),
				array('name' => "Guamo",'state_id' => 30, 'state' => 30),
				array('name' => "Guayabal",'state_id' => 30, 'state' => 30),
				array('name' => "Herveo",'state_id' => 30, 'state' => 30),
				array('name' => "Honda",'state_id' => 30, 'state' => 30),
				array('name' => "Ibague",'state_id' => 30, 'state' => 30),
				array('name' => "Icononzo",'state_id' => 30, 'state' => 30),
				array('name' => "Lerida",'state_id' => 30, 'state' => 30),
				array('name' => "Libano",'state_id' => 30, 'state' => 30),
				array('name' => "Mariquita",'state_id' => 30, 'state' => 30),
				array('name' => "Melgar",'state_id' => 30, 'state' => 30),
				array('name' => "Murillo",'state_id' => 30, 'state' => 30),
				array('name' => "Natagaima",'state_id' => 30, 'state' => 30),
				array('name' => "Ortega",'state_id' => 30, 'state' => 30),
				array('name' => "Palocabildo",'state_id' => 30, 'state' => 30),
				array('name' => "Piedras",'state_id' => 30, 'state' => 30),
				array('name' => "Planadas",'state_id' => 30, 'state' => 30),
				array('name' => "Prado",'state_id' => 30, 'state' => 30),
				array('name' => "Purificacion",'state_id' => 30, 'state' => 30),
				array('name' => "Rioblanco",'state_id' => 30, 'state' => 30),
				array('name' => "Roncesvalles",'state_id' => 30, 'state' => 30),
				array('name' => "Rovira",'state_id' => 30, 'state' => 30),
				array('name' => "Saldana",'state_id' => 30, 'state' => 30),
				array('name' => "San Antonio",'state_id' => 30, 'state' => 30),
				array('name' => "San Luis",'state_id' => 30, 'state' => 30),
				array('name' => "Santa Isabel",'state_id' => 30, 'state' => 30),
				array('name' => "Suarez",'state_id' => 30, 'state' => 30),
				array('name' => "Valle de San Juan",'state_id' => 30, 'state' => 30),
				array('name' => "Venadillo",'state_id' => 30, 'state' => 30),
				array('name' => "Villahermosa",'state_id' => 30, 'state' => 30),
				array('name' => "Villarrica",'state_id' => 30, 'state' => 30),
				array('name' => "Cali",'state_id' => 31, 'state' => 31),
				array('name' => "Alcalá",'state_id' => 31, 'state' => 31),
				array('name' => "Ansermanuevo",'state_id' => 31, 'state' => 31),
				array('name' => "Argelia",'state_id' => 31, 'state' => 31),
				array('name' => "Bolívar",'state_id' => 31, 'state' => 31),
				array('name' => "Cartago",'state_id' => 31, 'state' => 31),
				array('name' => "El Aguila",'state_id' => 31, 'state' => 31),
				array('name' => "El Cairo",'state_id' => 31, 'state' => 31),
				array('name' => "El Dovio",'state_id' => 31, 'state' => 31),
				array('name' => "La Unión",'state_id' => 31, 'state' => 31),
				array('name' => "La Victoria",'state_id' => 31, 'state' => 31),
				array('name' => "Obando",'state_id' => 31, 'state' => 31),
				array('name' => "Roldanillo",'state_id' => 31, 'state' => 31),
				array('name' => "Toro",'state_id' => 31, 'state' => 31),
				array('name' => "Ulloa",'state_id' => 31, 'state' => 31),
				array('name' => "Versalles",'state_id' => 31, 'state' => 31),
				array('name' => "Zarzal",'state_id' => 31, 'state' => 31),
				array('name' => "Andalucia",'state_id' => 31, 'state' => 31),
				array('name' => "Buga",'state_id' => 31, 'state' => 31),
				array('name' => "Bugalagrande",'state_id' => 31, 'state' => 31),
				array('name' => "Darién",'state_id' => 31, 'state' => 31),
				array('name' => "El Cerrito",'state_id' => 31, 'state' => 31),
				array('name' => "Ginebra",'state_id' => 31, 'state' => 31),
				array('name' => "Guacarí",'state_id' => 31, 'state' => 31),
				array('name' => "Restrepo",'state_id' => 31, 'state' => 31),
				array('name' => "Riofrio",'state_id' => 31, 'state' => 31),
				array('name' => "San Pedro",'state_id' => 31, 'state' => 31),
				array('name' => "Trujillo",'state_id' => 31, 'state' => 31),
				array('name' => "Tuluá",'state_id' => 31, 'state' => 31),
				array('name' => "Yotoco",'state_id' => 31, 'state' => 31),
				array('name' => "Buenaventura",'state_id' => 31, 'state' => 31),
				array('name' => "Caicedonia",'state_id' => 31, 'state' => 31),
				array('name' => "Sevilla",'state_id' => 31, 'state' => 31),
				array('name' => "Candelaria",'state_id' => 31, 'state' => 31),
				array('name' => "Dagua",'state_id' => 31, 'state' => 31),
				array('name' => "Florida",'state_id' => 31, 'state' => 31),
				array('name' => "Jamundí",'state_id' => 31, 'state' => 31),
				array('name' => "La Cumbre",'state_id' => 31, 'state' => 31),
				array('name' => "Palmira",'state_id' => 31, 'state' => 31),
				array('name' => "Pradera",'state_id' => 31, 'state' => 31),
				array('name' => "Yumbo",'state_id' => 31, 'state' => 31),
				array('name' => "Vijes",'state_id' => 31, 'state' => 31),
				array('name' => "Acaricuara",'state_id' => 32, 'state' => 32),
				array('name' => "Mitu",'state_id' => 32, 'state' => 32),
				array('name' => "Papunaua",'state_id' => 32, 'state' => 32),
				array('name' => "Taraira",'state_id' => 32, 'state' => 32),
				array('name' => "Villa Fatima",'state_id' => 32, 'state' => 32),
				array('name' => "Yavarate",'state_id' => 32, 'state' => 32),
				array('name' => "Cumaribo",'state_id' => 807, 'state' => 807),
				array('name' => "La Primavera",'state_id' => 807, 'state' => 807),
				array('name' => "Puerto Carreno",'state_id' => 807, 'state' => 807),
				array('name' => "Santa Rosalia",'state_id' => 807, 'state' => 807)
			);

			foreach ($cities as $city) {
				$wpdb->insert($country_cities_table, $city, array('%s', '%d', '%d'));
			}
		}
	}

	public static function createPages() {
		// Information needed for creating the plugin's pages
		$page_definitions = array(
			'login' => array(
				'title' 	  =>  'Inicio de sesión' ,
				'content' => '[elearning-login-form]',
				'slug'	    => 'inicio-de-sesion'
			),
			'registro' => array(
				'title' 	  => 'Registro',
				'content' => '[elearning-register-form]',
				'slug'	    => 'registro'
			),
			'recuperar-contrasena' => array(
				'title' 	  => '¿Olvidaste tu contraseña?',
				'content' => '[elearning-password-lost-form]',
				'slug'	    => 'recordar-datos'

			),
			'verificar-correo' => array(
				'title' 	  => 'Agregar nueva contraseña',
				'content' => '[elearning-password-reset-form]',
				'slug'		=> 'verificar-correo'
			),
			'e-learning' => array(
				'title' 	  => 'E-learning',
				'content' => '[elearning-content]',
				'slug'		=> 'e-learning'
			),
			'e-learning-mobile' => array(
				'title' 	  => 'E-learning',
				'content' => '[elearning-content-mobile]',
				'slug'		=> 'e-learning-mobile'
			)
		);

		foreach ( $page_definitions as $slug => $page ) {
			// Check that the page doesn't exist already
			$query = new WP_Query( 'pagename=' . $page['slug'] );
			if ( ! $query->have_posts() ) {
				// Add the page using the data from the array above
				wp_insert_post(
					array(
						'post_content'   	 => $page['content'],
						'post_name'      	 => $page['slug'],
						'post_title'         	 => $page['title'],
						'post_status'         => 'publish',
						'post_type'            => 'page',
						'ping_status'         => 'closed',
						'comment_status' => 'closed',
					)
				);
			}
		}
	}
}


/**
 * Elearning Apasionados BD Methods
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Elearning_Apasionados
 * @subpackage Elearning_Apasionados/includes
 */
/**
 *
 * This class defines all code necessary to run custom queries
 *
 * @since      1.0.0
 * @package    Elearning_Apasionados
 * @subpackage Elearning_Apasionados/includes
 * @author     José Duque <jmduque@smdigital.com.co>
 */
class Elearning_Apasionados_Database extends Elearning_Apasionados_Activator{

	/**
	 * 'fillLearnerData'
	 * Crea la estructura básica para un usuario.
	 *
	 * @return void
	 */
	public static function fillLearnerData( $user = null ) {
		if( ! $user ) return false;

		global $wpdb;

		$pf = $wpdb->prefix;
			
		$progress_table 	= $pf.self::$progress_table_name;
		$tests_table 		= $pf.self::$tests_table_name;
		$learners_table 	= $pf.self::$learners_table_name;
		$learner_test_table = $pf.self::$learner_test_table_name;

		/**
		 * User Data
		 */
		$userID = sanitize_text_field( $user->ID );
		$userCode = sanitize_user( $user->user_login );
		$userDisplayName = sanitize_text_field( $user->display_name );
		$userNiceName = $user->user_nicename;
		$userPass = $user->user_pass;

		/**
		 * Prepared BD Query
		 * @see Learners
		 */
		$learner = $wpdb->get_results( $wpdb->prepare( 
			"SELECT {$learners_table}.id, {$learner_test_table}.test_id, {$learner_test_table}.learner_id, {$learner_test_table}.note, {$progress_table}.lesson, {$progress_table}.section 
			FROM {$learners_table} 
			JOIN {$progress_table} ON {$progress_table}.learner_id = {$learners_table}.id 
			JOIN {$learner_test_table} ON {$learner_test_table}.learner_id = {$learners_table}.id WHERE code = '{$userCode}'", OBJECT
		) );
			
		/**
		 * Prepared BD Query
		 * @see Tests
		 */		
		$availableTests = $wpdb->get_col( $wpdb->prepare( 
			"SELECT {$tests_table}.id 
			FROM {$tests_table} ", OBJECT
		) );
		
		if( is_null( $learner ) || empty( $learner ) ) {

			/**
			 * Fill Learner Data
			 */
			$wpdb->insert( $learners_table, array(
					'name' => !empty( $userDisplayName ) ? $userDisplayName : $userNiceName,
					'code' => $userCode,
					'password' => $userPass, // ... and so on
				), '%s', '%s', '%s'
			);

			// Last created learned ID
			$new_learner_id = $wpdb->insert_id;
			
			/**
			 * Fill Progress Data
			 */
			$wpdb->insert( $progress_table, array(
				'learner_id' => $new_learner_id,
				'lesson' => isset( $availableTests[0] ) ? $availableTests[0] : 1
				), '%d', '%d'
			);
			
			/**
			 * Fill User Tests Data
			 */
			$note = number_format( (float) -1, 2 );
			foreach( $availableTests as $test_id ) {

				/**
				 * DB Learner Test Table Insert
				 */
				$wpdb->insert( $learner_test_table, array(
					'learner_id' => $new_learner_id,
					'test_id'    => $test_id,
					'note'       => $note
					), '%d', '%d', '%d'
				);
				$evaluations[$test_id] = $note;

				/**
				 * Learner Array
				 * @see Requires PHP +5.4 
				 */				
				$learner[] = (object) array(
					'lesson' => $test_id,
					'section' => 0
				);
			}
		} else {
			$evaluations = wp_list_pluck( $learner, 'note', 'test_id' );
		}

		/**
		 * 
		 * @see learner lesson & section son globales
		 * para cualquier indice del arreglo '$learner'
		 * 
		 */

		return array(
			'lesson'      => (int) $learner[0]->lesson,
			'section'     => (int) $learner[0]->section,
			'evaluations' => $evaluations,
		);

		// return $scorm_data;

		/** 
		 * Se obtiene la fecha de finalizacion del curso para mostrar en el diploma
		 */

		// global $wpdb;
		// $prefix = $wpdb->prefix;
		// $progress = $prefix."scorm_progress";
		// $learners = $prefix."scorm_learners";

		// $results = $wpdb->get_results( $wpdb->prepare( 
		// 	"SELECT $progress.updated_at 
		// 	FROM $learners INNER JOIN $progress ON $learners.id = $progress.learner_id 
		// 	WHERE email = '$email_user'", OBJECT 
		// ));

		// Return $results view.
		// $learner = $new_learner;
	}

	/**
	 * Get Learner ID by WP_User code usermeta
	 *
	 * @param WP_User $user
	 * @return int | WP_Error
	 */
	public static function getLearner( $user = null ) {
		if ( ! $user && $user->ID == 0 ) return false;
		
		/**
		 * User Code
		 */
		$userEmail = sanitize_user( $user->user_email );
		
		global $wpdb;

		$pf = $wpdb->prefix;
		$learners_table 	= $pf.self::$learners_table_name;

		$learner = $wpdb->get_var( $wpdb->prepare( "SELECT id FROM {$learners_table} WHERE email = '{$userEmail}'", OBJECT ) );
		
		if( $learner ) {
			return (int) $learner;
		} else {
			return new WP_Error( 'user_not_found', 'Usuario no encontrado' );
		}
	}

	/**
	 * 'updateLearnerProgress'
	 *
	 * @param string $learner_id
	 * @param string $lesson
	 * @param string $section
	 * @return void
	 */
	public static function updateLearnerProgress( $learner_id, $lesson, $section ) {
		if ( ! isset( $learner_id, $lesson, $section ) ) return false; // Bail

		global $wpdb;

		$pf = $wpdb->prefix;
		$progress_table 	= $pf.self::$progress_table_name;

		$lesson = sanitize_text_field( $lesson);
		$section = sanitize_text_field( $section );

		// /**
		//  * Verifica primero si la data es diferente.
		//  */
		// $currentData = $wpdb->get_row( $wpdb->prepare( 
		// 	"SELECT lesson, section FROM {$progress_table} WHERE learner_id = '{$learner_id}'"
		// , OBJECT ) );

		// $isUpgradeable = false;
		// if( $currentData && is_object( $currentData ) ){
		// 	// Update
		// 	var_dump( 
		// 		( $lesson == $currentData->lesson ) && ( $section > $currentData->section ), 
		// 		( $lesson > $currentData->lesson )
		// 	);
		// 	$isUpgradeable = ( ( ( $lesson == $currentData->lesson ) && ( $section > $currentData->section ) ) || ( $lesson > $currentData->lesson ) ) || false;
		// }
		// wp_die( var_dump( $isUpgradeable ) );
		
		$isUpdated = $wpdb->update( $progress_table, 
			array(
				'lesson'		=> stripslashes( $lesson ),
				'section'		=> stripslashes( $section ),
				'updated_at'	=> current_time( 'mysql' )
			),
			array( 'learner_id' => $learner_id ),
			array( 
				'%d',   // lesson
				'%d',   // section
				'%s'
			), 
			array( '%d' ) // learner_id
		);


		return ( $isUpdated !== false ) ? true : false;
	}

	/**
	 * 'updateLearnerTest'
	 *
	 * @param string $learner_id
	 * @param string $test_id
	 * @param string $note
	 * @return void
	 */
	public static function updateLearnerTest( $learner_id, $test_id, $note ) {
		if ( ! isset( $learner_id, $test_id, $note ) ) return false; // Bail

		global $wpdb;

		$pf = $wpdb->prefix;
		$learner_test_table 	= $pf.self::$learner_test_table_name;

		$test_id = sanitize_text_field( $test_id);
		$note = sanitize_text_field( $note );

		$isUpdated = $wpdb->update( $learner_test_table,
			array(
				'note'			=> (float) stripslashes( $note ),
				'updated_at'	=> current_time( 'mysql' )
			),
			array(
				'learner_id'			=> $learner_id,
				'test_id' 		=> stripslashes( $test_id )
			),
			array( 
				'%d',   // note
				'%s',   // updated_at,
			), 
			array( '%d', '%d' ) // learner_id & test_id
		);
		return ( $isUpdated !== false ) ? true : false;

        // $user = wp_get_current_user();
        // $learner = ScormLearner::where('email', $user->user_email)->first();
        // $learner_test = ScormLearnerTest::where('learner_id', $learner->id)->where('test_id', $http->get('test_id'))->first();
        // if($http->get('note') > $learner_test->note) {
        //     $learner_test->note = $http->get('note');
        //     $learner_test->save();
        // }		
	}

	/**
	 * 'finishCourse'
	 * Realiza el envío de automatizaciones relacionados con la culminación del curso.
	 *
	 * @param string $learner_id
	 * @return void
	 */
	public static function finishCourse( $learner_id ) {
		if ( ! isset( $learner_id ) ) return false; // Bail

		global $wpdb;

		$pf = $wpdb->prefix;
		$progress_table 		= $pf.self::$progress_table_name;

		$lastUpdate = $wpdb->get_var( $wpdb->prepare( 
			"SELECT DATE_FORMAT( updated_at, '%%Y | %%m | %%d') FROM {$progress_table} WHERE learner_id = '{$learner_id}'"
		, OBJECT ) );

		/**	
		 * hooked: send_finish_course_email
         */
        // do_action( 'elearning_send_finish_course_email', array(
        //     'name_user'		=> $user->display_name,
		// 	'user_type'		=> get_user_meta( $learner_id, 'user_type', true ),
		// 	// 'code_user'		=> get_user_meta( $learner_id, 'user_identification', true ),
        //     'finish_date'	=> $lastUpdate !== false ? $lastUpdate : current_time( 'Y | m | d' )
		// ));

		// $user = wp_get_current_user();
        // $user_meta = get_user_meta( $user->ID );

        // /**
        //  * @see Este action está en el tema.
		// * 'course' => $user_meta['user_type'][0],
		// * 'email'  => $user->user_email,
		// * 'display_name' => $user->display_name,
		// * 'finish_date' => date in PHP format:  d | m | Y
        //  */
		$user = wp_get_current_user();
        do_action( 'send_finish_course_email', array(
            'course' => get_user_meta( $learner_id, 'user_type', true ),
            'email'  => $user->user_email,
            'display_name' => $user->display_name,
            'finish_date'  => $lastUpdate !== false ? $lastUpdate : current_time( 'd | m | Y' )
            // 'finish_date'  => !empty( $http->get('finishDate') ) ? $http->get('finishDate') : current_time('d | m | Y'),
        ) );		
	}
}