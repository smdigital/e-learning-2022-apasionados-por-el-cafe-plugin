<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Elearning_Apasionados
 * @subpackage Elearning_Apasionados/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Elearning_Apasionados
 * @subpackage Elearning_Apasionados/includes
 * @author     Cristian Torres <catorres@smdigital.com.co>
 */
class Elearning_Apasionados {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Elearning_Apasionados    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $elearning_apasionados    The string used to uniquely identify this plugin.
	 */
	protected $elearning_apasionados;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'ELEARNING_APASIONADOS_VERSION' ) ) {
			$this->version = ELEARNING_APASIONADOS_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->elearning_apasionados = 'elearning-apasionados';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Elearning_Apasionados_Loader. Orchestrates the hooks of the plugin.
	 * - Elearning_Apasionados_i18n. Defines internationalization functionality.
	 * - Elearning_Apasionados_Admin. Defines all hooks for the admin area.
	 * - Elearning_Apasionados_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-elearning-apasionados-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-elearning-apasionados-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-elearning-apasionados-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-elearning-apasionados-public.php';

		$this->loader = new Elearning_Apasionados_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Elearning_Apasionados_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Elearning_Apasionados_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Elearning_Apasionados_Admin( $this->get_elearning_apasionados(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Elearning_Apasionados_Public( $this->get_elearning_apasionados(), $this->get_version() );
		//var_dump( $_SERVER);
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		$myrequest = $_SERVER['REQUEST_URI'];
		 if( strpos($myrequest, 'inicio-de-sesion') || strpos($myrequest, 'e-learning') || strpos($myrequest, 'registro') || strpos($myrequest, 'e-learning-mobile') || strpos($myrequest, 'recordar-datos') || strpos($myrequest, 'verificar-correo') ) { 

				$this->loader->add_filter( 'page_template', $plugin_public, 'our_own_custom_page_template' ); 
				
			}else{
			 remove_filter( 'page_template', $plugin_public, 'our_own_custom_page_template' );
		} 
		
		//Auth
		/* $this->loader->add_filter( 'wp_authenticate', $plugin_public, 'change_data_before_authenticate', 10, 2 ); */
		$this->loader->add_filter( 'authenticate', $plugin_public, 'maybe_redirect_at_authenticate' , 101, 3 );

		// Redirects
		
		$this->loader->add_action( 'login_form_register', $plugin_public,'do_register_user' );
		//$this->loader->add_filter( 'registration_errors', $plugin_public,  'do_register_user' );

		$this->loader->add_action( 'login_form_lostpassword', $plugin_public, 'do_password_lost' );

		$this->loader->add_filter( 'login_redirect', $plugin_public,'redirect_after_login', 10, 3);
		$this->loader->add_filter( 'logout_redirect', $plugin_public, 'redirect_after_logout' );

		$this->loader->add_action( 'login_form_login', $plugin_public,'redirect_to_custom_login' );

		//escuela clientes
		// Auth
		/* $this->loader->add_filter( 'wp_authenticate', $plugin_public, 'change_data_before_authenticate', 10, 2 );
		$this->loader->add_filter( 'authenticate', $plugin_public, 'elearning_maybe_redirect_at_authenticate' , 101, 3 );
		
		// Redirects
		$this->loader->add_filter( 'login_redirect', $plugin_public, 'redirect_after_login', 10, 3 );
		$this->loader->add_filter( 'logout_redirect', $plugin_public, 'redirect_after_logout' );

		// Private Pages Redirect
		$this->loader->add_filter( 'template_redirect', $plugin_public, 'redirect_to_specific_page' );

		// Redirect WP-admin/login view
		$this->loader->add_action( 'login_form_login', $plugin_public,'redirect_to_custom_login' ); */




		//Extra fields administrator
		$this->loader->add_action( 'show_user_profile', $plugin_public, 'elearning_extra_profile_fields', 10 );
		$this->loader->add_action( 'edit_user_profile', $plugin_public, 'elearning_extra_profile_fields', 10 );
	

		//Restablecer contraseña hook, actualmente comentado, por que se usa el de wordpress
		/* $this->loader->add_action( 'login_form_lostpassword', $plugin_public, 'redirect_to_custom_lostpassword' ); */
		/* $this->loader->add_action( 'login_form_resetpass', $plugin_public, 'redirect_to_custom_password_reset' ); */
	/* 	$this->loader->add_action( 'login_form_rp', $plugin_public,'do_password_reset' );
	
		$this->loader->add_action( 'login_form_resetpass', $plugin_public, 'do_password_reset' ); */
	

		// Handlers for form posting actions
		/* add_action( 'login_form_login', $plugin_public, 'elearning_do_register_user' ); */
		
		$this->loader->add_action( 'init', $plugin_public, 'register_shortcodes' ); 	
	
		
		//Handlers ajax

    //new countries, cities, states
  
       $this->loader->add_action( 'wp_ajax_get_states_mycountrie', $plugin_public, 'get_states_mycountrie' );
		$this->loader->add_action( 'wp_ajax_nopriv_get_states_mycountrie', $plugin_public, 'get_states_mycountrie' ); 

		$this->loader->add_action( 'wp_ajax_get_cities_mycities', $plugin_public, 'get_cities_mycities',1 );
		$this->loader->add_action( 'wp_ajax_nopriv_get_cities_mycities', $plugin_public, 'get_cities_mycities',1 ); 

		$this->loader->add_action( 'wp_ajax_update_progress', $plugin_public, 'elearning_update_progress_main' );
		$this->loader->add_action( 'wp_ajax_nopriv_update_progress', $plugin_public, 'elearning_update_progress_main' );

		$this->loader->add_action( 'wp_ajax_update_test', $plugin_public, 'elearning_update_test_main' );
		$this->loader->add_action( 'wp_ajax_nopriv_update_test', $plugin_public, 'elearning_update_test_main' );

		$this->loader->add_action( 'wp_ajax_finish_course', $plugin_public, 'elearning_finish_course_main' );
		$this->loader->add_action( 'wp_ajax_nopriv_finish_course', $plugin_public, 'elearning_finish_course_main' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_elearning_apasionados() {
		return $this->elearning_apasionados;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Elearning_Apasionados_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}