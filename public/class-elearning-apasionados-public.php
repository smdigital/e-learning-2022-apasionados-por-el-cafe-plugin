<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Elearning_Apasionados
 * @subpackage Elearning_Apasionados/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Elearning_Apasionados
 * @subpackage Elearning_Apasionados/public
 * @author     Cristian Torres <catorres@smdigital.com.co>
 */
class Elearning_Apasionados_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $elearning_apasionados    The ID of this plugin.
	 */
	private $elearning_apasionados;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $elearning_apasionados       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $elearning_apasionados, $version ) {

		$this->elearning_apasionados = $elearning_apasionados;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Elearning_Apasionados_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Elearning_Apasionados_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->elearning_apasionados, plugin_dir_url( __FILE__ ) . 'css/elearning-apasionados-public.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->elearning_apasionados, plugin_dir_url( __FILE__ ) . 'css/normalize.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->elearning_apasionados, plugin_dir_url( __FILE__ ) . 'css/style-mobile.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->elearning_apasionados, plugin_dir_url( __FILE__ ) . 'css/style.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Elearning_Apasionados_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Elearning_Apasionados_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->elearning_apasionados, plugin_dir_url( __FILE__ ) . 'js/elearning-apasionados-public.js', array( 'jquery' ), $this->version, false );
		
	}

	public function register_shortcodes() {
		add_shortcode( 'elearning-content', array( $this, 'render_elearning_content' ));
		add_shortcode( 'elearning-content-mobile', array( $this, 'render_elearning_content_mobile' ));
		add_shortcode( 'elearning-login-form', array( $this, 'render_login_form' ));
		add_shortcode( 'elearning-register-form', array( $this, 'render_register_form' ));
		add_shortcode( 'elearning-password-lost-form', array( $this, 'render_password_lost_form' ) );
		add_shortcode( 'elearning-password-reset-form', array( $this, 'render_verify_email' ) );
	}  


	function get_error_message( $error_code ) {
		switch ( $error_code ) {
			// Login errors
			case 'term_not_exists':
				return __( 'Hay un problema con los temas que seleccionaste, recarga e inténtalo de nuevo.', 'elearning-apasionados' );
				break;
				case 'term_empty':
				return __( 'Selecciona por lo menos 3 temas de interés de la lista.', 'elearning-apasionados' );
				break;
			case 'term_limit':
				return __( 'Selecciona por lo menos 3 temas de interés.', 'elearning-apasionados' );
				break;
			case 'username':
				return __( 'Ingresa tu nombre completo.', 'elearning-apasionados' );
				break;
			case 'password':
				return __( 'La contraseña debe contener más de 5 caracteres.', 'elearning-apasionados' );
				break;
			case 'user_inactive':
				return __( 'Esta cuenta esta inactiva.', 'elearning-apasionados' );
				break;
			case 'user_blocked':
				return __( 'Esta cuenta ha sido bloqueda por razones de seguridad. Vuelve a intentar en 5 minutos.', 'elearning-apasionados' );
				break;
			case 'password_confirmation':
				return __( 'Las contraseñas deben ser iguales para continuar.', 'elearning-apasionados' );
				break;
			case 'empty_username':
				return __( 'Ingresa tu correo electrónico', 'elearning-apasionados' );
				break;
			case 'empty_password':
				return __( 'Ingresa tu contraseña.', 'elearning-apasionados' );
				break;
			case 'check_terms':
				return __( 'Debe aceptar los términos y condiciones.', 'elearning-apasionados' );
				break;
			case 'check_privacy':
				return __( 'Debe aceptar la política de privacidad', 'elearning-apasionados' );
				break;
			case 'invalid_username':
				return __( 'Este correo no es válido.', 'elearning-apasionados'	);
				break;
			case 'incorrect_password':
				return __( 'La contraseña no es válida, verifica de nuevo.', 'elearning-apasionados' );
				break;
			// Registration errors
			case 'full_name':
				return __( 'Ingresa tu nombre completo.', 'elearning-apasionados' );
				break;
			case 'email':
				return __( 'El correo no es válido.', 'elearning-apasionados' );
				break;
			case 'email_exists':
				return __( 'Este correo ya esta registrado.', 'elearning-apasionados' );
				break;
			case 'user_type':
				return __( 'Seleccione un tipo de usuario.', 'elearning-apasionados' );
			break;
			case 'state':
				return __( 'Seleccione un departamento.', 'elearning-apasionados' );
			break;
			case 'city':
				return __( 'Seleccione una ciudad.', 'elearning-apasionados' );
			break;
			case 'closed':
				return __( 'El registro de nuevos usuarios no esta permitido.', 'elearning-apasionados' );
				break;
			case 'captcha':
				return __( 'Google reCAPTCHA falló. ¿Eres un robot?', 'elearning-apasionados' );
			break;
			case 'captcha':
				return __( 'Google reCAPTCHA falló. ¿Eres un robot?', 'elearning-apasionados' );
			break;
			case 'phone':
				return 'Ingresa un número de télefono valido.';
			break;
			// Lost password

			case 'empty_username':
				return __( 'Ingresa un correo para continuar.', 'elearning-apasionados' );
				break;
			case 'invalid_email':
			case 'invalidcombo':
				return __( 'Este correo no es válido.', 'elearning-apasionados' );
				break;
			// Reset password

			case 'expiredkey':
			case 'invalidkey':
				return __( 'Este vinculo usado ya no es válido.', 'elearning-apasionados' );
				break;
			case 'password_reset_mismatch':
				return __( "Las dos contraseñas no coinciden.", 'elearning-apasionados' );
				break;
			case 'password_reset_empty':
				return __( "Ingresa la contraseña.", 'elearning-apasionados' );
				break;
			default:
				break;
		}

		return __( 'Ha ocurrido un error. Por favor intenta nuevamente.', 'elearning-apasionados' );
	} 

	//
	// REDIRECT FUNCTIONS
	//

	/********* LOGIN ***************/


	/**
 * Login user
 */

	function redirect_to_custom_login() {

		if ( $_SERVER['REQUEST_METHOD'] == 'GET' ) {

			if ( is_user_logged_in() ) {
				$this->redirect_logged_in_user();
				exit;
			}

			// The rest are redirected to the login page
			$login_url = home_url();
			if ( ! empty( $_REQUEST['redirect_to'] ) ) {

				$login_url = add_query_arg( array( 
					'login-errors' => 'user_login',
					// 'redirect_to' => $_REQUEST['redirect_to']
				),
				$login_url );
			}
		/* 	if ( ! empty( $_REQUEST['checkemail'] ) ) {
				$login_url = add_query_arg( array(
					'login-errors' => 'invalid_user',
					// 'checkemail' => $_REQUEST['checkemail']
				), $login_url );
			} */
			
			wp_redirect( $login_url );
			exit;
		}
	}

	/**
 * Redirects the user to the correct page depending on whether he / she
 * is an admin or not.
 *
 * @param string $redirect_to   An optional redirect_to URL for admin users
 */

	function redirect_logged_in_user( $redirect_to = null ) {

		$user = wp_get_current_user();

		if ( user_can( $user, 'manage_options' ) ) {
			if ( $redirect_to ) {
				wp_safe_redirect( $redirect_to );
			} else {
				wp_redirect( admin_url() );
			}
		} else {
			wp_redirect( home_url('e-learning') );
		}
	}

	/**
	 * Returns the URL to which the user should be redirected after the (successful) login.
	 *
	 * @param string           $redirect_to           The redirect destination URL.
	 * @param string           $requested_redirect_to The requested redirect destination URL passed as a parameter.
	 * @param WP_User|WP_Error $user                  WP_User object if login was successful, WP_Error object otherwise.
	 *
	 * @return string Redirect URL
	 */
	/* function redirect_after_login( $redirect_to, $requested_redirect_to, $user ) {
		if(isset($_POST['redirect_to']) && !empty($_POST['redirect_to'])){
			$redirect_url = $_POST['redirect_to'];
		}else{
			$redirect_url = home_url('e-learning');
			wp_destroy_current_session();
			wp_set_current_user( $user->ID, $user->user_email );
			wp_clear_auth_cookie();
			wp_set_auth_cookie( $user->ID, true, false );
			$redirect_url = home_url('e-learning');
		}

		if ( ! isset( $user->ID ) ) {
			return $redirect_url;
		}

		if ( user_can( $user, 'manage_options' ) ) {
			// Use the redirect_to parameter if one is set, otherwise redirect to admin dashboard.
			if ( $requested_redirect_to == '' ) {
					$redirect_url = admin_url();
			} else {
					$redirect_url = $requested_redirect_to;
			}
		}

		return wp_validate_redirect($redirect_url,home_url());
	} */

	public function redirect_after_login( $redirect_to, $requested_redirect_to, $user ) {
		$redirect_url = home_url();

		if ( ! isset( $user->ID ) ) {
			return $redirect_url;
		}

		if ( user_can( $user, 'manage_options' ) ) {
			// Use the redirect_to parameter if one is set, otherwise redirect to admin dashboard.
			if ( $requested_redirect_to == '' ) {
				$redirect_url = admin_url();
			} else {
				$redirect_url = $redirect_to;
			}
		} else {
			// Non-admin users always go to home after login
			$redirect_url = site_url('e-learning');
		}
		return wp_validate_redirect( $redirect_url, home_url() );
	}

	public function redirect_after_logout () {
		return esc_url( home_url() );
	}


	/**
	 * Redirect the user after authentication if there were any errors.
	 *
	 * @param Wp_User|Wp_Error  $user       The signed in user, or the errors that have occurred during login.
	 * @param string            $username   The user name used to log in.
	 * @param string            $password   The password used to log in.
	 *
	 * @return Wp_User|Wp_Error The logged in user, or error information if there were errors.
	 */
	function maybe_redirect_at_authenticate( $user, $username, $password ) {
		// Check if the earlier authenticate filter (most likely, 
		// the default WordPress authentication) functions have found errors
		if ( $_SERVER['REQUEST_METHOD'] === 'POST' ) {
			if ( is_wp_error( $user ) ) {
				$error_codes = join( ',', $user->get_error_codes() );

				$login_url = home_url( 'inicio-de-sesion' );
				/* $login_url = add_query_arg(
					array(
						'login'=> $error_codes,
						'email'=> $_POST['log'],
						'login-errors', $error_codes
					), $login_url ); */

			//	$currentUser = get_user_by( 'email',  $username);
				$login_url = add_query_arg( 'login-errors', $error_codes, $login_url );

				wp_redirect( $login_url );
				exit;
			}
		}
	
		return $user;
	}

	public function render_login_form( $attributes, $content = null ) {
		// Parse shortcode attributes
		$default_attributes = array( 'show_title' => false );
		$attributes = shortcode_atts( $default_attributes, $attributes );

		if ( is_user_logged_in() ) {
			$currentUser = wp_get_current_user();
			return sprintf( __( '¡Hola %1$s!<br/>¿Deseas <a href="%2$s" class="login-a">cerrar sesión</a>?', 'smdigital-elearning' ), esc_html( $currentUser->display_name ), wp_logout_url() );
		}

		// Error messages
		$errors = array();
		if ( isset( $_REQUEST['login-errors'] ) ) {
			$error_codes = explode( ',', $_REQUEST['login-errors'] );
			foreach ( $error_codes as $code ) {
				$errors []= $this->get_error_message( $code );
			}
		}

		$attributes['errors'] = $errors;

		// Check if user just logged out
		$attributes['logged_out'] = isset( $_REQUEST['logged_out'] ) && $_REQUEST['logged_out'] == true;

		// Check if the user just registered
		//$attributes['registered'] = isset( $_REQUEST['registered'] );
		// Check if the user just requested a new password
		// $attributes['lost_password_sent'] = isset( $_REQUEST['checkemail'] ) && $_REQUEST['checkemail'] == 'confirm';

		// Check if user just updated password
		// $attributes['password_updated'] = isset( $_REQUEST['password'] ) && $_REQUEST['password'] == 'changed';

		// Retrieve recaptcha key
		$attributes['recaptcha_site_key'] = get_option( 'smdigital-elearning-recaptcha-site-key', null );

		// Parse shortcode attributes
		/* $default_attributes = array( 'show_title' => false );
		$attributes = shortcode_atts( $default_attributes, $attributes );

		if ( is_user_logged_in() )
			return __( 'You are already signed in.', 'personalize-login' ); */
/* 
		// Pass the redirect parameter to the WordPress login functionality: by default,
		// don't specify a redirect, but if a valid redirect URL has been passed as
		// request parameter, use it.
		$attributes['redirect'] = '';
		if ( isset( $_REQUEST['redirect_to'] ) ) {
			$attributes['redirect'] = wp_validate_redirect( $_REQUEST['redirect_to'], $attributes['redirect'] );
		}

		// Error messages
		$errors = array();
		if ( isset( $_REQUEST['login-errors'] ) ) {
			$error_codes = explode( ',', $_REQUEST['login-errors'] );
			foreach ( $error_codes as $code ) {
				$errors []= $this->get_error_message( $code );
			}
		}
		
		$attributes['errors'] = $errors;

		// Check if user just logged out
		$attributes['logged_out'] = isset( $_REQUEST['logged_out'] ) && $_REQUEST['logged_out'] == true;

		// Check if the user just registered
		//$attributes['registered'] = isset( $_REQUEST['registered'] );

		// Check if the user just requested a new password
		$attributes['lost_password_sent'] = isset( $_REQUEST['checkemail'] ) && $_REQUEST['checkemail'] == 'confirm';

		// Check if user just updated password
		$attributes['password_updated'] = isset( $_REQUEST['password'] ) && $_REQUEST['password'] == 'changed';

		// Retrieve recaptcha key
		$attributes['recaptcha_site_key'] = get_option( 'personalize-login-recaptcha-site-key', null ); */

		// Render the login form using an external template
		return $this->get_template_html( 'elearning-apasionados-login-form', $attributes );
	}

	/********* END LOGIN *********/ 

	/******* REGISTER ********* */

   /* 	public function do_register_user() {
		
		$referer = $_SERVER['HTTP_REFERER'];
		$findme   = '/registro/';
		$found = strpos($referer, $findme);
		
		if ( $found !== false &&  $_SERVER['REQUEST_METHOD']  == 'POST' ) {
		

			$redirect_url = home_url( 'registro/' );

			//opción en el administrador, si cualquier usuario se puede registrar
			if ( ! get_option( 'users_can_register' ) ) {

				// Registration closed, display error
				$redirect_url = add_query_arg( 'register-errors', 'closed', $redirect_url );
			} else {
			
				$email = sanitize_email($_POST['email']);

				$result = $this->register_user( $email );
			
           
				if ( is_wp_error( $result ) ) {
					// Parse errors into a string and append as parameter to redirect
					
					$errors = join( ',', $result->get_error_codes() );
					$redirect_url = add_query_arg(
						array(
							'register-errors' => $errors,
							'full_name' 	    => $_POST['full_name'],
							'user_type' 	    => $_POST['user_type'],
							'email' 			    => $email,
							'password' 		    => $_POST['password'],
							'phone'			      => $_POST['phone'],
						), $redirect_url );

				} else {
					// Success, redirect to login page.
					$user = get_user_by( 'id', $result );
					
					wp_destroy_current_session();
					wp_set_current_user( $user->ID, $email );
					wp_clear_auth_cookie();
					wp_set_auth_cookie( $user->ID, true, false );
					$redirect_url = home_url('e-learning');

				}
			}
			wp_redirect( $redirect_url );
			exit;
		}
	} 
	
	
	*/

	//private function register_user( $email ) {
		/* $errors = new WP_Error();

		$full_name = sanitize_text_field($_POST['full_name']);
		$user_type = sanitize_text_field($_POST['user_type']);
		$password  = sanitize_text_field($_POST['password']);
		$state 	   = sanitize_text_field($_POST['state']);
		$city 		 = sanitize_text_field($_POST['city']);
		$phone 	   = sanitize_text_field($_POST['phone']);

		
		if ( strlen( trim( $full_name ) ) == 0 || !filter_var( $full_name, FILTER_SANITIZE_STRING) ) {
			$errors->add( 'full_name', $this->get_error_message( 'full_name' ) );
			return $errors;
		}		

		if( empty($user_type) ) {
			$errors->add( 'user_type', $this->get_error_message( 'user_type' ) );
			return $errors;
		}

		// Validar email con filtro PHP
		if ( ! is_email( $email ) || !filter_var($email, FILTER_VALIDATE_EMAIL) ) {
			$errors->add( 'email', $this->get_error_message( 'email' ) );
			return $errors;
		}		

		// Validar email con Regex
		$regex = '/^([A-Za-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/';
		if (!preg_match($regex, $email)) {
			$errors->add( 'email', $this->get_error_message( 'email' ) );
		}

		// Usuario duplicado ( Por nombre de usuario y correo )
		if ( username_exists( $email ) || email_exists( $email ) ) {
			$errors->add( 'email_exists', $this->get_error_message( 'email_exists') );
			return $errors;
		}
		
		if ( strlen( $password ) < 5 ) {
			$errors->add( 'password', $this->get_error_message( 'password'), $password  );
			return $errors;
		}

		if( empty( $state ) ) {
			$errors->add( 'state', $this->get_error_message( 'state' ) );
			return $errors;
		}

		if( empty( $city ) ) {
			$errors->add( 'city', $this->get_error_message( 'city' ) );
			return $errors;
		}

		if( !is_numeric( $phone ) ) {
			$errors->add( 'phone', $this->get_error_message( 'phone' ) );
			return $errors;
		}
		if( empty( $user_type  ) ) {
			$errors->add( 'user_type', $this->get_error_message( 'user_type' ) );
			return $errors;
		} */
	
		/* if ( $_POST['user_password'] !== $_POST['password_confirmation'] ) {
			$errors->add( 'password_confirmation', $this->get_error_message( 'password_confirmation') );
			return $errors;
		} */

		/* if ( empty($_POST['check_terms']) ) {
			$errors->add( 'check_terms', $this->get_error_message( 'check_terms') );
			return $errors;
		}

		if ( empty($_POST['check_privacy']) ) {
			$errors->add( 'check_privacy', $this->get_error_message( 'check_privacy') );
			return $errors;
		} */

		// Generate the password so that the subscriber will have to check email...
		
		/* $user_data = array(
			'user_pass'    => $password,
			'user_login'   => $email,
			'user_email'   => $email,
			'display_name' => $full_name,
			'first_name'   => $full_name,
			'user_type'    => $lastName,
		);

		$user_id = wp_insert_user( $user_data );

			if ( ! is_wp_error( $user_id ) ) {
				add_user_meta($user_id, 'user_type', $user_type, true);
				add_user_meta($user_id, 'state', $state, true);
				add_user_meta($user_id, 'city', $city, true);
				add_user_meta($user_id, 'phone', $phone, true);
			}

		return $user_id;
	} */

		
	/**
	 * Handles the registration of a new user.
	 *
	 * Used through the action hook "login_form_register" activated on wp-login.php
	 * when accessed through the registration action.
	 */
	public function do_register_user() {
			$token      = $_POST['crfapasionados'];

			if ( 'POST' == $_SERVER['REQUEST_METHOD'] ) {

				$redirect_url = home_url( 'registro/' );

				//administrator: Adjustments->general, any user can register

				if ( ! get_option( 'users_can_register' ) ) {
			

						// Registration closed, display error
						$redirect_url = add_query_arg( 'register-errors', 'closed', $redirect_url );
				} else {

						$result =  $this->elearning_register_user($_POST);
						
						if ( is_wp_error( $result ) ) {
						
								// Parse errors into a string and append as parameter to redirect
								$errors = join( ',', $result->get_error_codes() );
								$redirect_url = add_query_arg( 'register-errors', $errors, $redirect_url );
						} else {
								// Success, redirect to login page.

									/* $user = get_user_by( 'id', $result );
									wp_destroy_current_session();
									wp_set_current_user( $user->ID, $user->user_email );
									wp_clear_auth_cookie();
									wp_set_auth_cookie( $user->ID, true, false ); */
									$redirect_url = home_url('e-learning');
						}
				}

				wp_redirect( $redirect_url );
				exit;
		}
	}

	
	/**
	 * Validates and then completes the new user signup process if all went well.
	 *
	 * @param string $email         The new user's email address
	 * @param string $first_name    The new user's first name
	 * @param string $last_name     The new user's last name
	 *
	 * @return int|WP_Error         The id of the user that was created, or error if failed.
	 */
	public function elearning_register_user( $inputs ) {

		$full_name = sanitize_text_field($inputs['full_name']);
		$user_type = sanitize_text_field($inputs['user_type']);
		$email = sanitize_email($_POST['email']);
		$password  = sanitize_text_field($inputs['password']);
    $country 	   = sanitize_text_field($inputs['country']);
		$state 	   = sanitize_text_field($inputs['state']);
		$city 		 = sanitize_text_field($inputs['city']);
		$phone 	   = sanitize_text_field($inputs['phone']);
		$terms     = sanitize_text_field($inputs['terms'] );

		
		$errors = new WP_Error();
		
		if( strlen( trim( $full_name ) ) == 0 || !filter_var( $full_name, FILTER_SANITIZE_STRING) ) {
			$errors->add( 'full_name', $this->get_error_message( 'full_name' ) );
			return $errors;
		}		

		if( empty( $user_type  ) ) {
			$errors->add( 'user_type', $this->get_error_message( 'user_type' ) );
			return $errors;
		} 


		if ( ! is_email( $email ) || !filter_var($email, FILTER_VALIDATE_EMAIL) ) {
			$errors->add( 'email', $this->get_error_message( 'email' ) );
			return $errors;
		}		

		// Validar email con Regex
		$regex = '/^([A-Za-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/';
		if (!preg_match($regex, $email)) {
			$errors->add( 'email', $this->get_error_message( 'email' ) );
		}
		

		// Usuario duplicado ( Por nombre de usuario y correo )
		if ( username_exists( $email ) || email_exists( $email ) ) {
			$errors->add( 'email_exists', $this->get_error_message( 'email_exists') );
			return $errors;
		}

		
		if ( strlen( $password ) < 5 ) {
			$errors->add( 'password', $this->get_error_message( 'password'), $password  );
			return $errors;
		}

    if( empty( $country ) ) {
			$errors->add( 'state', $this->get_error_message( 'country' ) );
			return $errors;
		}

		if( empty( $state ) ) {
			$errors->add( 'state', $this->get_error_message( 'state' ) );
			return $errors;
		}

	/* 	if( empty( $city ) ) {
			$errors->add( 'city', $this->get_error_message( 'city' ) );
			return $errors;
		} */

		if( !is_numeric( $phone ) ) {
			$errors->add( 'phone', $this->get_error_message( 'phone' ) );
			return $errors;
		}
		
		/* if ( $_POST['user_password'] !== $_POST['password_confirmation'] ) {
			$errors->add( 'password_confirmation', $this->get_error_message( 'password_confirmation') );
			return $errors;
		} */

		if ( empty($terms) ) {
			$errors->add( 'check_terms', $this->get_error_message( 'check_terms') );
			return $errors;
		}
		/*
		if ( empty($_POST['check_privacy']) ) {
			$errors->add( 'check_privacy', $this->get_error_message( 'check_privacy') );
			return $errors;
		} */

		$domain = substr($email, strpos($email, '@') + 1);
		$requireOptIn = false;

		if($user_type == 'comercial'){
				$requireOptIn = true;
				$accept_domain = ['novaventa.com', 'comercialnutresa.com.co', 'larecetta.com','colcafe.com.co', 'cordialsa.com.ec'];

				if(!in_array($domain, $accept_domain)){
					$errors->add( 'invalid_email', $this->get_error_message( 'invalid_email') );
					return $errors;
				}
		}elseif($user_type == 'colaborador'){
				$requireOptIn = true;
				$accept_domain = ['colcafe.com.co', 'tropicalcoffee.com.co', 'aliadas.com.co'];
				if(!in_array($domain, $accept_domain)){
					$errors->add( 'invalid_email', $this->get_error_message( 'invalid_email') );
					return $errors;
				}
		} 
	
		$userData = array(
			'user_pass'    => $password,
			'user_login'   => $email,
			'user_email'   => $email,
			'display_name' => $full_name,
			'first_name'   => $full_name,
			'user_type'    => $user_type,
			'role'				 => 'subscriber'
		);


		$userID = wp_insert_user( $userData );
		
		if ( ! is_wp_error( $userID ) ) {
			add_user_meta($userID, 'user_type', $user_type, true);
      add_user_meta($userID, 'country', $country, true);
			add_user_meta($userID, 'state', $state, true);
			add_user_meta($userID, 'city', $city, true);
			add_user_meta($userID, 'phone', $phone, true);
			add_user_meta( $userID, 'terms', $terms, true ) ;
		}

	 /*  wp_new_user_notification( $userID);
		wp_set_auth_cookie($userID, true, '', wp_get_session_token() );
		wp_set_current_user($userID, $email);	
		do_action('wp_login', $email);  */

		return $userID;
	}
		
	public function render_register_form( $attributes, $content = null ) {
		// Parse shortcode attributes
		$default_attributes = array( 'show_title' => false );
		$attributes = shortcode_atts( $default_attributes, $attributes );

		if ( is_user_logged_in() ) {
			return  'You are already signed in.';
		} elseif ( ! get_option( 'users_can_register' ) ) {
			// return 'Registering new users is currently not allowed.';
			//Departments
			//$attributes['states'] = $this->get_states();
      $attributes['countries'] = $this->get_countries();

			return $this->get_template_html( 'elearning-apasionados-register-form', $attributes );
		} else {
			// Retrieve possible errors from request parameters
			$attributes['errors'] = array();
			if ( isset( $_REQUEST['register-errors'] ) ) {
				$error_codes = explode( ',', $_REQUEST['register-errors'] );

				foreach ( $error_codes as $error_code ) {
					$attributes['errors'] []= $this->get_error_message( $error_code );
				}
			}
			// Retrieve recaptcha key
			$attributes['recaptcha_site_key'] = get_option( 'personalize-login-recaptcha-site-key', null );

			//Departments
			//$attributes['states'] = $this->get_states();
      $attributes['countries'] = $this->get_countries();
			return $this->get_template_html( 'elearning-apasionados-register-form', $attributes );
		}
	}

	/****** END REGISTER ********/

	/****** LOST PASSWORD *******/
	
	/**
	 * Initiates password reset.
	 */
	public function do_password_lost() {

		if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
		
			$errors = retrieve_password();
		
			if ( is_wp_error( $errors ) ) {
				
				// Errors found
				$redirect_url = home_url( 'recordar-datos' );
				$redirect_url = add_query_arg( 'errors', join( ',', $errors->get_error_codes() ), $redirect_url );
			} else {
				// Email sent
				$redirect_url = home_url( 'verificar-correo' );
				$redirect_url = add_query_arg( 'checkemail', 'confirm', $redirect_url );
				if ( ! empty( $_REQUEST['redirect_to'] ) ) {
					$redirect_url = $_REQUEST['redirect_to'];
				}
			}
			wp_safe_redirect( $redirect_url );
			exit;
		}
	}

	/**
	 * Restablecer contraseña hook, actualmente comentado, por que se usa el de wordpress
	 */
	/* public function do_password_reset() {
		

		if ( 'POST' == $_SERVER['REQUEST_METHOD'] ) {
			var_dump('do_password_reset'); exit();
			$rp_key = $_REQUEST['rp_key'];
			$rp_login = $_REQUEST['rp_login'];

			$user = check_password_reset_key( $rp_key, $rp_login );

			if ( ! $user || is_wp_error( $user ) ) {
				if ( $user && $user->get_error_code() === 'expired_key' ) {
					wp_redirect( home_url( 'login?login=expiredkey' ) );
				} else {
					wp_redirect( home_url( 'login?login=invalidkey' ) );
				}
				exit;
			}

			if ( isset( $_POST['pass1'] ) ) {
				if ( $_POST['pass1'] != $_POST['pass2'] ) {
					// Passwords don't match
					$redirect_url = home_url( 'resetear-contrasena/' );

					$redirect_url = add_query_arg( 'key', $rp_key, $redirect_url );
					$redirect_url = add_query_arg( 'login', $rp_login, $redirect_url );
					$redirect_url = add_query_arg( 'error', 'password_reset_mismatch', $redirect_url );

					wp_redirect( $redirect_url );
					exit;
				}

				if ( empty( $_POST['pass1'] ) ) {
					// Password is empty
					$redirect_url = home_url( 'resetear-contrasena/' );

					$redirect_url = add_query_arg( 'key', $rp_key, $redirect_url );
					$redirect_url = add_query_arg( 'login', $rp_login, $redirect_url );
					$redirect_url = add_query_arg( 'error', 'password_reset_empty', $redirect_url );

					wp_redirect( $redirect_url );
					exit;

				}

				// Parameter checks OK, reset password
				reset_password( $user, $_POST['pass1'] );
				wp_redirect( home_url( 'login?password=changed' ) );
			} else {
				echo "Invalid request.";
			}

			exit;
		}
	} */

	/* public function redirect_to_custom_lostpassword() {
		var_dump('redirect_to_custom_lostpassword');exit();

		if ( 'GET' == $_SERVER['REQUEST_METHOD'] ) {
			if ( is_user_logged_in() ) {
				$this->redirect_logged_in_user();
				exit;
			}

			wp_redirect( home_url( 'recuperar-contrasena' ) );
			exit;
		}
	} */
	

	/*public function redirect_to_custom_password_reset() {

		
		if ( 'GET' == $_SERVER['REQUEST_METHOD'] ) {
			// Verify key / login combo
			$user = check_password_reset_key( $_REQUEST['key'], $_REQUEST['login'] );
			if ( ! $user || is_wp_error( $user ) ) {
				if ( $user && $user->get_error_code() === 'expired_key' ) {
					wp_redirect( home_url( 'login?login=expiredkey' ) );
				} else {
					wp_redirect( home_url( 'login?login=invalidkey' ) );
				}
				exit;
			}

			$redirect_url = home_url( 'resetear-contrasena' );
			$redirect_url = add_query_arg( 'login', esc_attr( $_REQUEST['login'] ), $redirect_url );
			$redirect_url = add_query_arg( 'key', esc_attr( $_REQUEST['key'] ), $redirect_url );
			wp_redirect( $redirect_url );
			exit;
		}
	}*/

	public function render_password_lost_form( $attributes, $content = null ) {
		
		// Parse shortcode attributes
		$default_attributes = array( 'show_title' => false );
		$attributes = shortcode_atts( $default_attributes, $attributes );

		if ( is_user_logged_in() ) {
			return __( 'You are already signed in.', 'personalize-login' );
		} else {
			// Retrieve possible errors from request parameters
			$attributes['errors'] = array();
			if ( isset( $_REQUEST['errors'] ) ) {
				$error_codes = explode( ',', $_REQUEST['errors'] );

				foreach ( $error_codes as $error_code ) {
					$attributes['errors'] []= $this->get_error_message( $error_code );
				}
			}

			return $this->get_template_html( 'elearning-apasionados-lostpassword-form', $attributes );
		}
	}

	public function render_verify_email( $attributes, $content = null ) {
		return $this->get_template_html( 'elearning-apasionados-verifyemail-password', $attributes );
	}

	/* public function render_verify_email( $attributes, $content = null ) {
		// Parse shortcode attributes
		$default_attributes = array( 'show_title' => false );
		$attributes = shortcode_atts( $default_attributes, $attributes );

		if ( is_user_logged_in() ) {
			return __( 'You are already signed in.', 'personalize-login' );
		} else {
			if ( isset( $_REQUEST['login'] ) && isset( $_REQUEST['key'] ) ) {
				$attributes['login'] = $_REQUEST['login'];
				$attributes['key'] = $_REQUEST['key'];

				// Error messages
				$errors = array();
				if ( isset( $_REQUEST['error'] ) ) {
					$error_codes = explode( ',', $_REQUEST['error'] );

					foreach ( $error_codes as $code ) {
						$errors []= $this->get_error_message( $code );
					}
				}
				$attributes['errors'] = $errors;

				return $this->get_template_html( 'password_reset_form', $attributes );
			} else {
				return __( 'Invalid password reset link.', 'personalize-login' );
			}
		}
	}
	 */
	/****** END LOST PASSWORD ******/

	public function render_elearning_content( $attributes, $content = null ){
		$default_attributes = array( 'show_title' => false );
		$attributes = shortcode_atts( $default_attributes, $attributes );
		//Learner
		$attributes['learner'] = $this->index();
		/* $attributes['source']['application'] = sprintf(
			'%1$s/resources/assets/app/%2$s',
			$assetsUrl,
			$attributes['viewport'] == 'mobile' ? 'app-mobile.js' : 'app.js'
		); */
		return $this->get_template_html( 'elearning-apasionados-content', $attributes );	
	}

	public function render_elearning_content_mobile( $attributes, $content = null ){
		$default_attributes = array( 'show_title' => false );
		$attributes = shortcode_atts( $default_attributes, $attributes );
		//Learner
		$attributes['learner'] = $this->index();
		/* $attributes['source']['application'] = sprintf(
			'%1$s/resources/assets/app/%2$s',
			$assetsUrl,
			$attributes['viewport'] == 'mobile' ? 'app-mobile.js' : 'app.js'
		); */
		return $this->get_template_html( 'elearning-apasionados-content-mobile', $attributes );	
	}

	public function index() {
		require_once plugin_dir_path( __DIR__  ) . 'includes/class-elearning-apasionados-activator.php';
		$user = wp_get_current_user();
    
		$user_meta = get_user_meta( $user->ID );
    
		$user_type = $user_meta['user_type'][0];
		$email_user = $user->user_email;
		$name_user = $user->display_name;
		$terms = $user_meta['terms'][0];
		$pass = $user->user_pass;
	
		if($user->ID == 0 ) {
			//return redirect_response('/inicio-de-sesion');
			    // wp_redirect( '/inicio-de-sesion/' );
                // exit;
		} else {
			$suggest_learner_email = $email_user;
			
			global $wpdb;
			$progress_table = $wpdb->prefix . Elearning_Apasionados_Activator::$progress_table_name;
			$tests_table = $wpdb->prefix . Elearning_Apasionados_Activator::$tests_table_name;
			$learners_table = $wpdb->prefix . Elearning_Apasionados_Activator::$learners_table_name;
			$learner_test_table = $wpdb->prefix . Elearning_Apasionados_Activator::$learner_test_table_name;

			$learner = $wpdb->get_results($wpdb->prepare( "SELECT {$learners_table}.id, {$learner_test_table}.test_id, {$learner_test_table}.learner_id, {$learner_test_table}.note, {$progress_table}.lesson, {$progress_table}.section FROM {$learners_table} JOIN {$progress_table} ON {$progress_table}.learner_id = {$learners_table}.id JOIN {$learner_test_table} ON {$learner_test_table}.learner_id = {$learners_table}.id WHERE email = '{$suggest_learner_email}'", OBJECT));

			if(is_null($learner) || empty($learner) ) {

				$wpdb->insert( $learners_table, array(
					'name' => !empty($user->display_name) ? $user->display_name : $user->user_nicename,
					'email' => $email_user,
					'password' => $pass,
					'terms' => $terms
					// 'created_at' => current_time('mysql'),
					// 'updated_at' => current_time('mysql'),
					),'%s','%s','%s', '%s', '%s'
				);

				$new_learner_id = $wpdb->insert_id;
				
				$wpdb->insert( $progress_table, array(
					'learner_id' => $new_learner_id,
					// 'created_at' => current_time('mysql'),
					// 'updated_at' => current_time('mysql'),
					), '%d', '%s', '%s'
				);

				$tests = array();
				$note = number_format( (float) -1, 2 );
				$availableTests = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
				foreach ($availableTests as $test_id) {
					$wpdb->insert( $learner_test_table, array(
						'learner_id' => $new_learner_id,
						'test_id'    => $test_id,
						'note'          => $note,
						// 'created_at' => current_time('mysql'),
						// 'updated_at' => current_time('mysql')
						),'%d','%d', '%f', '%s', '%s'
					);
					$evaluations[ $test_id ] = $note;
				}

				$learner = $wpdb->get_results($wpdb->prepare( "SELECT {$learners_table}.id, {$learner_test_table}.test_id, {$learner_test_table}.learner_id, {$learner_test_table}.note, {$progress_table}.lesson, {$progress_table}.section FROM {$learners_table} JOIN {$progress_table} ON {$progress_table}.learner_id = {$learners_table}.id JOIN {$learner_test_table} ON {$learner_test_table}.learner_id = {$learners_table}.id WHERE email = '{$suggest_learner_email}'", OBJECT));

			}
		 }
		// $tests = [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1];
		// foreach ($learner as $test) {
		// 	$tests[$test->test_id] = $test->pivot->note;
		// }
		$scorm_data = json_encode(array(
			'lesson'      => $learner->lesson,
			'section'     => $learner->section,
			'evaluations' => $tests,
		));
		return $learner;
		wp_die();
		/** 
		 * Se obtiene la fecha de finalizacion del curso para mostrar en el diploma
		**/
		// global $wpdb;
		// $prefix = $wpdb->prefix;
		// $progress = $prefix."scorm_progress";
		// $learners = $prefix."scorm_learners";
		// $sql = $wpdb->prepare("SELECT $progress.updated_at FROM $learners INNER JOIN $progress ON $learners.id = $progress.learner_id WHERE email = '$email_user' ");     
		// $results = $wpdb->get_results( $sql, OBJECT );
		// }
	}


	private function get_template_html( $template_name, $attributes = null) {
		if ( ! $attributes ) {
			$attributes = array();
		}

		ob_start();

		do_action( 'personalize_login_before_' . $template_name );

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/partials/'.$template_name.'.php';

		do_action( 'personalize_login_after_' . $template_name );

		$html = ob_get_contents();
		ob_end_clean();

		return $html;
	}


	public function our_own_custom_page_template() {
		global $post;
		
		if( $post->post_name == "inicio-de-sesion" || $post->post_name == "registro" || $post->post_name == "recordar-datos" || $post->post_name == "verificar-correo" || $post->post_name == "e-learning" || $post->post_name == "e-learning-mobile") {
			
			$page_template =  plugin_dir_path( dirname( __FILE__ ) ) . 'public/partials/elearning-apasionados-layout.php';
		} 


		return $page_template;
	} 


  public function get_countries() {
    require_once plugin_dir_path( __DIR__  ) . 'includes/class-elearning-apasionados-activator.php';
		
		global $wpdb;

		$countries_table = $wpdb->prefix . Elearning_Apasionados_Activator::$countries_table_name;

		$countries_data = $wpdb->get_results($wpdb->prepare( "SELECT  *  FROM {$countries_table} ", OBJECT));
		
		return  $countries_data; 


  }
	/**
   * Get the states list by country
   *
   * @since     1.0.0
   */
	public function get_states_mycountrie() {	
   
		require_once plugin_dir_path( __DIR__  ) . 'includes/class-elearning-apasionados-activator.php';
		
		global $wpdb;

    $states_table = $wpdb->prefix . Elearning_Apasionados_Activator::$states_table_name;
		$cities_table = $wpdb->prefix . Elearning_Apasionados_Activator::$cities_table_name;
		$country_code = $_GET['country_id'];

    $states_data = $wpdb->get_results($wpdb->prepare( "SELECT  *  FROM {$states_table} WHERE country_id = {$country_code}" , OBJECT));

    if($states_data) 
		wp_send_json_success( json_encode($states_data), 200);

		wp_send_json_error( $wpdb->show_errors ,422); 

	  /* 	$country_states_table = $wpdb->prefix . Elearning_Apasionados_Activator::$country_states_table_name;

		$country_states_data = $wpdb->get_results($wpdb->prepare( "SELECT  *  FROM {$country_states_table} ", OBJECT));
		
		return  $country_states_data;  */
   /*  $states_table = $wpdb->prefix . Elearning_Apasionados_Activator::$states_table_name;

		$states_data = $wpdb->get_results($wpdb->prepare( "SELECT  *  FROM {$states_table} ", OBJECT));
		
		return  $states_data;  */
	
    return  $states_data; 
		wp_die();
	}

  /**
   * Get the cities list by state
   *
   * @since     1.0.0
   */
	public function get_cities_mycities() {
		
		require_once plugin_dir_path( __DIR__  ) . 'includes/class-elearning-apasionados-activator.php';

		global $wpdb;

		$states_table = $wpdb->prefix . Elearning_Apasionados_Activator::$states_table_name;
		$cities_table = $wpdb->prefix . Elearning_Apasionados_Activator::$cities_table_name;
		

		$state_code = $_GET['state_id'];
		

		// $country_states_data = $wpdb->get_row($wpdb->prepare( "SELECT  id FROM {$country_states_table} WHERE id = %d", $state_code, OBJECT));

		$cities_data = $wpdb->get_results($wpdb->prepare( "SELECT  *  FROM {$cities_table} WHERE state_id = {$state_code}" , OBJECT));
		// $country_cities_data = $wpdb->get_results($wpdb->prepare( "SELECT  *  FROM {$country_cities_table}" , OBJECT));
		//echo json_encode( $country_cities_data );
		// var_dump(json_encode($country_cities_data));exit();

		//echo json_encode($country_cities_data);
		if($cities_data) 
		wp_send_json_success( json_encode($cities_data), 200);

		wp_send_json_error( $wpdb->show_errors ,422); 
		
		return  $cities_data; 

		wp_die();
	}


	/**
	 * Elearning Update Progress Ajax function
	 *
	 * @return void
	 */
	public function elearning_update_progress_main(){

		if( ! ( isset( $_POST ) && !empty( $_POST ) ) ) {
			wp_send_json_error( 'No hay información en el request.' );
		}

		if( ! ( isset( $_POST['action'] ) && $_POST['action'] == 'update_progress' ) ) {
			wp_send_json_error( 'No estás autorizado para acceder a este endpoint.' );
		}

		if( ! ( isset( $_POST['nonce'] ) && wp_verify_nonce( $_POST['nonce'], 'ajax_nonce' ) ) ) {
			wp_send_json_error( 'Recarga la ventana e inténtalo de nuevo.' );
		}

		if( ! ( isset( $_POST['lesson'] ) && isset( $_POST['section'] ) ) ) {
			wp_send_json_error( 'Los campos "lesson" y "section" son obligatorios.' );
		}

		/**
		 * Import Activator & DB Data
		 */
		require_once plugin_dir_path( __DIR__  ).'includes/class-elearning-apasionados-activator.php';

		/**
		 * Get Learner Data
		 */
		$learner_id = Elearning_Apasionados_Database::getLearner( wp_get_current_user() );
		
		$progressStatus = new WP_Error( 'user_update_failed', __( 'No se actualizó la información del Aprendiz', 'smdigital_elearning' ) );
		

		if( !is_wp_error( $learner_id ) ) {
			$progressStatus = Elearning_Apasionados_Database::updateLearnerProgress( $learner_id, $_POST['lesson'], $_POST['section'] );
		}
		global $wpdb;
		$progress_table = $wpdb->prefix . Elearning_Apasionados_Activator::$progress_table_name;
		$current_user = wp_get_current_user();
		$user_email = $current_user->user_email;

		$progress_query = $wpdb->get_results($wpdb->prepare( "SELECT {$progress_table}.lesson, {$progress_table}.section FROM {$progress_table} WHERE {$progress_table}.learner_id = {$learner_id} ", OBJECT));

		if( $progress_query[0]->lesson == 0 && $progress_query[0]->section == 1 ){
			do_action( 'send_unfinish_course_email', $user_email );
		}
		/**
		 * Response
		 */
		if( $progressStatus && ! is_wp_error( $progressStatus ) ){
			wp_send_json_success( __( 'Información actualizada', 'smdigital_elearning' ) );
		} else {
			wp_send_json_error( $progressStatus->get_error_message( 'user_update_failed' ) );
		}

        // $learner = ScormLearner::where('email', $user->user_email)->first();
        // $learner_progress = ScormProgress::where('learner_id', $learner->id)->first();
        // $learner_progress->lesson = $http->get('lesson');
        // $learner_progress->section = $http->get('section');
        // $learner_progress->save();

        // if($http->get('lesson') == 0 && $http->get('section')==1){
        //     do_action( 'send_unfinish_course_email', $user->user_email );    
        // }
		wp_die();

  }
	/**
	 * Elearning Finish Course Ajax function
	 *
	 * @return void
	 */	
  public function elearning_finish_course_main(){

		if( ! ( isset( $_POST ) && !empty( $_POST ) ) )
			wp_send_json_error( 'No hay información en el request.' );

		if( ! ( isset( $_POST['action'] ) && $_POST['action'] == 'finish_course' ) )
			wp_send_json_error( 'No estás autorizado para acceder a este endpoint.' );

		if( ! ( isset( $_POST['nonce'] ) && wp_verify_nonce( $_POST['nonce'], 'ajax_nonce' ) ) )
			wp_send_json_error( 'Recarga la ventana e inténtalo de nuevo.' );
	
		/**
		 * Import Activator & DB Data
		 */
		require_once plugin_dir_path( __DIR__  ).'includes/class-elearning-apasionados-activator.php';

		/**
		 * Get Learner Data
		 */
		$learner_id = Elearning_Apasionados_Database::getLearner( wp_get_current_user() );
		
		$queryStatus = new WP_Error( 'user_finish_failed', __( 'No se ejecutaron las tareas de finalización', 'smdigital_elearning' ) );

		if( !is_wp_error( $learner_id ) ) {
			$queryStatus = Elearning_Apasionados_Database::finishCourse( $learner_id );
		}

		/**
		 * Response
		 */
		if( $queryStatus && ! is_wp_error( $queryStatus ) ){
			wp_send_json_success( __( 'Información actualizada finish', 'smdigital_elearning' ) );
		} else {
			wp_send_json_error( $queryStatus->get_error_message( 'user_finish_failed' ) );
		}

        // $user = wp_get_current_user();
        // $user_meta = get_user_meta( $user->ID );

        // /**
        //  * @see Este action está en el tema.
        //  */
        // do_action( 'send_finish_course_email', array(
        //     'course' => $user_meta['user_type'][0],
        //     'email'  => $user->user_email,
        //     'display_name' => $user->display_name,
        //     'finish_date'  => !empty( $http->get('finishDate') ) ? $http->get('finishDate') : current_time('d | m | Y'),
        // ) );
  }

	/**
	 * Elearning Update Test Ajax function
	 *
	 * @return void
	 */	
  public function elearning_update_test_main(){

		if( ! ( isset( $_POST ) && !empty( $_POST ) ) ) {
			wp_send_json_error( 'No hay información en el request.' );
		}

		if( ! ( isset( $_POST['action'] ) && $_POST['action'] == 'update_test' ) ) {
			wp_send_json_error( 'No estás autorizado para acceder a este endpoint.' );
		}

		if( ! ( isset( $_POST['nonce'] ) && wp_verify_nonce( $_POST['nonce'], 'ajax_nonce' ) ) ) {
			wp_send_json_error( 'Recarga la ventana e inténtalo de nuevo.' );
		}

		if( ! ( isset( $_POST['test_id'] ) && isset( $_POST['note'] ) ) ) {
			wp_send_json_error( 'Los campos "lesson" y "section" son obligatorios.' );
		}

		/**
		 * Import Activator & DB Data
		 */
		require_once plugin_dir_path( __DIR__  ).'includes/class-elearning-apasionados-activator.php';	
		
		/**
		 * Get Learner Data
		 */
		$learner_id = Elearning_Apasionados_Database::getLearner( wp_get_current_user() );	
		
		$testStatus = new WP_Error( 'user_test_update_failed', __( 'No se actualizó la información del Aprendiz Test', 'smdigital_elearning' ) );		

		if( !is_wp_error( $learner_id ) ) {
			$testStatus = Elearning_Apasionados_Database::updateLearnerTest( $learner_id, $_POST['test_id'], $_POST['note'] );
		}

		/**
		 * Response
		 */		
		if( $testStatus && ! is_wp_error( $testStatus ) ){
			wp_send_json_success( __( 'Información actualizada (test)', 'smdigital_elearning' ) );
		} else {
			wp_send_json_error( $testStatus->get_error_message( 'user_test_update_failed' ) );
		}		

		wp_die();
        // $user = wp_get_current_user();
        // $learner = ScormLearner::where('email', $user->user_email)->first();
        // $learner_test = ScormLearnerTest::where('learner_id', $learner->id)->where('test_id', $http->get('test_id'))->first();
        // if($http->get('note') > $learner_test->note) {
        //     $learner_test->note = $http->get('note');
        //     $learner_test->save();
        // }
  }


	/** 
	 * @param ADMINISTRADOR
	 * Campo extra en el perfil del usuario.
	 */
	function elearning_extra_profile_fields( $user ) { ?>
		<?php 
		
				$term = esc_attr( get_the_author_meta( 'terms', $user->ID ) ); 

				if( $term == '1')
					$checkeTerms = 'checked';   
		?>
		<h3>Campos Extras</h3>
		<table class="form-table">
		<tr> 
				<th><label for="user_type">Tipo de usuario</label></th>
				<td>
				<input type="text" name="user_type" id="user_type" value="<?php echo esc_attr( get_the_author_meta( 'user_type', $user->ID ) ); ?>"/><br />
				</td>
			</tr>
      <tr> 
				<th><label for="country">País</label></th>
				<td>
				<input type="text" name="country" id="country" value="<?php echo esc_attr( get_the_author_meta( 'country', $user->ID ) ); ?>"/><br />
				</td>
			</tr>
			<tr> 
				<th><label for="state">Departamento</label></th>
				<td>
				<input type="text" name="state" id="state" value="<?php echo esc_attr( get_the_author_meta( 'state', $user->ID ) ); ?>"/><br />
				</td>
			</tr>
			<tr> 
				<th><label for="city">Ciudad</label></th>
				<td>
				<input type="text" name="city" id="city" value="<?php echo esc_attr( get_the_author_meta( 'city', $user->ID ) ); ?>"/><br />
				</td>
			</tr>

			<tr> 
				<th><label for="phone">telefono</label></th>
				<td>
					<input type="tel" name="phone" id="phone" value="<?php echo esc_attr( get_the_author_meta( 'phone', $user->ID ) ); ?>"/><br />
				</td>
			</tr>
			<tr> 
				<th><label for="terms">Términos y Condiciones</label></th>
				<td>
					<input type="checkbox" name="terms" id="terms" value="<?php echo esc_attr( get_the_author_meta( 'terms', $user->ID ) ); ?>" <?php echo( !empty($checkeTerms ) ) ? $checkeTerms : null; ?>/>
				</td>
			</tr>
		</table>
	<?php
	}


	/** 
	* @param ADMINISTRADOR
	* Función que guarda los datos del usuario cuando se edita el perfil.
	*/

	function elearning_save_extra_profile_fields( $user_id ) {

		if ( !current_user_can( 'edit_user', $user_id ) )
				return false;
				add_user_meta($userID, 'user_type', $_POST['user_type'], true);
        add_user_meta($userID, 'country', $_POST['country'], true);
				add_user_meta($userID, 'state', $_POST['state'], true);
				add_user_meta($userID, 'city', $_POST['city'], true);
				add_user_meta($userID, 'phone', $_POST['phone'], true);
				add_user_meta( $userID, 'terms', $_POST['terms'], true ) ;
			}
	}