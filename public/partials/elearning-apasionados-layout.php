<?php 
    $assetUrl = plugin_dir_url(__DIR__);  
    $homeUrl = home_url();  
    global $post;
?>

<?php //if( !is_user_logged_in() ){ wp_redirect('/inicio-de-sesion/'); } ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Apasionados por el Café</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?php echo $assetUrl; ?>resources/assets/images/favicon.ico">
        <link rel="stylesheet" href="<?php echo $assetUrl; ?>css/normalize.css">
        <link rel="stylesheet" href="<?php echo $assetUrl; ?>css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo $assetUrl; ?>css/uniform.css">
        <link rel="stylesheet" href="https://www.apasionadosporelcafe.com/wp-content/themes/apasionados-por-el-cafe-website/library/css/style.css">
        <link rel="stylesheet" href="<?php echo $assetUrl; ?>css/style.css">
        <?php 
            if( $post->post_name == 'inicio-de-sesion' || $post->post_name == 'registro' || $post->post_name == 'recordar-datos' || $post->post_name == 'verificar-correo'): ?>
                <meta name="robots" content="noindex">
        <?php endif; ?>

        <script type="text/javascript">
            var assets_url = "",data = "";
            data = data.replace(/&quot;/g,'"');
            var scorm_data = data; 
		    var ajax_elearning = {url: "<?php echo admin_url( 'admin-ajax.php' ) ?>", nonce: "<?php echo wp_create_nonce( 'ajax_nonce' ); ?>" };
            var logout = '<?php echo wp_logout_url(); ?>';
        </script>
	</head>
	<body class="elearning-layout">
    <?php 
        
                        
            switch ($post->post_name) {
                case 'inicio-de-sesion':
                  if( is_user_logged_in() ) {
                    wp_redirect(home_url());
                  }else{
                    echo do_shortcode('[elearning-login-form]');
                  }
                break;
                case 'registro':
                  if( is_user_logged_in() ) {
                    wp_redirect(home_url());
                  }else{
                    echo do_shortcode('[elearning-register-form]');
                  }
                break;
                case 'recordar-datos':
                  if( is_user_logged_in() ) {
                    wp_redirect(home_url());
                  }else{
                    echo do_shortcode('[elearning-password-lost-form]');
                  }
                break;
                case 'e-learning':
                    if( is_user_logged_in() ):
                        echo do_shortcode('[elearning-content]');
                    else:
                        echo do_shortcode('[elearning-login-form]');
                    endif;
                break;
                case 'e-learning-mobile':
                    if( is_user_logged_in() ):
                        echo do_shortcode('[elearning-content-mobile]');
                    else:
                        echo do_shortcode('[elearning-login-form]');
                    endif;
                break;
                default:
                        echo do_shortcode('[elearning-password-reset-form]');
                break;
            }
        ?>
        <?php if( !wp_is_mobile() ): ?>
        <footer>
        <div class="wrapper-foot align-center-phone">
            <ul id="menu-footer-links" class=""><li id="menu-item-24" class="grupo-nutresa menu-item menu-item-type-custom menu-item-object-custom menu-item-24"><a target="_blank" href="http://www.gruponutresa.com/">&nbsp;</a></li>
                <li id="menu-item-21" class="colcafe menu-item menu-item-type-custom menu-item-object-custom menu-item-21"><a target="_blank" href="http://es.colcafe.com.co/">Industria Colombiana de Café</a></li>
                <li id="menu-item-22" class="copyright menu-item menu-item-type-custom menu-item-object-custom menu-item-22"><a><strong>Copyright © 2018 colcafé</strong> Todos los derechos reservados Medellín – Colombia.</a></li>
                <li id="menu-item-943" class="habeas menu-item menu-item-type-post_type menu-item-object-page menu-item-943"><a href="https://www.apasionadosporelcafe.com/habeas-data/">Habeas data</a></li>
                <li id="menu-item-23" class="by-sm menu-item menu-item-type-custom menu-item-object-custom menu-item-23"><a target="_blank" href="http://www.smdigital.com.co/">SMDigital</a></li>
            </ul>
        </div>
        <?php endif; ?>
        </footer>
        <script src="<?php echo $assetUrl; ?>js/jquery.min.js"></script>
        <script src="<?php echo $assetUrl; ?>js/jquery-ui.min.js"></script>
        <script src="<?php echo $assetUrl; ?>js/bootstrap.min.js"></script>
        <script src="<?php echo $assetUrl; ?>js/jquery.uniform.min.js"></script>
        <?php /*<script src="<?php echo $assetUrl; ?>js/parsley.js"></script> */?>
        <script src="<?php echo $assetUrl; ?>js/parsley.min.js"></script>
        <script src="<?php echo $assetUrl; ?>js/elearning-apasionados-public.js"></script>
        
    </body>
</html>

