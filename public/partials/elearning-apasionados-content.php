<?php 
$assetUrl = plugin_dir_url(__DIR__);  
$homeUrl = home_url();  
?>
<!-- Scripts angular Desktop-->
<link rel="stylesheet" href="<?php echo $assetUrl; ?>resources/assets/css/normalize.css">
<link rel="stylesheet" href="<?php echo $assetUrl; ?>resources/assets/includes/widgets/bootstrap-3/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo $assetUrl; ?>resources/assets/includes/widgets/uniform/uniform.css">
<link rel="stylesheet" href="<?php echo $assetUrl; ?>resources/assets/bower_components/intro.js/minified/introjs.min.css">
<link rel="stylesheet" href="<?php echo $assetUrl; ?>resources/assets/bower_components/videogular-themes-default/videogular.min.css">
<link rel="stylesheet" href="<?php echo $assetUrl; ?>resources/assets/bower_components/slick-carousel/slick/slick.css">
<link rel="stylesheet" href="<?php echo $assetUrl; ?>resources/assets/bower_components/slick-carousel/slick/slick-theme.css">
<link rel="stylesheet" href="<?php echo $assetUrl; ?>resources/assets/css/style.css">

<script src="<?php echo $assetUrl; ?>resources/assets/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo $assetUrl; ?>resources/assets/bower_components/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo $assetUrl; ?>resources/assets/includes/widgets/bootstrap-3/js/bootstrap.min.js"></script>
<script src="<?php echo $assetUrl; ?>resources/assets/includes/widgets/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo $assetUrl; ?>resources/assets/bower_components/intro.js/minified/intro.min.js"></script>
<script src="<?php echo $assetUrl; ?>resources/assets/js/main.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.5/handlebars.min.js"></script>

<script src="<?php echo $assetUrl; ?>resources/assets/bower_components/angular/angular.min.js"></script>
<script src="<?php echo $assetUrl; ?>resources/assets/bower_components/angular-intro.js/build/angular-intro.min.js"></script>
<script src="<?php echo $assetUrl; ?>resources/assets/bower_components/slick-carousel/slick/slick.js"></script>
<script src="<?php echo $assetUrl; ?>resources/assets/bower_components/angular-slick-carousel/dist/angular-slick.min.js"></script>
<script src="<?php echo $assetUrl; ?>resources/assets/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
<script src="<?php echo $assetUrl; ?>resources/assets/bower_components/angular-sanitize/angular-sanitize.min.js"></script>
<script src="<?php echo $assetUrl; ?>resources/assets/bower_components/angular-resource/angular-resource.min.js"></script>
<script src="<?php echo $assetUrl; ?>resources/assets/bower_components/angular-ui-router/release/angular-ui-router.min.js"></script>
<script src="<?php echo $assetUrl; ?>resources/assets/bower_components/videogular/videogular.min.js"></script>
<script src="<?php echo $assetUrl; ?>resources/assets/bower_components/videogular-controls/vg-controls.min.js"></script>


<!-- Scripts angular -->

<script type="text/javascript">
    var assets_url = "<?php echo $assetUrl; ?>";
    var home_url = "<?php echo $homeUrl; ?>";
    <?php 
    if( is_array( $attributes['learner'] ) ):

    $lesson = array_pop($attributes['learner']);
    $section = array_pop($attributes['learner']);
    $evaluations = $attributes['learner'];
    
    $tests = [];
    foreach ($evaluations as $test ) {
        $note = doubleval($test->note);
        array_push($tests, $note);
    }
    
    $scorm_data = json_encode(array(
        'lesson'      => intval($lesson->lesson),
        'section'     => intval($section->section),
        'evaluations' => $tests,
    )); 
    endif;
    
?>
    var data = '<?php echo $scorm_data; ?>';

    //data = data.replace(/&quot;/g,'"');

    var scorm_data = data;
    <?php 
        $user = wp_get_current_user();
        $user_id = get_user_meta( $user->ID );
        $user_type = $user_id['user_type'][0];
        
        $email_user = $user->user_email;
        $name_user = $user->display_name; 
        function my_has_role($user, $role) {
            $roles = $user->roles; 
            return in_array($role, (array) $user->roles);
          }
          
          if(my_has_role($user, 'administrator')) {
            $user_type = 'administrator';
          }

          if(my_has_role($user, 'business')) {
            $user_type = 'business';
            $user_id = $user->ID;
          }
    ?>
    var email_user = "<?php echo $email_user; ?>";
    var name_user = "<?php echo $name_user; ?>";
    var id_user = "<?php echo $user_id; ?>";
    var public = "<?php echo $user_type; ?>";


    <?php if ($results != null): ?>
            /* Fecha obtenida desde la BD, fecha de finalizacion del curso */
            var fecha = new Date(" {{  results[0].updated_at  }} ");
            fecha = fecha.getDate() + " | " + (fecha.getMonth()+1)+ " | " + fecha.getFullYear();
           <?php else: ?>
            /*Fecha en caso de estar vacia imprime la fecha del dia actual */
            var fecha = new Date();
            fecha = fecha.getDate() + " | " +(fecha.getMonth()+1) + " | " +fecha.getFullYear();
            <?php endif; ?>
           


            <?php if ($user_type == 'aficionado'): ?>
               var curso = 'Aficionado';
            <?php elseif ($user_type == 'emprendedor'): ?>
               var curso = 'Emprendedor';
            <?php elseif ($user_type == 'barista-chef'): ?>
               var curso = 'Barista o chef';
            <?php elseif ($user_type == 'colaborador'): ?>
               var curso = 'Colaborador Negocio de Café';
            <?php elseif ($user_type == 'comercial'): ?>
               var curso = 'Equipo comercial';
            <?php elseif ($user_type == 'administrator'): ?>
               var curso = 'Apasionados por el Café';
            <?php elseif ($user_type == 'business'): ?>
               var curso = 'Apasionados por el Café';
            <?php endif; ?>

            function showPublic(){document.getElementById("showpublic").innerHTML = "Soy: " + public};
            function showUser(){document.getElementById("username").innerHTML = "Estudiante: " + name_user};
            function showName(){document.getElementById("namediploma").innerHTML = name_user};
            function showDate(){document.getElementById("datediploma").innerHTML = fecha};
            function showCourse(){document.getElementById("curso").innerHTML = curso};

            setTimeout(function(){ showName(); }, 3000);
            setTimeout(function(){ showDate(); }, 3000);
            setTimeout(function(){ showCourse(); }, 3000);

            function printDiploma(diploma) {
                var contenido= document.getElementById(diploma).innerHTML;
                var contenidoOriginal= document.body.innerHTML;
                document.body.innerHTML = contenido;
                window.print();
                document.body.innerHTML = contenidoOriginal;
            }
</script>
<?php 
$typeDiplomaUser = "";

?>
<script src="<?php echo $assetUrl; ?>resources/assets/app/app.js"></script>
<script src="<?php echo $assetUrl; ?>resources/assets/app/controllers.js"></script>
<?php if ($user_type == 'aficionado'): $typeDiplomaUser = $assetUrl.'resources/assets/img/diploma/Diploma_aficionado.png'; ?>
    <script src="<?php echo $assetUrl; ?>resources/assets/app/services-aficionado.js"></script>
    
<?php elseif ($user_type == 'emprendedor'):  $typeDiplomaUser = $assetUrl.'resources/assets/img/diploma/Diploma-apasionados-cafe_03.jpg'; ?>
    <script src="<?php echo $assetUrl; ?>resources/assets/app/services-emprendedor.js"></script>
<?php elseif ($user_type == 'barista-chef'): $typeDiplomaUser = $assetUrl.'resources/assets/img/diploma/Diploma_barista_chef.png'; ?>
    <script src="<?php echo $assetUrl; ?>resources/assets/app/services-barista-chef.js"></script>
<?php elseif ($user_type == 'colaborador'): $typeDiplomaUser = $assetUrl.'resources/assets/img/diploma/Diploma_colaborador.png'; ?>
    <script src="<?php echo $assetUrl; ?>resources/assets/app/services-colaborador-nutresa.js"></script>
<?php elseif ($user_type == 'comercial'): $typeDiplomaUser = $assetUrl.'resources/assets/img/diploma/Diploma_comercial.png'; ?>
    <script src="<?php echo $assetUrl; ?>resources/assets/app/services-comercial-nutresa.js"></script>
<?php elseif ($user_type == 'administrator'): $typeDiplomaUser = $assetUrl.'resources/assets/img/diploma/Diploma-apasionados-cafe_03.jpg'; ?>
    <script src="<?php echo $assetUrl; ?>resources/assets/app/services.js"></script>
<?php elseif ($user_type == 'business'): $typeDiplomaUser = $assetUrl.'resources/assets/img/diploma/Diploma-apasionados-cafe_03.jpg';?>
  <script src="<?php echo $assetUrl; ?>resources/assets/app/services.js"></script>
<?php endif; ?>

<body ng-app="scormmomentos" class="home" >
    <section class="curso">
        <!-- Main view for layout -->
        <?php if( !is_user_logged_in() ): ?>
            <a href="/inicio-de-sesion/" >Para ingresar el curso debes iniciar sesion</a>
            <?php 
                // wp_redirect( '/inicio-de-sesion/' );
                // exit;
            ?>
        <?php else: ?>
            <div ui-view="main"></div>
        <?php endif; ?>
        <!-- Descargar Diploma -->
        <div id="areaImprimir" style="display: none;">
           <table width="800" border="0" align="center" cellpadding="0" cellspacing="0">
              <tbody>
                 <tr>
                    <td><img style="display:block; border:0; padding:0; margin:0;" src="<?php echo $assetUrl; ?>resources/assets/img/diploma/Diploma-apasionados-cafe_01.jpg" width="800"></td>
                 </tr>
                 <tr>
                    <td>
                       <table width="800" border="0" cellspacing="0" cellpadding="0">
                          <tbody>
                             <tr>                        
                                <td height="45" style="background: url('<?php echo $assetUrl; ?>resources/assets/img/diploma/Diploma-apasionados-cafe_02.jpg'); background-color: #f4f4f4; text-align:center; color:#4a2722; font-family:Arial, Helvetica, sans-serif; font-size:22px; text-transform: uppercase;"><strong id="namediploma"></strong></td>                        
                             </tr>
                          </tbody>
                       </table>
                    </td>
                 </tr>
                 <tr>
                    <td><img style="display:block; border:0; padding:0; margin:0;" src="<?php echo  $typeDiplomaUser; ?>" width="800"></td>
                 </tr>
                 <tr>
                    <td>
                       <table width="800" border="0" cellspacing="0" cellpadding="0">
                          <tbody>
                             <tr>
                                <td><img style="display:block; border:0; padding:0; margin:0;" src="<?php echo $assetUrl; ?>resources/assets/img/diploma/Diploma-apasionados-cafe_04.jpg" width="538"></td>
                                <!-- <td><img style="display:block; border:0; padding:0; margin:0;" src="<?php echo $assetUrl; ?>resources/assets/img/diploma/Diploma-apasionados-cafe_05.jpg" width="143"></td> -->
                                <td width="143" style="background: url('<?php echo $assetUrl; ?>resources/assets/img/diploma/Diploma-apasionados-cafe_05.jpg'); background-size: contain; background-color: #f4f4f4; font-family: Arial, Helvetica, sans-serif; text-align: center;"><span style="font-size:15px;color:#4a2722 !important;display: block; background-color: #e4e4e3; border-radius: 30px;" width="143" id="datediploma"></span></td>
                                <td><img style="display:block; border:0; padding:0; margin:0;" src="<?php echo $assetUrl; ?>resources/assets/img/diploma/Diploma-apasionados-cafe_06.jpg" width="119"></td>                     
                             </tr>
                          </tbody>
                       </table>
                    </td>
                 </tr>
                 <tr>
                    <td><img style="display:block; border:0; padding:0; margin:0;" src="<?php echo $assetUrl; ?>resources/assets/img/diploma/Diploma-apasionados-cafe_07.jpg" width="800"></td>
                 </tr>
              </tbody>
           </table>
        </div>
        <!-- Fin descargar diploma -->
    </section>
</body>

    