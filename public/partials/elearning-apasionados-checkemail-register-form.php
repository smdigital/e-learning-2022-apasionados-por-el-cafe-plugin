<?php 
$assetUrl = plugin_dir_url(__DIR__); 
$homeUrl = home_url();  
?>


<section id="register">
    <main class="login-view">
        <div class="login">
            <img class="banner" src="<?php echo $assetUrl?>reources/assets/images/registro-mobile-new.png" alt="Bienvenidos Apasionados por el café">
            <div class="info-verify">
                {% if(message) %}
                    <p>{{ message }}</p>
                {% else %}
                    <p>Gracias por registrarte. <br>Por favor verifica tu correo de registro para confirmar tu cuenta.</p>
                {% endif %}

                <hr>
            </div>
        </div>
    </main>
</section>