<?php 
$assetUrl = plugin_dir_url(__DIR__); 
$homeUrl = home_url();  
?>
<section id="register">
    <section class="marcas-login inline">
        <figure class="umbrella">
            <a href="/">
                <img src="<?php echo $assetUrl; ?>resources/assets/img/icon/logo-apasionado-por-el-cafe.jpg" alt="">
            </a>
        </figure>
        <figure>
            <a href="http://www.ponletuselloalavida.com/" target="_blank">
                <img src="<?php echo $assetUrl; ?>resources/assets/img/icon/nuestras-marcas-cafe-sello-rojo-small.png" alt="">
            </a>
        </figure>
        <figure>
            <a href="http://www.cafelabastilla.com/" target="_blank">
                <img src="<?php echo $assetUrl; ?>resources/assets/img/icon/nuestras-marcas-cafe-la-bastilla-small.png" alt="">
            </a>
        </figure>
        <figure>
            <a href="http://www.colcafe.com/" target="_blank">
                <img src="<?php echo $assetUrl; ?>resources/assets/images/logo-marca-colcafe-3.png" alt="">
            </a>
        </figure>
        <figure>
            <a href="http://cafematiz.com/" target="_blank">
                <img src="<?php echo $assetUrl; ?>resources/assets/img/icon/nuestras-marcas-matiz-small.png" alt="">
            </a>
        </figure>
        <figure class="">
            <a href="https://capsulasexpressnutresa.com" target="_blank">
            <img src="https://apasionados-por-el-cafe.s3.amazonaws.com/wp-content/uploads/2023/12/logo-centto-apasionados-por-el-cafe.png" alt="logo centto">
            </a>
        </figure>
    </section>
    <main class="login-view">
        <div class="bg-footer-login">
            <div class="login">
                <h2>Inicio de sesión</h2>
                <form id="loginForm" class="" action="<?php echo wp_login_url(); ?>" method="POST" novalidate data-parsley-validate>
                    <div class="column-grid">
                        <div class="cont-login">
                            <i class="icon-email"></i>
                            <input type="email" name="log" id="user_login" value="" placeholder="Correo electrónico" required>
                        </div>
                        <div class="cont-login">
                            <i class="icon-password"></i>
                            <input type="password" name="pwd" id="user_pass" value="" placeholder="Contraseña" required>
                        </div>
                    </div>
                    <input type="submit" class="submit" name="" value="Iniciar sesión">
                    <a class="forget" href="/recordar-datos"> Olvidé mi contraseña</a>
                    
                    <?php if ( count( $attributes['errors'] ) > 0 ) : ?>
                    <div class="col-lg-12">
                    <?php if( $attributes['errors'] && count($attributes['errors']) > 0 ) { ?>
    					<div class="form--error">
							<?php foreach($attributes['errors'] as $error){
									echo "<span class='error'>$error</span><br />";
							}?>
    					</div>
    				<?php } ?>
                    </div>
                <?php endif; ?>
                    <a class="register-link register-radius" href="/registro">Si no estás registrado, Regístrate aquí</a>
                </form>
            </div>
        </div>
    </main>
    <?php //var_dump( $attributes );  ?>
</section>
   