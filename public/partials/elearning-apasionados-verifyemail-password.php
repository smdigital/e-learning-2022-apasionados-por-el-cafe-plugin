<?php 
$assetUrl = plugin_dir_url(__DIR__);
$homeUrl = home_url();   
?>

<section id="register">
    <main class="login-view">
        <div class="login">
            <img class="banner" src="<?php echo $assetUrl; ?>resources/assets/images/registro-mobile-new.png" alt="Bienvenidos Apasionados por el café">
            <div class="info-verify">
                <p>Verifica tu correo electrónico y haz clic en el enlace para restablecer la contraseña</p>
                <hr>
                <a class="forget" href="/recordar-datos"> Enviar de nuevo el correo</a>
                <br>
                <a href="/inicio-de-sesion">Volver al inicio de sesión</a>
                <br>
                <span class="register">Si no estás registrado, <a class="register-link" href="/registro">Regístrate aquí</a></span>
            </div>
        </div>
    </main>
</section>