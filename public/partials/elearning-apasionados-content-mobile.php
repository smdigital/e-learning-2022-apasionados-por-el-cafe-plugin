<?php 
$assetUrl = plugin_dir_url(__DIR__);  
$homeUrl = home_url();  
?>
<!-- Scripts angular -->
<link rel="stylesheet" href="<?php echo $assetUrl; ?>resources/assets/css/normalize.css">
<link rel="stylesheet" href="<?php echo $assetUrl; ?>resources/assets/includes/widgets/bootstrap-3/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo $assetUrl; ?>resources/assets/includes/widgets/uniform/uniform.css">
<link rel="stylesheet" href="<?php echo $assetUrl; ?>resources/assets/bower_components/intro.js/minified/introjs.min.css">
<link rel="stylesheet" href="<?php echo $assetUrl; ?>resources/assets/bower_components/videogular-themes-default/videogular.min.css">
<link rel="stylesheet" href="<?php echo $assetUrl; ?>resources/assets/bower_components/slick-carousel/slick/slick.css">
<link rel="stylesheet" href="<?php echo $assetUrl; ?>resources/assets/bower_components/slick-carousel/slick/slick-theme.css">
<!-- <link rel="stylesheet" href="<?php echo $assetUrl; ?>resources/assets/css/style.css"> -->
<link rel="stylesheet" href="<?php echo $assetUrl; ?>css/style-mobile.css">

<script src="<?php echo $assetUrl; ?>resources/assets/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo $assetUrl; ?>resources/assets/bower_components/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo $assetUrl; ?>resources/assets/includes/widgets/bootstrap-3/js/bootstrap.min.js"></script>
<script src="<?php echo $assetUrl; ?>resources/assets/includes/widgets/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo $assetUrl; ?>resources/assets/bower_components/intro.js/minified/intro.min.js"></script>
<script src="<?php echo $assetUrl; ?>resources/assets/js/main.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.5/handlebars.min.js"></script>

<script src="<?php echo $assetUrl; ?>resources/assets/bower_components/angular/angular.min.js"></script>
<script src="<?php echo $assetUrl; ?>resources/assets/bower_components/angular-intro.js/build/angular-intro.min.js"></script>
<script src="<?php echo $assetUrl; ?>resources/assets/bower_components/slick-carousel/slick/slick.js"></script>
<script src="<?php echo $assetUrl; ?>resources/assets/bower_components/angular-slick-carousel/dist/angular-slick.min.js"></script>
<script src="<?php echo $assetUrl; ?>resources/assets/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
<script src="<?php echo $assetUrl; ?>resources/assets/bower_components/angular-sanitize/angular-sanitize.min.js"></script>
<script src="<?php echo $assetUrl; ?>resources/assets/bower_components/angular-resource/angular-resource.min.js"></script>
<script src="<?php echo $assetUrl; ?>resources/assets/bower_components/angular-ui-router/release/angular-ui-router.min.js"></script>
<script src="<?php echo $assetUrl; ?>resources/assets/bower_components/videogular/videogular.min.js"></script>
<script src="<?php echo $assetUrl; ?>resources/assets/bower_components/videogular-controls/vg-controls.min.js"></script>
<!-- Scripts angular -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dom-to-image/2.6.0/dom-to-image.js"></script>

<script type="text/javascript">
    var assets_url = "<?php echo $assetUrl; ?>";
    var home_url = "<?php echo $homeUrl; ?>";
    <?php 
    if( is_array( $attributes['learner'] ) ):

    $lesson = array_pop($attributes['learner']);
    $section = array_pop($attributes['learner']);
    $evaluations = $attributes['learner'];
    
    $tests = [];
    foreach ($evaluations as $test ) {
        $note = doubleval($test->note);
        array_push($tests, $note);
    }
    
    $scorm_data = json_encode(array(
        'lesson'      => intval($lesson->lesson),
        'section'     => intval($section->section),
        'evaluations' => $tests,
    )); 
    endif;
    
?>
    var data = '<?php echo $scorm_data; ?>';

    //data = data.replace(/&quot;/g,'"');

    var scorm_data = data;
    <?php 
        $user = wp_get_current_user();
        $user_id = get_user_meta( $user->ID );
        $user_type = $user_id['user_type'][0];
        
        $email_user = $user->user_email;
        $name_user = $user->display_name; 
        function my_has_role($user, $role) {
            $roles = $user->roles; 
            return in_array($role, (array) $user->roles);
          }
          
          if(my_has_role($user, 'administrator')) {
            $user_type = 'administrator';
          }
    ?>
    var email_user = "<?php echo $email_user; ?>";
    var name_user = "<?php echo $name_user; ?>";
    var id_user = "<?php echo $user_id; ?>";
    var public = "<?php echo $user_type; ?>";


    <?php if ($results != null): ?>
            /* Fecha obtenida desde la BD, fecha de finalizacion del curso */
            var fecha = new Date(" {{  results[0].updated_at  }} ");
            fecha = fecha.getDate() + " | " + (fecha.getMonth()+1)+ " | " + fecha.getFullYear();
           <?php else: ?>
            /*Fecha en caso de estar vacia imprime la fecha del dia actual */
            var fecha = new Date();
            fecha = fecha.getDate() + " | " +(fecha.getMonth()+1) + " | " +fecha.getFullYear();
            <?php endif; ?>
           


            <?php if ($user_type == 'aficionado'): ?>
               var curso = 'Aficionado';
            <?php elseif ($user_type == 'emprendedor'): ?>
               var curso = 'Emprendedor';
            <?php elseif ($user_type == 'barista-chef'): ?>
               var curso = 'Barista o chef';
            <?php elseif ($user_type == 'colaborador'): ?>
               var curso = 'Colaborador Negocio de Café';
            <?php elseif ($user_type == 'comercial'): ?>
               var curso = 'Equipo comercial';
            <?php elseif ($user_type == 'administrator'): ?>
               var curso = 'Apasionados por el Café';
            <?php endif; ?>
</script>

<script src="<?php echo $assetUrl; ?>resources/assets/app/app-mobile.js"></script>
<script src="<?php echo $assetUrl; ?>resources/assets/app/controllers-mobile.js"></script>
<?php if ($user_type == 'aficionado'): ?>
    <script src="<?php echo $assetUrl; ?>resources/assets/app/services-aficionado.js"></script>
<?php elseif ($user_type == 'emprendedor'): ?>
    <script src="<?php echo $assetUrl; ?>resources/assets/app/services-emprendedor.js"></script>
<?php elseif ($user_type == 'barista-chef'): ?>
    <script src="<?php echo $assetUrl; ?>resources/assets/app/services-barista-chef.js"></script>
<?php elseif ($user_type == 'colaborador'): ?>
    <script src="<?php echo $assetUrl; ?>resources/assets/app/services-colaborador-nutresa.js"></script>
<?php elseif ($user_type == 'comercial'): ?>
    <script src="<?php echo $assetUrl; ?>resources/assets/app/services-comercial-nutresa.js"></script>
<?php elseif ($user_type == 'administrator'): ?>
    <script src="<?php echo $assetUrl; ?>resources/assets/app/services.js"></script>
<?php endif; ?>

<body ng-app="scormmomentos" class="home">
    <!-- Main view for layout -->
    <div ui-view="main"></div>

    <script type="text/javascript">

        function callCloseEvent() {
            jQuery(".capitulos-nav").on("click", function() {
                jQuery("#hamburger").toggleClass("is-active");
                jQuery(".capitulos-nav").toggleClass("is-active");
            });
            
        }

        // Solución temporal, se bloquea cuando se invoca un video y sale error de jQuery con evento play()
        jQuery(document).on("ready", function() {
            setTimeout(function () {
                jQuery("#hamburger").on("click", function() {
                    jQuery(this).toggleClass("is-active");
                    jQuery(".capitulos-nav").toggleClass("is-active");
                    // callCloseEvent();
                });
            }, 3000);
        });
    </script>
</body>

    