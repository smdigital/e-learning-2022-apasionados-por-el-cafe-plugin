<?php 
$assetUrl = plugin_dir_url(__DIR__);
$homeUrl = home_url();  
?>
<section id="register" class="lost-password">
    <section class="marcas-login inline">
        <figure class="umbrella">
            <a href="/">
                <img src="<?php echo $assetUrl; ?>resources/assets/img/icon/logo-apasionado-por-el-cafe.jpg" alt="">
            </a>
        </figure>
        <figure>
            <a href="http://www.ponletuselloalavida.com/" target="_blank">
                <img src="<?php echo $assetUrl; ?>resources/assets/img/icon/nuestras-marcas-cafe-sello-rojo-small.png" alt="">
            </a>
        </figure>
        <figure>
            <a href="http://www.cafelabastilla.com/" target="_blank">
                <img src="<?php echo $assetUrl; ?>resources/assets/img/icon/nuestras-marcas-cafe-la-bastilla-small.png" alt="">
            </a>
        </figure>
        <figure>
            <a href="http://www.colcafe.com/" target="_blank">
                <img src="<?php echo $assetUrl; ?>resources/assets/images/logo-marca-colcafe-3.png" alt="">
            </a>
        </figure>
        <figure>
            <a href="http://cafematiz.com/" target="_blank">
                <img src="<?php echo $assetUrl; ?>resources/assets/img/icon/nuestras-marcas-matiz-small.png" alt="">
            </a>
        </figure>
        <figure class="">
            <a href="https://capsulasexpressnutresa.com" target="_blank">
                <img src="<?php echo $assetUrl; ?>resources/assets/img/icon/cen-nutresa-icon.png" alt="marcas banner">
            </a>
        </figure>
    </section>
    <main class="login-view">
        <div class="login">
            <h2>Recuperar contraseña</h2>
            <form class="recovery" action="<?php echo wp_lostpassword_url(); ?>" method="POST">
                <input type="email"  name="user_login" id="user_login" value="" placeholder="Correo electrónico">
                <input type="submit" class="submit" name="" value="Recordar datos">
                
                <a class="forget" href="/inicio-de-sesion"> Iniciar sesión</a>
                <?php if ( count( $attributes['errors'] ) > 0 ) : ?>
                    <div class="col-lg-12">
                        <?php foreach ( $attributes['errors'] as $error ) : ?>
                            <span class="error"><?php echo $error; ?></span>
                        <?php endforeach; ?>
                    </div>
            <?php endif; ?>
                <a class="register-link register-radius" href="/registro">Si no estás registrado, Regístrate aquí</a>
            </form>
        </div>
    </main>
</section>
   