
<?php  
$assetUrl = plugin_dir_url(__DIR__); 
$homeUrl = home_url();  
?>

<section id="register" class="form-elearning">
    <section class="marcas-login inline">
        <figure class="umbrella">
            <a href="/">
                <img src="<?php echo $assetUrl; ?>resources/assets/img/icon/logo-apasionado-por-el-cafe.jpg" alt="">
            </a>
        </figure>
        <figure>
            <a href="http://www.ponletuselloalavida.com/" target="_blank">
                <img src="<?php echo $assetUrl; ?>resources/assets/img/icon/nuestras-marcas-cafe-sello-rojo-small.png" alt="">
            </a>
        </figure>
        <figure>
            <a href="http://www.cafelabastilla.com/" target="_blank">
                <img src="<?php echo $assetUrl; ?>resources/assets/img/icon/nuestras-marcas-cafe-la-bastilla-small.png" alt="">
            </a>
        </figure>
        <figure>
            <a href="http://www.colcafe.com/" target="_blank">
                <img src="<?php echo $assetUrl; ?>resources/assets/images/logo-marca-colcafe-3.png" alt="">
            </a>
        </figure>
        <figure>
            <a href="http://cafematiz.com/" target="_blank">
                <img src="<?php echo $assetUrl; ?>resources/assets/img/icon/nuestras-marcas-matiz-small.png" alt="">
            </a>
        </figure>
        <figure class="">
            <a href="https://capsulasexpressnutresa.com" target="_blank">
                <img src="<?php echo $assetUrl; ?>resources/assets/img/icon/cen-nutresa-icon.png" alt="marcas banner">
            </a>
        </figure>
    </section>
    <main class="register-view">
        <div class="login">
            <a href="/inicio-de-sesion/" class="back-login">Regresar a iniciar sesión</a>
            <h3>Registrate</h3>
            <div class="clr"></div>
            <form id="registerForm" class="" action="<?php echo wp_registration_url(); ?>" method="POST" autocomplete="on" novalidate data-parsley-validate>
                <div class="column-grid">
                    <div class="cont-input">
                        <input type="text" name="full_name" value="<?php echo !empty($_GET['full_name']) ? $_GET['full_name']:'' ?>" placeholder="Nombre y Apellidos" required>
                    </div>
                    <div class="cont-input">
                        <select class="" name="user_type" required>
                            <option value="">Tipo de usuario</option>
                            <option value="aficionado" <?php echo (!empty($_GET['user_type']) && $_GET['user_type'] == 'aficionado') ? 'selected':null ?>>Aficionado</option>
                            <option value="emprendedor" <?php echo (!empty($_GET['user_type']) && $_GET['user_type'] == 'emprendedor') ? 'selected':null ?>>Emprendedor</option>
                            <option value="barista-chef" <?php echo (!empty($_GET['user_type']) && $_GET['user_type'] == 'barista-chef') ? 'selected':null ?>>Barista o Chef</option>
                            <option value="colaborador" <?php echo (!empty($_GET['user_type']) && $_GET['user_type'] == 'colaborador') ? 'selected':null ?>>Soy colaborador del Negocio Café del Grupo Nutresa</option>
                            <option value="comercial" <?php echo (!empty($_GET['user_type']) && $_GET['user_type'] == 'comercial') ? 'selected':null ?>>Soy del Equipo Comercial de Grupo Nutresa</option>
                        </select>
                    </div>
                </div>
                <div class="column-grid">
                    <div class="cont-input">
                        <input type="number" name="phone" value="<?php echo !empty($_GET['phone']) ? $_GET['phone']:'' ?>" placeholder="Teléfono" required>
                    </div>
                    <div class="cont-input">
                        <select id="country" class="" name="country" required>
                            <option value="">País</option>
                            <?php 
                            /** 
                             * Get country data
                            */
                            if( is_array($attributes['countries']) && count($attributes['countries']) > 0 ): 
                             foreach ($attributes['countries'] as $country) : ?>
                            <option value="<?php echo $country->id; ?>"><?php echo $country->name; ?></option>
                            <?php endforeach; endif;?>
                        </select>
                    </div>
                </div>
                <div class="column-grid">
                    <div class="cont-input">
                        <select id="state" class="" name="state" required>
                            <option value="">Departamento</option>
                            <?php 
                            /** 
                             * Get department data
                            */
                            if( is_array($attributes['states']) && count($attributes['states']) > 0 ): 
                             foreach ($attributes['states'] as $state) : ?>
                            <option value="<?php echo $state->id; ?>"><?php echo $state->name; ?></option>
                            <?php endforeach; endif;?>
                        </select>
                    </div>
                    <div class="cont-input">
                        <select id="city" class="" name="city">
                            <option value="">Ciudad</option>
                        </select>
                    </div>
                </div>
                <div class="column-grid">
                    <div class="cont-input">
                        <input type="email" name="email" value="<?php echo !empty($_GET['email']) ? $_GET['email']:'' ?>" placeholder="Correo electrónico" required>
                    </div>
                    <div class="cont-input">
                        <input type="password" name="password" value="<?php echo !empty($_GET['password']) ? $_GET['password']:'' ?>" placeholder="Contraseña" required>
                    </div>
                </div>

                <div class="cont-input terms main-terms">
                    <input type="checkbox" id="terms" name="terms" value="1" required="" <?php echo !empty($_GET['terms']) ? 'checked':'' ?> data-parsley-errors-container="#checkbox-errors">
                    <label for="terms" class="terms-label">Autorizo el tratamiento de mis datos personales según <a href="/habeas-data/" target="_blank">HABEAS DATA</a
                    label>
                    <div id="checkbox-errors"></div>
                </div>
                <div class="clr"></div>
                <div class="center-flex-form">                    
                    <input type="hidden" name="crfapasionados" value="<?php echo wp_create_nonce('elearning-register-nonce'); ?>"/>
                    <input type="submit" class="submit" name="" value="Regístrate aquí">
                </div>                
                <div class="main-iniciar">
                    <h6>¿Ya tienes una cuenta?</h6>
                    <div class="clr"></div>
                    <a class="login-links register-radius" href="/inicio-de-sesion">Iniciar sesión</a>
                </div>
                <?php  
                /** 
                 * Get error data
                */
                if ( is_array($attributes['errors']) && count( $attributes['errors'] ) > 0 ) : ?>
                    <div class="col-lg-12">
                        <?php foreach ( $attributes['errors'] as $error ) : ?>
                            <span class="error"><?php echo $error; ?></span>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </form>
        </div>
    </main>
</section>