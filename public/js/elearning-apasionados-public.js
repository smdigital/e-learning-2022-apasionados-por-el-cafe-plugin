(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

	 jQuery(document).ready(function($) {
		 if(jQuery('.elearning-layout').length > 0 ) {
		var ajaxurl = ajax_elearning.url;}
		//const ajaxnonce = ajax_elearning.nonce;
		/**
     * new countries, cities, states
     */
    
     $("#country").on("change", function() {
      var country_code = $(this).val();
      var options = '<option value="">Departamento</option>';
      $.get(ajaxurl, {'action': 'get_states_mycountrie', 'country_id': country_code}, function(response) {		
        
          console.log('pasa');
          console.log(response);
         var states = $.parseJSON(response.data);
        console.log(states);
        var len = states.length;
        for (var i = 0;  i < len; i++) {
          options += '<option value="' + states[i].id + '">' + states[i].name + '</option>';
        }
        $.each(states, function(idx, el) {
         
        });
        $("#state").html(options);  
        
        
      }); 
    }); 
		// if( $('#register').length > 0 ) {
			$("#state").on("change", function() {
				var department_code = $(this).val();
				var options = '<option value="">Ciudad</option>';
				$.get(ajaxurl, {'action': 'get_cities_mycities', 'state_id': department_code}, function(response) {		
					
					// var cities = $.parseJSON(response.data);
					var cities = $.parseJSON(response.data);
					// console.log(cities);
					var len = cities.length;
					for (var i = 0;  i < len; i++) {
						options += '<option value="' + cities[i].name + '">' + cities[i].name + '</option>';
					}
					$.each(cities, function(idx, el) {
						// console.log(el);
					});
					$("#city").html(options); 
					
				}); 
			});
		// }
		$('#registerForm').parsley();
		$('#loginForm').parsley();
		Parsley.addMessages('es', {
			defaultMessage: "Este valor parece ser inválido.",
			type: {
			  email:        "Este valor debe ser un correo válido.",
			  url:          "Este valor debe ser una URL válida.",
			  number:       "Este valor debe ser un número válido.",
			  integer:      "Este valor debe ser un número válido.",
			  digits:       "Este valor debe ser un dígito válido.",
			  alphanum:     "Este valor debe ser alfanumérico."
			},
			notblank:       "Este valor no debe estar en blanco.",
			required:       "Este valor es requerido.",
			pattern:        "Este valor es incorrecto.",
			min:            "Este valor no debe ser menor que %s.",
			max:            "Este valor no debe ser mayor que %s.",
			range:          "Este valor debe estar entre %s y %s.",
			minlength:      "Este valor es muy corto. La longitud mínima es de %s caracteres.",
			maxlength:      "Este valor es muy largo. La longitud máxima es de %s caracteres.",
			length:         "La longitud de este valor debe estar entre %s y %s caracteres.",
			mincheck:       "Debe seleccionar al menos %s opciones.",
			maxcheck:       "Debe seleccionar %s opciones o menos.",
			check:          "Debe seleccionar entre %s y %s opciones.",
			equalto:        "Este valor debe ser idéntico."
		  });
		  Parsley.setLocale('es');
	});
 

})( jQuery );