angular.module('scormmomentos', [
    'ui.router',
    'angular-intro',
    'ngResource',
    'ui.bootstrap',
    'slickCarousel',
    'scormmomentos.controllers',
    'scormmomentos.services',
])
.config([
    '$stateProvider',
    '$urlRouterProvider',
    function(
        $stateProvider,
        $urlRouterProvider
    ){
    $stateProvider
    // Layout state
    .state('app', {
        abstract: true,
        views: {
            'main': {
                templateUrl: assets_url + '/resources/assets/layouts/app-mobile.html',
                controller: 'AppCtrl'
            }
        }
    })
    // States for each module of the course
    .state('app.course', {
        url: "/",
        views: {
            'menu': {
                templateUrl: assets_url + "/resources/assets/pages/course-menu-mobile.html"
            },
            'progress': {
                templateUrl: assets_url + "/resources/assets/pages/course-progress-mobile.html"
            },
            'content': {
                templateUrl: assets_url + "/resources/assets/pages/course-content-mobile.html"
            }
        }
    })
    // Costeo
    .state('app.costeo', {
        url: "/costeo",
        views: {
            'menu': {
                templateUrl: "app/templates/pages/course-menu.html"
            },
            'progress': {
                templateUrl: "app/templates/pages/course-progress.html"
            },
            'content': {
                templateUrl: "app/templates/pages/costeo.html",
                controller: 'CosteoCtrl'
            }
        }
    })
    $urlRouterProvider.otherwise('/');
}])
// Filter html tags
.filter('convertState', function ($sce) {
    return function (state) {
        if (state == 1) {
            return $sce.trustAsHtml(state);
        }
        else {
            return $sce.trustAsHtml(state);
        }
    }
});;
