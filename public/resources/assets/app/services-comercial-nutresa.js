angular.module('scormmomentos.services', [])
.factory('Service',[
    '$q',
    '$resource',
    '$location',
    function(
        $q,
        $resource,
        $location
    ){
    return {
        public: function() {
            return [{
                "name": "comercial"
            }]
        },
        all : function() {
            return [{
                // MODULO 1
                "index": "0",
                "name": "Café y Cultura",
                "unique": "cultura",
                "time": "1h",
                "intro": false,
                "template": "intro-1",
                "content": {
                    "0": {
                        "index": "1.1",
                        "name": "Introducción Café y Cultura",
                        "image": "modulo1-1.png",
                        "time": "00:33",
                        "type": "1",
                        "content": "intro_cafe_y_cultura.mp4"
                    },
                    "1": {
                        "index": "1.2",
                        "name": "Historia del Café",
                        "image": "modulo1-1.png",
                        "time": "03:33",
                        "type": "1",
                        "content": "historia-interactiva-del-cafe.mp4"
                    },
                    "2": {
                        "index": "1.3",
                        "name": "El café en Colombia",
                        "image": "modulo1-2.png",
                        "time": "04:30",
                        "type": "3",
                        "content": [
                            "modulo-1/infografico-1.png",
                            "modulo-1/infografico-2.png",
                            "modulo-1/infografico-3.png",
                            "modulo-1/infografico-4.png",
                            "modulo-1/infografico-5.png",
                            "modulo-1/infografico-7.png",
                            "modulo-1/infografico-6.png",
                            "modulo-1/infografico-8.png",
                            "modulo-1/infografico-9.png"
                        ]
                    },
                    "3": {
                        "index": "1.4",
                        "name": "Proceso agrícola e industrial",
                        "image": "modulo2-1.png",
                        "time": "08:45",
                        "type": "1",
                        "content": "los-procesos-colcafe.mp4"
                    },
                    "4": {
                        "index": "1.5",
                        "name": "Proceso del café tostado y molido",
                        "image": "modulo2-3.png",
                        "time": "02:30",
                        "type": "3",
                        "content": [
                            "modulo-2/infografico-1.png",
                            "modulo-2/infografico-6.png",
                            "modulo-2/infografico-7.jpg",
                            "modulo-2/infografico-tostado-molido-3.png",
                            "modulo-2/infografico-tostado-molido-4.png",
                            "modulo-2/infografico-tostado-molido-5.png",
                            "modulo-2/infografico-8.jpg",
                            "modulo-2/infografico-5.png"
                        ]
                    },
                    "5": {
                        "index": "1.6",
                        "name": "Proceso del café soluble o instantáneo",
                        "image": "modulo2-5.png",
                        "time": "03:30",
                        "type": "3",
                        "content": [
                            "modulo-2/infografico-2-1.png",
                            "modulo-2/infografico-6.png",
                            "modulo-2/infografico-tostado-molido-6.jpg",
                            "modulo-2/infografico-ind-soluble-3.png",
                            "modulo-2/infografico-ind-soluble-4.png",
                            "modulo-2/infografico-ind-soluble-5.png",
                            "modulo-2/infografico-ind-soluble-6.png",
                            "modulo-2/infografico-ind-soluble-7.png",
                            "modulo-2/infografico-2-5-1.jpg",
                            "modulo-2/infografico-2-6.png"
                        ]
                    },
                    // "6": {
                    //     "index": "1.7",
                    //     "name": "Nuestros productos",
                    //     "image": "modulo2-4.png",
                    //     "time": "03:00",
                    //     "type": "3",
                    //     "content": [
                    //         "mac/mapa-sitio-intro.png",
                    //         "mac/mapa-sitio-1.jpg",
                    //         "mac/mac.jpeg",
                    //         "mac/mapa-sitio-4.jpg"
                    //     ]
                    // },
                    "6": {
                        "index": "1.7",
                        "name": "Nuestros productos",
                        "image": "modulo2-4.png",
                        "time": "03:00",
                        "type": "11",
                        "content": [
                                    "mac/mapa-sitio-intro.png",
                                    "mac/mapa-sitio-1.jpg",
                                    "mac/mac.jpeg",
                                    "mac/mapa-sitio-4.jpg"
                                ]
                    },
                    "7": {
                        "index": "1.8",
                        "name": "Claves para la preparación I",
                        "image": "modulo2-4.png",
                        "time": "01:00",
                        "type": "12",
                        "content": [
                            "ResumenCafe-intro.png",
                            "ResumenCafe-1.png",
                            "ResumenCafe-2.png",
                            "ResumenCafe-3.png",
                            "ResumenCafe-4.png",
                            "ResumenCafe-5.png",
                            "ResumenCafe-6.png",
                            /*  "ResumenCafe-7.png", */
                           /*  "mac/mac.jpeg", */
                            "ResumenCafe-8.png",
                        ]
                    },
                    "8": {
                        "index": "1.9",
                        "name": "Claves para la preparación II",
                        "image": "modulo2-6.png",
                        "time": "03:17",
                        "type": "1",
                        "content": "tips-preparar-un-buen-cafe.mp4"
                    },
                    "9": {
                        "index": "1.10",
                        "name": "Prueba tu conocimiento",
                        "titlemodulo": "Ejercicio de conocimiento de Café y Cultura",
                        "image": "modulo1-3.png",
                        "time": "11:00",
                        "type": "2",
                        "content": {
                            "type": "a",
                            "intro" : true,
                            "template" : "intro-1",
                            "questions": {
                                "0": {
                                    "numberquestions": "1",
                                    "description": "Se dice que el café se descubrió hace más de 1.300 años en:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-1/thumbnail-1-1.jpg",
                                            "infoquestions": "a. El eje cafetero en Colombia.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-1/thumbnail-1-2.jpg",
                                            "infoquestions": "b. El occidente de Brasil.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "2": {
                                            "description": "modulo-1/thumbnail-1-3.jpg",
                                            "infoquestions": "c. La ciudad de Kaffa en Etiopía (África).",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "3": {
                                            "description": "modulo-1/thumbnail-1-4.jpg",
                                            "infoquestions": "d. Los bares de café en Italia.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "1": {
                                    "numberquestions": "2",
                                    "description": "Existen diferentes tipos de arbustos de café, de los cuales las 2 especies más comerciales a nivel mundial son:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-1/thumbnail-2-1.jpg",
                                            "infoquestions": "a. Caturra y Bourbón.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-1/thumbnail-2-2.jpg",
                                            "infoquestions": "b. Arábica y Robusta.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "2": {
                                            "description": "modulo-1/thumbnail-2-3.jpg",
                                            "infoquestions": "c. Brasilera y Colombiana.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-1/thumbnail-2-4.jpg",
                                            "infoquestions": "d. Arabusta y Robica.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "2": {
                                    "numberquestions": "3",
                                    "description": "El café llega a Colombia:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-1/thumbnail-3-1.jpg",
                                            "infoquestions": "a. En el año 1.732, por los Santanderes, Orinoco y Costa Norte.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "1": {
                                            "description": "modulo-1/thumbnail-3-2.jpg",
                                            "infoquestions": "b. En el año 1.600, por el Amazonas.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "2": {
                                            "description": "modulo-1/thumbnail-3-3.jpg",
                                            "infoquestions": "c. En el año 1.900, por la Costa Pacífica.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-1/thumbnail-3-4.jpg",
                                            "infoquestions": "d. En el año 1.850, por Panamá.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "3": {
                                    "numberquestions": "4",
                                    "description": "El café que se produce en Colombia, pertenece a la especie:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-1/thumbnail-4-1.jpg",
                                            "infoquestions": "a. Robusta.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-1/thumbnail-4-2.jpg",
                                            "infoquestions": "b. Arábica.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "2": {
                                            "description": "modulo-1/thumbnail-4-3.jpg",
                                            "infoquestions": "c. 50% Arábica y 50% Robusta.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-1/thumbnail-4-4.jpg",
                                            "infoquestions": "d. 80% Arábica y 20% Robusta.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "4": {
                                    "numberquestions": "5",
                                    "description": "¿Cuántas familias de caficultores colombianos están detrás de una deliciosa taza de café?",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-1/thumbnail-5-1.jpg",
                                            "infoquestions": "a. 340.000",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-1/thumbnail-5-2.jpg",
                                            "infoquestions": "b. 1´000.000",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "2": {
                                            "description": "modulo-1/thumbnail-5-3.jpg",
                                            "infoquestions": "c. 250.000",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-1/thumbnail-5-4.jpg",
                                            "infoquestions": "d. Más de 500.000",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        }
                                    }
                                },
                                "5": {
                                    "numberquestions": "6",
                                    "description": "Uno de los secretos del mejor café suave del mundo, se basa en la recolección de granos: ",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-1/thumbnail-6-1.jpg",
                                            "infoquestions": "a. Pintones + maduros.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-1/thumbnail-6-2.jpg",
                                            "infoquestions": "b. Solo maduros.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "2": {
                                            "description": "modulo-1/thumbnail-6-3.jpg",
                                            "infoquestions": "c. Verdes + Maduros.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-1/thumbnail-6-4.jpg",
                                            "infoquestions": "d. Pintones + verdes.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "6": {
                                    "numberquestions": "7",
                                    "description": "El orden correcto del proceso detrás de una taza de café es: ",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-1/thumbnail-7-1.jpg",
                                            "infoquestions": "a. Cultivo, cosecha, beneficio, trilla, tostión, preparación.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "1": {
                                            "description": "modulo-1/thumbnail-7-2.jpg",
                                            "infoquestions": "b. Cultivo, cosecha, beneficio, tostión, trilla, preparación.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "2": {
                                            "description": "modulo-1/thumbnail-7-3.jpg",
                                            "infoquestions": "c. Cultivo, cosecha, trilla, beneficio, tostión, preparación.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-1/thumbnail-7-4.jpg",
                                            "infoquestions": "d. Cultivo, cosecha, trilla, tostión, beneficio, preparación.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "7": {
                                    "numberquestions": "8",
                                    "description": "Para preparar un delicioso café, es necesario tener en cuenta:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-1/thumbnail-8-1.jpg",
                                            "infoquestions": "a.Utilizar un café vencido, agua de color amarillo, una cafetera  oxidada y agregarle mucha agua para que quede clarito.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-1/thumbnail-8-2.jpg",
                                            "infoquestions": "b. Café fresco y bien conservado, cantidades de café y agua recomendadas, cafeteras e implementos limpios y preparar únicamente el que se va a  consumir de inmediato o  máximo en una hora. ",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "2": {
                                            "description": "modulo-1/thumbnail-8-3.jpg",
                                            "infoquestions": "c. Mantener el paquete abierto y preparar café para todo el día.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-1/thumbnail-8-4.jpg",
                                            "infoquestions": "d. Aplicarse perfume o loción, no lavarse las manos y recalentar el café.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "8": {
                                    "numberquestions": "9",
                                    "description": "Las variables clave para la preparación, forman la palabra CAFE, cuyo significado es: ",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-1/thumbnail-9-1.jpg",
                                            "infoquestions": "a. Cereza, Almendra Familias y El proceso de tostión.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-1/thumbnail-9-2.jpg",
                                            "infoquestions": "b. Colino, Almácigo, Fruto y El proceso de beneficio.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "2": {
                                            "description": "modulo-1/thumbnail-9-3.jpg",
                                            "infoquestions": "c. Café, Agua, Funcionamiento del equipo y Elaboración del café.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "3": {
                                            "description": "modulo-1/thumbnail-9-4.jpg",
                                            "infoquestions": "d. Colombia, Arábica, Fermentación y Exportaciones.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "9": {
                                    "numberquestions": "10",
                                    "description": "Para obtener un café aromático y de sabor intenso, la medida recomendada  para un pocillo tintero, si se prepara con café tostado y molido es: ",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-1/thumbnail-10-1.jpg",
                                            "infoquestions": "a. 1 cucharada sopera rasa (5 gramos).",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "1": {
                                            "description": "modulo-1/thumbnail-10-2.jpg",
                                            "infoquestions": "b. ½ cucharada sopera (3 gramos).",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "2": {
                                            "description": "modulo-1/thumbnail-10-3.jpg",
                                            "infoquestions": "c. 2 cucharadas soperas (12 gramos).",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-1/thumbnail-10-4.jpg",
                                            "infoquestions": "d. 1 cucharadita mediana  (3 gramos).",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "10": {
                                    "numberquestions": "11",
                                    "description": "Para obtener un café aromático y de sabor intenso, la medida recomendada para un pocillo tintero, si se prepara con café instantáneo es:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-1/thumbnail-11-1.jpg",
                                            "infoquestions": "a. 1 cucharadita pequeña colmada (1.5 gramos).",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "1": {
                                            "description": "modulo-1/thumbnail-11-2.jpg",
                                            "infoquestions": "b. ½ cucharada sopera (3 gramos).",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "2": {
                                            "description": "modulo-1/thumbnail-11-3.jpg",
                                            "infoquestions": "c. 2 cucharadas soperas (12 gramos).",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-1/thumbnail-11-4.jpg",
                                            "infoquestions": "d. 1 cucharadita mediana  (3 gramos).",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            {
                // MODULO 2
                "index": "1",
                "name": "Mercado del Café y Propuesta de Valor",
                "unique": "mercado-valor",
                "time": "4h",
                "content": {
                    "0": {
                        "index": "2.1",
                        "name": "Mercado del café en Colombia",
                        "image": "modulo8-1.png",
                        "time": "00:45",
                        "type": "10",
                        "content": {
                            "0": {
                                "type": "10-a",
                                "source": "mercado-del-cafe-propuesta-de-valor.mp4"
                            },
                            "1": {
                                "type": "10-c",
                                "source": [
                                    "modulo-8/infografico-1.png",
                                    "modulo-8/infografico-2.png",
                                    "modulo-8/infografico-3.png",
                                    "modulo-8/infografico-4.png",
                                    "modulo-8/infografico-5.png",
                                    "modulo-8/infografico-6.png",
                                    "modulo-8/infografico-7.png",
                                    "modulo-8/infografico-8.jpg",
                                    "modulo-8/infografico-9.png",
                                    "modulo-8/infografico-10.png",
                                    "modulo-8/infografico-11.jpg",
                                    "modulo-8/infografico-12.jpg"
                                ]
                            }
                        }
                    },
                    "1": {
                        "index": "2.2",
                        "name": "Propuesta de Valor Colcafé",
                        "image": "modulo8-2.png",
                        "time": "01:10",
                        "type": "10",
                        "content": {
                            "0": {
                                "type": "10-a",
                                "source": "colcafe-propuesta-de-valor.mp4"
                            },
                            "1": {
                                "type": "10-a",
                                "source": "colcafe-marcas-productos.mp4"
                            },
                            "2": {
                                "type": "10-c",
                                "source": [
                                    "modulo-8/propuesta-de-valor-intro.png",
                                    "modulo-8/propuesta-de-valor-1.jpg",
                                    "modulo-8/propuesta-de-valor-2.png",
                                    "modulo-8/propuesta-de-valor-3.png",
                                    "modulo-8/propuesta-de-valor-4.jpg",
                                    "modulo-8/propuesta-de-valor-5.jpg"
                                ]
                            }
                        }
                    },
                    "2": {
                        "index": "2.3",
                        "name": "Prueba tu conocimiento",
                        "titlemodulo": "Ejercicio de conocimiento Mercado del Café y Propuesta de Valor",
                        "image": "modulo8-3.png",
                        "time": "11:00",
                        "type": "2",
                        "content": {
                            "type": "a",
                            "intro" : true,
                            "template" : "intro-7",
                            "questions": {
                                "0": {
                                    "numberquestions": "1",
                                    "description": "De cada 10 hogares colombianos ¿Cuántos consumen café?:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-7/thumbnail-1-1.jpg",
                                            "infoquestions": "a. 2 de cada 10 hogares.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-7/thumbnail-1-2.jpg",
                                            "infoquestions": "b. 5 de cada 10 hogares.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "2": {
                                            "description": "modulo-7/thumbnail-1-3.jpg",
                                            "infoquestions": "c. 9 de cada 10 hogares.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "3": {
                                            "description": "modulo-7/thumbnail-1-4.jpg",
                                            "infoquestions": "d. 10 de cada 10 hogares.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "1": {
                                    "numberquestions": "2",
                                    "description": "De cada 10 tazas que consumen los colombianos, ¿Cuántas se preparan con café molido y cuántas con café instantáneo?:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-7/thumbnail-2-1.jpg",
                                            "infoquestions": "a. 8 con café molido y 2 con café instanáneo.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-7/thumbnail-2-2.jpg",
                                            "infoquestions": "b. 6 con café molido y 4 con café instantáneo.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "2": {
                                            "description": "modulo-7/thumbnail-2-3.jpg",
                                            "infoquestions": "c.4 con café molido  y 6 con café instantáneo.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-7/thumbnail-2-4.jpg",
                                            "infoquestions": "d. 1 con café molido y 9 con café instanáneo.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "2": {
                                    "numberquestions": "3",
                                    "description": "Los momentos de mayor consumo de café en el día son:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-7/thumbnail-3-1.jpg",
                                            "infoquestions": "a. Antes del desayuno  y en el desayuno.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "1": {
                                            "description": "modulo-7/thumbnail-3-2.jpg",
                                            "infoquestions": "b. En el almuerzo.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "2": {
                                            "description": "modulo-7/thumbnail-3-3.jpg",
                                            "infoquestions": "c. Después de la cena.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-7/thumbnail-3-4.jpg",
                                            "infoquestions": "d. Ninguna de las  anteriores.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "3": {
                                    "numberquestions": "4",
                                    "description": "La mayor parte de los hogares colombianos prepara el café molido en:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-7/thumbnail-4-1.jpg",
                                            "infoquestions": "a. Cafetera por goteo.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-7/thumbnail-4-2.jpg",
                                            "infoquestions": "b. Olla u olleta.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "2": {
                                            "description": "modulo-7/thumbnail-4-3.jpg",
                                            "infoquestions": "c. Cafetera de prensa  francesa.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-7/thumbnail-4-4.jpg",
                                            "infoquestions": "d. Cafetera italiana.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "4": {
                                    "numberquestions": "5",
                                    "description": "La mayor parte de los hogares colombianos, prepara el café molido utilizando la siguiente cantidad de café por pocillo:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-7/thumbnail-5-1.jpg",
                                            "infoquestions": "a. 5 gramos  (como lo  indica la cantidad  recomendada).",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-7/thumbnail-5-2.jpg",
                                            "infoquestions": "b. 8 gramos (un poco más café que la cantidad recomendada).",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "2": {
                                            "description": "modulo-7/thumbnail-5-3.jpg",
                                            "infoquestions": "c. 4 gramos (menos café que la cantidad  recomendada).",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "3": {
                                            "description": "modulo-7/thumbnail-5-4.jpg",
                                            "infoquestions": "d. 10 gramos (el doble  de la cantidad  recomendada).",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "5": {
                                    "numberquestions": "6",
                                    "description": "Los 2 métodos que los hogares colombianos  prefieren para preparar café instantáneo son:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-7/thumbnail-6-1.jpg",
                                            "infoquestions": "a. Cafetera por goteo  y cafetera de  prensa francesa.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-7/thumbnail-6-2.jpg",
                                            "infoquestions": "b. Olla (u olleta) y pocillo.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "2": {
                                            "description": "modulo-7/thumbnail-6-3.jpg",
                                            "infoquestions": "c. Termo y cafetera institucional por goteo.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-7/thumbnail-6-4.jpg",
                                            "infoquestions": "d. Cafetera italiana.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "6": {
                                    "numberquestions": "7",
                                    "description": "¿Qué porcentaje (%) de colombianos endulza el café para consumirlo?:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-7/thumbnail-7-1.jpg",
                                            "infoquestions": "a. Menos del 20%.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-7/thumbnail-7-2.jpg",
                                            "infoquestions": "b.50%.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "2": {
                                            "description": "modulo-7/thumbnail-7-3.jpg",
                                            "infoquestions": "c. Más del 80%.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "3": {
                                            "description": "modulo-7/thumbnail-7-4.jpg",
                                            "infoquestions": "d. Ninguna de las  anteriores.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "7": {
                                    "numberquestions": "8",
                                    "description": "Durante los últimos años, el consumo  de café por fuera del hogar:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-7/thumbnail-8-1.jpg",
                                            "infoquestions": "a. Se ha incrementado.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "1": {
                                            "description": "modulo-7/thumbnail-8-2.jpg",
                                            "infoquestions": "b.Se ha mantenido constante.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "2": {
                                            "description": "modulo-7/thumbnail-8-3.jpg",
                                            "infoquestions": "c. Ha disminuido.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-7/thumbnail-8-4.jpg",
                                            "infoquestions": "d. Se ha eliminado.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "8": {
                                    "numberquestions": "9",
                                    "description": "Los lugares en que los colombianos consumen café fuera del hogar son:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-7/thumbnail-9-1.jpg",
                                            "infoquestions": "a. Cafeterías, tiendas  y panaderías.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-7/thumbnail-9-2.jpg",
                                            "infoquestions": "b. Casa de otros familiares o amigos.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "2": {
                                            "description": "modulo-7/thumbnail-9-3.jpg",
                                            "infoquestions": "c. Oficina, trabajo o vendedor ambulante.",
                                            "correct": false,
                                            "feedback": "Correcta"
                                        },
                                        "3": {
                                            "description": "modulo-7/thumbnail-9-4.jpg",
                                            "infoquestions": "d. Todas las anteriores.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        }
                                    }
                                },
                                "9": {
                                    "numberquestions": "10",
                                    "description": "La empresa fabricante líder del mercado de café en Colombia es:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-7/thumbnail-10-1.jpg",
                                            "infoquestions": "a. Federación de Cafeteros, con sus marcas de café Juan Valdez y Buendía.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-7/thumbnail-10-2.jpg",
                                            "infoquestions": "b. Colcafé S.A.S con sus marcas de café Sello Rojo, La Bastilla, Matiz y Colcafé.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "2": {
                                            "description": "modulo-7/thumbnail-10-3.jpg",
                                            "infoquestions": "c. Torrecafé Águila Roja con su marca de café Águila Roja.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-7/thumbnail-10-4.jpg",
                                            "infoquestions": "d. Nestlé, con sus marcas de café Nescafé, Dolca y Dolce Gusto.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "10": {
                                    "numberquestions": "11",
                                    "description": "El porcentaje (%) de participación de mercado de Colcafé S.A.S.  con las marcas Sello Rojo, La Bastilla, Matiz y Colcafé es:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-7/thumbnail-11-1.jpg",
                                            "infoquestions": "a. 50%",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "1": {
                                            "description": "modulo-7/thumbnail-11-2.jpg",
                                            "infoquestions": "b. 20%",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "2": {
                                            "description": "modulo-7/thumbnail-11-3.jpg",
                                            "infoquestions": "c. 5%",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-7/thumbnail-11-4.jpg",
                                            "infoquestions": "d. 40%",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            {
                // MODULO 3
                "index": "2",
                "name": "Gestión Comercial",
                "unique": "gestion-comercial",
                "time": "4h",
                "content": {
                    "0": {
                        "index": "3.1",
                        "name": "Diamante de desarrollo de clientes",
                        "image": "modulo9-1.png",
                        "time": "00:54",
                        "type": "1",
                        "content": "conoce-nuestros-clientes.mp4"
                    },
                    "1": {
                        "index": "3.2",
                        "name": "1. Equipos",
                        "image": "modulo9-2.png",
                        "time": "03:00",
                        "type": "6",
                        "intro": true,
                        "introtemplate": "intro-equipo",
                        "portfolio": {
                            "main": "cartas/equipos-maquinas-costeo-1.png",
                            "items": {
                                "0": {
                                    "item": "cartas/equipos-maquinas-costeo-2.png",
                                    "detail": "cartas/equipos-maquinas-descripcion-1.png"
                                },
                                "1": {
                                    "item": "cartas/equipos-maquinas-costeo-3.png",
                                    "detail": "cartas/equipos-maquinas-descripcion-2.png"
                                },
                                "2": {
                                    "item": "cartas/equipos-maquinas-costeo-4.png",
                                    "detail": "cartas/equipos-maquinas-descripcion-3.jpg"
                                },
                                "3": {
                                    "item": "cartas/equipos-maquinas-costeo-5.png",
                                    "detail": "cartas/equipos-maquinas-descripcion-4.png"
                                },
                                "4": {
                                    "item": "cartas/equipos-maquinas-costeo-6.png",
                                    "detail": "cartas/equipos-maquinas-descripcion-5.png"
                                },
                                "5": {
                                    "item": "cartas/portafolio-maquinas-8.png",
                                    "detail": "cartas/equipos-maquinas-descripcion-6.png"
                                }
                            }
                        }
                    },
                    "2": {
                        "index": "3.3",
                        "name": "2. Portafolio",
                        "image": "modulo9-3.png",
                        "time": "03:30",
                        "type": "6",
                        "intro": true,
                        "introtemplate": "intro-portafolio",
                        "portfolio": {
                            "main": "cartas/portafolio-maquinas-1.png",
                            "items": {
                                "0": {
                                    "item": "cartas/portafolio-maquinas-2.png",
                                    "detail": "cartas/portafolio-maquinas-info-1.png"
                                },
                                "1": {
                                    "item": "cartas/portafolio-maquinas-3.png",
                                    "detail": "cartas/portafolio-maquinas-info-2.png"
                                },
                                "2": {
                                    "item": "cartas/portafolio-maquinas-4.png",
                                    "detail": "cartas/portafolio-maquinas-info-3.png"
                                },
                                "3": {
                                    "item": "cartas/portafolio-maquinas-5.png",
                                    "detail": "cartas/portafolio-maquinas-info-4.png"
                                },
                                "4": {
                                    "item": "cartas/portafolio-maquinas-6.png",
                                    "detail": "cartas/portafolio-maquinas-info-5.png"
                                },
                                "5": {
                                    "item": "cartas/portafolio-maquinas-7.png",
                                    "detail": "cartas/portafolio-maquinas-info-6.png"
                                },
                                "6": {
                                    "item": "cartas/portafolio-maquinas-8.png",
                                    "detail": "cartas/portafolio-maquinas-info-7.png"
                                }
                            }
                        }
                    },
                    "3": {
                        "index": "3.4",
                        "name": "3. Carta de bebidas y costeo",
                        "image": "modulo9-4.png",
                        "time": "05:00",
                        "type": "5",
                        "intro": true,
                        "introtemplate": "intro-carta",
                        "content": {
                            "description": "cartas/carta-bebidas-costeo-1.png",
                            "menu": {
                                "0": {
                                    "main": "cartas/carta-bebidas-costeo-2.png",
                                    "text": "Carta de bebidas calientes de café con greca o cafetera por goteo",
                                    "machine": "cartas/carta-maquina-caliente.png",
                                    "collection": {
                                        "0": {
                                            "title": "Café negro o tinto",
                                            "image": "cartas/carta-bebidas-caliente-1.png",
                                            "description": "Bebida tradicional con el mejor aroma y sabor  de nuestro café recién preparado."
                                        },
                                        "1": {
                                            "title": "Café campesino",
                                            "image": "cartas/carta-bebidas-caliente-2.png",
                                            "description": "Deliciosa combinación de Café negro o tinto, con la dulzura de la panela tradicional colombiana."
                                        },
                                        "2": {
                                            "title": "Carajillo",
                                            "image": "cartas/carta-bebidas-caliente-3.png",
                                            "description": "Fusión del sabor de café negro o tinto, con un toque de aguardiente o brandy."
                                        },
                                        "3": {
                                            "title": "Café con leche",
                                            "image": "cartas/carta-bebidas-caliente-4.png",
                                            "description": "Café negro o tinto tradicional mezclado con leche caliente."
                                        },
                                        "4": {
                                            "title": "Cappuccino",
                                            "image": "cartas/carta-bebidas-caliente-5.png",
                                            "description": "Deliciosa mezcla cremosa de café con leche espumosa."
                                        }
                                    }
                                },
                                "1": {
                                    "main": "cartas/carta-bebidas-costeo-3.png",
                                    "text": "Carta de bebidas frías de café con greca o cafetera por goteo",
                                    "machine": "cartas/carta-maquina-caliente.png",
                                    "collection": {
                                        "0": {
                                            "title": "Cappuccino frío",
                                            "image": "cartas/carta-bebidas-fria-1.png",
                                            "description": "Deliciosa alternativa de cappuccino para días calurosos."
                                        },
                                        "1": {
                                            "title": "Granizado",
                                            "image": "cartas/carta-bebidas-fria-2.png",
                                            "description": "Exquisita receta original de café espumoso preparado sobre hielo granizado."
                                        },
                                        "2": {
                                            "title": "Malteada de café",
                                            "image": "cartas/carta-bebidas-fria-3.png",
                                            "description": "Deliciosa bebida preparada a base de helado y café."
                                        },
                                        "3": {
                                            "title": "Café refrescante",
                                            "image": "cartas/carta-bebidas-fria-4.png",
                                            "description": "Bebida alternativa de café negro, hielo y deliciosos sabores."
                                        }
                                    }
                                },
                                "2": {
                                    "main": "cartas/carta-bebidas-costeo-4.png",
                                    "text": "Carta de bebidas calientes de café con máquina espresso",
                                    "machine": "cartas/carta-maquina-espresso.png",
                                    "collection": {
                                        "0": {
                                            "title": "Espresso",
                                            "image": "cartas/carta-bebidas-caliente-negocio-1.png",
                                            "description": "Deliciosa bebida de café con alta intensidad de sabor y aroma."
                                        },
                                        "1": {
                                            "title": "Espresso Macchiato",
                                            "image": "cartas/carta-bebidas-caliente-negocio-2.png",
                                            "description": "Espresso con un toque de leche cremosa."
                                        },
                                        "2": {
                                            "title": "Espresso con Panna",
                                            "image": "cartas/carta-bebidas-caliente-negocio-3.png",
                                            "description": "Espresso con un dulce toque de crema chantilly."
                                        },
                                        "3": {
                                            "title": "Café Americano ",
                                            "image": "cartas/carta-bebidas-caliente-negocio-4.png",
                                            "description": "Exquisito espresso diluido para disfrutar una sensación más suave de café."
                                        },
                                        "4": {
                                            "title": "Cappuccino",
                                            "image": "cartas/carta-bebidas-caliente-negocio-5.png",
                                            "description": "Bebida que combina el sabor intenso del espresso y de la cremosidad de la leche vaporizada."
                                        },
                                        "5": {
                                            "title": "Moccaccino",
                                            "image": "cartas/carta-bebidas-caliente-negocio-6.png",
                                            "description": "Cappuccino tradicional con una deliciosa adición de salsa de chocolate."
                                        }
                                    }
                                },
                                "3": {
                                    "main": "cartas/carta-bebidas-costeo-5.png",
                                    "text": "Carta de bebidas frías de café con máquina espresso",
                                    "machine": "cartas/carta-maquina-espresso.png",
                                    "collection": {
                                        "0": {
                                            "title": "Espresso Affogato",
                                            "image": "cartas/carta-bebidas-fria-negocio-5.png",
                                            "description": "Fusión de un espresso con una porción de helado de vainilla."
                                        },
                                        "1": {
                                            "title": "Cappuccino frío",
                                            "image": "cartas/carta-bebidas-fria-negocio-1.png",
                                            "description": "Deliciosa alternativa de cappuccino para días calurosos."
                                        },
                                        "2": {
                                            "title": "Granizado de café",
                                            "image": "cartas/carta-bebidas-fria-negocio-2.png",
                                            "description": "Exquisita receta original de café espumoso preparado sobre hielo granizado."
                                        },
                                        "3": {
                                            "title": "Malteada de café",
                                            "image": "cartas/carta-bebidas-fria-negocio-3.png",
                                            "description": "Deliciosa bebida preparada a base de helado y café espresso."
                                        },
                                        "4": {
                                            "title": "Café refrescante",
                                            "image": "cartas/carta-bebidas-fria-negocio-4.png",
                                            "description": "Bebida alternativa de café americano, hielo y deliciosos sabores."
                                        }
                                    }
                                },
                            }
                        }
                    },
                    "4": {
                        "index": "3.5",
                        "name": "3.1 Ejercicio de Costeo 1",
                        "image": "modulo9-5.png",
                        "time": "04:30",
                        "template": "case-1",
                        "type": "8",
                        "final": "uno",
                        "content": {
                            "0": {
                                "type": "x",
                                "intro": true,
                                "template": "caso1-0"
                            },
                            "1": {
                                "type": "y",
                                "content": {
                                    "0": {
                                        "image": "case-1_01_01.jpg"
                                    },
                                    "1": {
                                        "image": "case-1_02.jpg",
                                        "correct": false
                                    },
                                    "2": {
                                        "image": "case-1_03.jpg",
                                        "correct": true
                                    },
                                    "3": {
                                        "image": "case-1_04.jpg",
                                        "correct": false
                                    },
                                    "4": {
                                        "image": "case-1_05.jpg",
                                        "correct": false
                                    },
                                    "5": {
                                        "image": "case-1_06.jpg",
                                        "correct": false
                                    },
                                    "6": {
                                        "image": "case-1_07.jpg",
                                        "correct": false
                                    }
                                }
                            },
                            "2": {
                                "type": "z",
                                "description": "A partir de la necesidad de Don Armando, define 6 bebidas de café,<br>teniendo en cuenta los productos que podría preparar en sus equipos",
                                "content": {
                                    "0": {
                                        "image": "numeral-2.jpg"
                                    },
                                    "1": {
                                        "image": "case-2_01.jpg",
                                        "correct": true
                                    },
                                    "2": {
                                        "image": "case-2_02.jpg",
                                        "correct": true
                                    },
                                    "3": {
                                        "image": "case-2_03.jpg",
                                        "correct": false
                                    },
                                    "4": {
                                        "image": "case-2_04.jpg",
                                        "correct": true
                                    },
                                    "5": {
                                        "image": "case-2_05.jpg",
                                        "correct": true
                                    },
                                    "6": {
                                        "image": "case-2_06.jpg",
                                        "correct": true
                                    },
                                    "7": {
                                        "image": "case-2_07.jpg",
                                        "correct": true
                                    }
                                }
                            },
                            "3": {
                                "type": "z",
                                "description": "Define 4 productos del portafolio de café que puedes ofrecerle a don Armando para la carta establecida",
                                "content": {
                                    "0": {
                                        "image": "numeral-3.jpg"
                                    },
                                    "1": {
                                        "image": "case-3_01.jpg",
                                        "correct": true
                                    },
                                    "2": {
                                        "image": "case-3_02.jpg",
                                        "correct": false
                                    },
                                    "3": {
                                        "image": "case-3_03.jpg",
                                        "correct": true
                                    },
                                    "4": {
                                        "image": "case-3_04.jpg",
                                        "correct": false
                                    },
                                    "5": {
                                        "image": "case-3_05.jpg",
                                        "correct": false
                                    },
                                    "6": {
                                        "image": "case-3_06.jpg",
                                        "correct": true
                                    },
                                    "7": {
                                        "image": "case-3_07.jpg",
                                        "correct": true
                                    },
                                    "8": {
                                        "image": "case-3_08.jpg",
                                        "correct": false
                                    }
                                }
                            },
                            "4": {
                                "type": "x",
                                "intro": true,
                                "template": "caso1-1"
                            },
                            "5": {
                                "type": "w",
                                "unique": false,
                                "hideSoftwareBanner": true,
                                "question": "¿Cuáles serían las <strong>2 marcas de café</strong> recomendadas para preparar<br><strong>el café negro o tinto</strong> según el equipo de preparación que utiliza don Armando?",
                                "content": {
                                    "0": {
                                        "description": "casos/numeral-4.jpg"
                                    },
                                    "1": {
                                        "description": "casos/case-4_01.jpg",
                                        "infoquestions": "a. Café Sello Rojo",
                                        "correct": true
                                    },
                                    "2": {
                                        "description": "casos/case-4_02.jpg",
                                        "infoquestions": "b. Café La Bastilla",
                                        "correct": true
                                    },
                                    "3": {
                                        "description": "casos/case-4_03.jpg",
                                        "infoquestions": "c. Café Colcafé",
                                        "correct": false
                                    },
                                    "4": {
                                        "description": "casos/case-4_04.jpg",
                                        "infoquestions": "d. Café Matiz",
                                        "correct": false
                                    }
                                }
                            },
                            "6": {
                                "type": "x",
                                "intro": true,
                                "template": "caso1-2"
                            },
                            "7": {
                                "type": "w",
                                "unique": true,
                                "question": "El costo de 1 bebida de <strong>café negro o tinto</strong> es:",
                                "content": {
                                    "0": {
                                        "description": "casos/numeral-5.jpg"
                                    },
                                    "1": {
                                        "description": "casos/case-5_01.jpg",
                                        "infoquestions": "a. $166 incluyendo el café y los insumos.",
                                        "correct": true
                                    },
                                    "2": {
                                        "description": "casos/case-5_02.jpg",
                                        "infoquestions": "b. $500 incluyendo solamente el café.",
                                        "correct": false
                                    },
                                    "3": {
                                        "description": "casos/case-5_03.jpg",
                                        "infoquestions": "c. $600 incluyendo el café y los insumos.",
                                        "correct": false
                                    },
                                    "4": {
                                        "description": "casos/case-5_04.jpg",
                                        "infoquestions": "d. $200 incluyendo solamente el café.",
                                        "correct": false
                                    }
                                }
                            },
                            "8": {
                                "type": "w",
                                "unique": true,
                                "question": "<strong>La utilidad</strong> que don Armando obtiene por cada bebida de café preparado es de:",
                                "content": {
                                    "0": {
                                        "description": "casos/numeral-6.jpg"
                                    },
                                    "1": {
                                        "description": "casos/case-6_01.jpg",
                                        "infoquestions": "a. $50 por bebida.",
                                        "correct": false
                                    },
                                    "2": {
                                        "description": "casos/case-6_02.jpg",
                                        "infoquestions": "b. $80 por bebida.",
                                        "correct": false
                                    },
                                    "3": {
                                        "description": "casos/case-6_03.jpg",
                                        "infoquestions": "c. $173 por bebida.",
                                        "correct": false
                                    },
                                    "4": {
                                        "description": "casos/case-6_04.jpg",
                                        "infoquestions": "d. $435 por bebida.",
                                        "correct": true
                                    }
                                }
                            },
                            "9": {
                                "type": "w",
                                "unique": true,
                                "question": "La <strong>rentabilidad sobre la venta</strong>  del café preparado es de:",
                                "content": {
                                    "0": {
                                        "description": "casos/numeral-7.jpg"
                                    },
                                    "1": {
                                        "description": "casos/case-10_01.jpg",
                                        "infoquestions": "a. 10%",
                                        "correct": false
                                    },
                                    "2": {
                                        "description": "casos/case-10_02.jpg",
                                        "infoquestions": "b. 50%",
                                        "correct": false
                                    },
                                    "3": {
                                        "description": "casos/case-10_03.jpg",
                                        "infoquestions": "c. 72%",
                                        "correct": true
                                    },
                                    "4": {
                                        "description": "casos/case-10_04.jpg",
                                        "infoquestions": "d. 40%",
                                        "correct": false
                                    }
                                }
                            },
                            "10": {
                                "type": "u",
                                "unique": true,
                                "question": "Por cada paquete de café que prepara don Armando, debe invertir $16.550 (incluidos insumos), sus ventas serán de $60.000 y su utilidad es de $43.450 por paquete.",
                                "content": {
                                    "0": {
                                        "value": "a. Verdadero",
                                        "correct": true
                                    },
                                    "1": {
                                        "value": "b. Falso",
                                        "correct": false
                                    }
                                }
                            },
                            "11": {
                                "type": "x",
                                "intro": true,
                                "template": "caso1-3"
                            },
                            "12": {
                                "type": "w",
                                "unique": true,
                                "question": "El valor de las <strong>ventas</strong> mensuales de don Armando<br>por concepto de café negro o tinto preparado, es de:",
                                "content": {
                                    "0": {
                                        "description": "casos/numeral-9.jpg"
                                    },
                                    "1": {
                                        "description": "casos/case-7_01.jpg",
                                        "infoquestions": "a. $200.000 mensuales.",
                                        "correct": false
                                    },
                                    "2": {
                                        "description": "casos/case-7_02.jpg",
                                        "infoquestions": "b. $500.000 mensuales.",
                                        "correct": false
                                    },
                                    "3": {
                                        "description": "casos/case-7_03.jpg",
                                        "infoquestions": "c. $1'000.000 mensuales.",
                                        "correct": false
                                    },
                                    "4": {
                                        "description": "casos/case-7_04.jpg",
                                        "infoquestions": "d. $2'400.000 mensuales.",
                                        "correct": true
                                    }
                                }
                            },
                            "13": {
                                "type": "w",
                                "unique": true,
                                "question": "El costo mensual del café y los insumos requeridos<br>para la venta de café preparado es de:",
                                "content": {
                                    "0": {
                                        "description": "casos/numeral-10.jpg"
                                    },
                                    "1": {
                                        "description": "casos/case-8_01.jpg",
                                        "infoquestions": "a. $662.000 mensuales.",
                                        "correct": true
                                    },
                                    "2": {
                                        "description": "casos/case-8_02.jpg",
                                        "infoquestions": "b. $800.000 mensuales.",
                                        "correct": false
                                    },
                                    "3": {
                                        "description": "casos/case-7_03.jpg",
                                        "infoquestions": "c. $1'000.000 mensuales.",
                                        "correct": false
                                    },
                                    "4": {
                                        "description": "casos/case-8_04.jpg",
                                        "infoquestions": "d. $2'000.000 mensuales.",
                                        "correct": false
                                    }
                                }
                            },
                            "14": {
                                "type": "w",
                                "unique": true,
                                "question": "<strong>La utilidad mensual que</strong> don Armando obtiene por la venta de café preparado es de:",
                                "content": {
                                    "0": {
                                        "description": "casos/numeral-11.jpg"
                                    },
                                    "1": {
                                        "description": "casos/case-9_01.jpg",
                                        "infoquestions": "a. $700.000 mensuales.",
                                        "correct": false
                                    },
                                    "2": {
                                        "description": "casos/case-7_02.jpg",
                                        "infoquestions": "b. $500.000 mensuales.",
                                        "correct": false
                                    },
                                    "3": {
                                        "description": "casos/case-9_03.jpg",
                                        "infoquestions": "c. $800.000 mensuales.",
                                        "correct": false
                                    },
                                    "4": {
                                        "description": "casos/case-9_04.jpg",
                                        "infoquestions": "d. $1'738.000 mensuales.",
                                        "correct": true
                                    }
                                }
                            },
                            "15": {
                                "type": "w",
                                "unique": true,
                                "question": "<strong>La rentabilidad mensual</strong> sobre las ventas del café preparado es de:",
                                "content": {
                                    "0": {
                                        "description": "casos/numeral-12.jpg"
                                    },
                                    "1": {
                                        "description": "casos/case-10_01.jpg",
                                        "infoquestions": "a. 10%",
                                        "correct": false
                                    },
                                    "2": {
                                        "description": "casos/case-10_02.jpg",
                                        "infoquestions": "b. 50%",
                                        "correct": false
                                    },
                                    "3": {
                                        "description": "casos/case-10_03.jpg",
                                        "infoquestions": "c. 72%",
                                        "correct": true
                                    },
                                    "4": {
                                        "description": "casos/case-10_04.jpg",
                                        "infoquestions": "d. 40%",
                                        "correct": false
                                    }
                                }
                            },
                            "16": {
                                "type": "w",
                                "unique": true,
                                "question": "<strong>Para operar su negocio, mensualmente, don Armando</strong><br>requiere realizar el siguiente pedido de café:",
                                "content": {
                                    "0": {
                                        "description": "casos/numeral-13.jpg"
                                    },
                                    "1": {
                                        "description": "casos/case-11_01.jpg",
                                        "infoquestions": "a. 40 paquetes de café de 500 gramos, cuyo costo es de $290.000.",
                                        "correct": true
                                    },
                                    "2": {
                                        "description": "casos/case-11_02.jpg",
                                        "infoquestions": "b. 80 paquetes de café de 500 gramos, cuyo costo es de $640.000.",
                                        "correct": false
                                    },
                                    "3": {
                                        "description": "casos/case-11_03.jpg",
                                        "infoquestions": "c. 20 paquetes de café de 500 gramos, cuyo costo es de $160.000.",
                                        "correct": false
                                    },
                                    "4": {
                                        "description": "casos/case-11_04.jpg",
                                        "infoquestions": "d. 10 paquetes de café de 500 gramos, cuyo costo es de $80.000.",
                                        "correct": false
                                    }
                                }
                            }
                        }
                    },
                    "5": {
                        "index": "3.6",
                        "name": "3.2 Ejercicio de Costeo 2",
                        "image": "modulo9-6.png",
                        "time": "04:30",
                        "template": "case-2",
                        "type": "8",
                        "final": "dos",
                        "content": {    
                            "0": {
                                "type": "x",
                                "intro": true,
                                "template": "caso2-0"
                            },
                            "1": {
                                "type": "z",
                                "content": {
                                    "0": {
                                        "image": "case-1_01.jpg"
                                    },
                                    "1": {
                                        "image": "case-1_02.jpg",
                                        "correct": false
                                    },
                                    "2": {
                                        "image": "case-1_03.jpg",
                                        "correct": false
                                    },
                                    "3": {
                                        "image": "case-1_04.jpg",
                                        "correct": true
                                    },
                                    "4": {
                                        "image": "case-1_05.jpg",
                                        "correct": false
                                    },
                                    "5": {
                                        "image": "case-1_06.jpg",
                                        "correct": true
                                    },
                                    "6": {
                                        "image": "case-1_07.jpg",
                                        "correct": true
                                    }
                                }
                            },
                            "2": {
                                "type": "z",
                                "description": "A partir de la necesidad de Doña Luciana, define la <strong>carta de bebidas de café</strong>,<br>teniendo en cuenta los productos que podría preparar en sus equipos",
                                "content": {
                                    "0": {
                                        "image": "numeral-2.jpg"
                                    },
                                    "1": {
                                        "image": "case-2_01.jpg",
                                        "correct": true
                                    },
                                    "2": {
                                        "image": "case-2_02.jpg",
                                        "correct": true
                                    },
                                    "3": {
                                        "image": "case-2_03.jpg",
                                        "correct": true
                                    },
                                    "4": {
                                        "image": "case-2_04.jpg",
                                        "correct": true
                                    },
                                    "5": {
                                        "image": "case-2_05.jpg",
                                        "correct": true
                                    },
                                    "6": {
                                        "image": "case-2_06.jpg",
                                        "correct": true
                                    },
                                    "7": {
                                        "image": "case-2_07.jpg",
                                        "correct": true
                                    }
                                }
                            },
                            "3": {
                                "type": "z",
                                "description": "Define 4 productos del portafolio de café que se pueden ofrecer a doña Luciana para la carta establecida.",
                                "content": {
                                    "0": {
                                        "image": "numeral-3.jpg"
                                    },
                                    "1": {
                                        "image": "case-3_01.jpg",
                                        "correct": false
                                    },
                                    "2": {
                                        "image": "case-3_02.jpg",
                                        "correct": false
                                    },
                                    "3": {
                                        "image": "case-3_03.jpg",
                                        "correct": false
                                    },
                                    "4": {
                                        "image": "case-3_04.jpg",
                                        "correct": false
                                    },
                                    "5": {
                                        "image": "case-3_05.jpg",
                                        "correct": true
                                    },
                                    "6": {
                                        "image": "case-3_06.jpg",
                                        "correct": true
                                    },
                                    "7": {
                                        "image": "case-3_07.jpg",
                                        "correct": true
                                    },
                                    "8": {
                                        "image": "case-3_08.jpg",
                                        "correct": true
                                    }
                                }
                            },
                            "4": {
                                "type": "x",
                                "intro": true,
                                "template": "caso2-1"
                            },
                            "5": {
                                "type": "w",
                                "unique": true,
                                "hideSoftwareBanner": true,
                                "question": "¿Cuál sería la marca de café recomendada para preparar el <br> café negro o café americano, según el equipo de preparación que utiliza doña Luciana?",
                                "content": {
                                    "0": {
                                        "description": "casos/numeral-4.jpg"
                                    },
                                    "1": {
                                        "description": "casos/case-4_01.jpg",
                                        "infoquestions": "a. Café Sello Rojo",
                                        "correct": false
                                    },
                                    "2": {
                                        "description": "casos/case-4_02.jpg",
                                        "infoquestions": "b. Café La Bastilla",
                                        "correct": false
                                    },
                                    "3": {
                                        "description": "casos/case-4_03.jpg",
                                        "infoquestions": "c. Café Colcafé",
                                        "correct": false
                                    },
                                    "4": {
                                        "description": "casos/case-4_04.jpg",
                                        "infoquestions": "d. Café Matiz",
                                        "correct": true
                                    }
                                }
                            },
                            "6": {
                                "type": "x",
                                "intro": true,
                                "template": "caso2-2"
                            },
                            "7": {
                                "type": "w",
                                "unique": true,
                                "question": "El costo de 1 bebida de <strong>café negro o café americano es</strong>:",
                                "content": {
                                    "0": {
                                        "description": "casos/numeral-5.jpg"
                                    },
                                    "1": {
                                        "description": "casos/case-5_01.jpg",
                                        "infoquestions": "a. $298 incluyendo el café y los insumos.",
                                        "correct": true
                                    },
                                    "2": {
                                        "description": "casos/case-5_02.jpg",
                                        "infoquestions": "b. $500 incluyendo solamente el café.",
                                        "correct": false
                                    },
                                    "3": {
                                        "description": "casos/case-5_03.jpg",
                                        "infoquestions": "c. $600 incluyendo el café y los insumos.",
                                        "correct": false
                                    },
                                    "4": {
                                        "description": "casos/case-5_04.jpg",
                                        "infoquestions": "d. $350 incluyendo solamente el café.",
                                        "correct": false
                                    }
                                }
                            },
                            "8": {
                                "type": "w",
                                "unique": true,
                                "question": "<strong>La utilidad</strong> que doña Luciana obtiene por cada bebida de café preparado es de:",
                                "content": {
                                    "0": {
                                        "description": "casos/numeral-6.jpg"
                                    },
                                    "1": {
                                        "description": "casos/case-6_01.jpg",
                                        "infoquestions": "a. $50 por bebida.",
                                        "correct": false
                                    },
                                    "2": {
                                        "description": "casos/case-6_02.jpg",
                                        "infoquestions": "b. $80 por bebida.",
                                        "correct": false
                                    },
                                    "3": {
                                        "description": "casos/case-6_03.jpg",
                                        "infoquestions": "c. $173 por bebida.",
                                        "correct": false
                                    },
                                    "4": {
                                        "description": "casos/case-12_04.jpg",
                                        "infoquestions": "d. $902 por bebida.",
                                        "correct": true
                                    }
                                }
                            },
                            "9": {
                                "type": "w",
                                "unique": true,
                                "question": "<strong>La rentabilidad</strong> sobre la venta del café preparado es de:",
                                "content": {
                                    "0": {
                                        "description": "casos/numeral-7.jpg"
                                    },
                                    "1": {
                                        "description": "casos/case-10_01.jpg",
                                        "infoquestions": "a. 10%",
                                        "correct": false
                                    },
                                    "2": {
                                        "description": "casos/case-10_02.jpg",
                                        "infoquestions": "b. 50%",
                                        "correct": false
                                    },
                                    "3": {
                                        "description": "casos/case-16_03.jpg",
                                        "infoquestions": "c. 75%",
                                        "correct": true
                                    },
                                    "4": {
                                        "description": "casos/case-10_04.jpg",
                                        "infoquestions": "d. 40%",
                                        "correct": false
                                    }
                                }
                            },
                            "10": {
                                "type": "u",
                                "question": "Por cada paquete de café que prepara doña Luciana, <strong>debe invertir $9.326</strong> <br> (incluidos insumos), sus ventas <strong>serán de $37.500</strong> y su utilidad es de </strong>$28.175 por paquete.</strong>",
                                "content": {
                                    "0": {
                                        "value": "a. Verdadero",
                                        "correct": true
                                    },
                                    "1": {
                                        "value": "b. Falso",
                                        "correct": false
                                    }
                                }
                            },
                            "11": {
                                "type": "x",
                                "intro": true,
                                "template": "caso2-3"
                            },
                            "12": {
                                "type": "w",
                                "unique": true,
                                "question": "<strong>El valor de las ventas mensuales</strong> de doña Luciana<br>por concepto de <strong>café negro o café americano preparado</strong>, es de:",
                                "content": {
                                    "0": {
                                        "description": "casos/numeral-9.jpg"
                                    },
                                    "1": {
                                        "description": "casos/case-7_01.jpg",
                                        "infoquestions": "a. $200.000 mensuales.",
                                        "correct": false
                                    },
                                    "2": {
                                        "description": "casos/case-7_02.jpg",
                                        "infoquestions": "b. $500.000 mensuales.",
                                        "correct": false
                                    },
                                    "3": {
                                        "description": "casos/case-7_03.jpg",
                                        "infoquestions": "c. $1'000.000 mensuales.",
                                        "correct": false
                                    },
                                    "4": {
                                        "description": "casos/case-13_04.jpg",
                                        "infoquestions": "d. $4'800.000 mensuales.",
                                        "correct": true
                                    }
                                }
                            },
                            "13": {
                                "type": "w",
                                "unique": true,
                                "question": "<strong>El costo mensual del café</strong> y los insumos <br> requeridos para la venta de café preparado es de:",
                                "content": {
                                    "0": {
                                        "description": "casos/numeral-10.jpg"
                                    },
                                    "1": {
                                        "description": "casos/case-14_01.jpg",
                                        "infoquestions": "a. $1'193.664 mensuales.",
                                        "correct": true
                                    },
                                    "2": {
                                        "description": "casos/case-14_02.jpg",
                                        "infoquestions": "b. $1'800.000 mensuales.",
                                        "correct": false
                                    },
                                    "3": {
                                        "description": "casos/case-7_03.jpg",
                                        "infoquestions": "c. $1'000.000 mensuales.",
                                        "correct": false
                                    },
                                    "4": {
                                        "description": "casos/case-8_04.jpg",
                                        "infoquestions": "d. $2'000.000 mensuales.",
                                        "correct": false
                                    }
                                }
                            },
                            "14": {
                                "type": "w",
                                "unique": true,
                                "question": "<strong>La utilidad mensual</strong> que doña Luciana obtiene por la venta de café preparado es de:",
                                "content": {
                                    "0": {
                                        "description": "casos/numeral-11.jpg"
                                    },
                                    "1": {
                                        "description": "casos/case-9_01.jpg",
                                        "infoquestions": "a. $700.000 de utilidad mensual.",
                                        "correct": false
                                    },
                                    "2": {
                                        "description": "casos/case-7_02.jpg",
                                        "infoquestions": "b. $500.000 de utilidad mensual.",
                                        "correct": false
                                    },
                                    "3": {
                                        "description": "casos/case-9_03.jpg",
                                        "infoquestions": "c. $800.000 de utilidad mensual.",
                                        "correct": false
                                    },
                                    "4": {
                                        "description": "casos/case-15_04.jpg",
                                        "infoquestions": "d. $3'606.336 de utilidad mensual.",
                                        "correct": true
                                    }
                                }
                            },
                            "15": {
                                "type": "w",
                                "unique": true,
                                "question": "<strong>La rentabilidad mensual</strong> sobre las ventas del café preparado es de:",
                                "content": {
                                    "0": {
                                        "description": "casos/numeral-12.jpg"
                                    },
                                    "1": {
                                        "description": "casos/case-10_01.jpg",
                                        "infoquestions": "a. 10%",
                                        "correct": false
                                    },
                                    "2": {
                                        "description": "casos/case-10_02.jpg",
                                        "infoquestions": "b. 50%",
                                        "correct": false
                                    },
                                    "3": {
                                        "description": "casos/case-16_03.jpg",
                                        "infoquestions": "c. 75%",
                                        "correct": true
                                    },
                                    "4": {
                                        "description": "casos/case-10_04.jpg",
                                        "infoquestions": "d. 40%",
                                        "correct": false
                                    }
                                }
                            },
                            "16": {
                                "type": "w",
                                "unique": true,
                                "question": "Para operar su negocio, mensualmente, doña Luciana <br> <strong>requiere realizar el siguiente pedido de café:</strong>",
                                "content": {
                                    "0": {
                                        "description": "casos/numeral-13.jpg"
                                    },
                                    "1": {
                                        "description": "casos/case-17_01.jpg",
                                        "infoquestions": "a. 128 paquetes de café de 250 gramos equivalentes a 64 libras de café, cuyo costo es de $993.664.",
                                        "correct": true
                                    },
                                    "2": {
                                        "description": "casos/case-17_02.jpg",
                                        "infoquestions": "b. 64 paquetes de café de 250 gramos equivalentes a 32 libras de café, cuyo costo es de $496.832.",
                                        "correct": false
                                    },
                                    "3": {
                                        "description": "casos/case-17_03.jpg",
                                        "infoquestions": "c. 32 paquetes de café de 250 gramos equivalentes a 16 libras de café, cuyo costo es de $248.416.",
                                        "correct": false
                                    },
                                    "4": {
                                        "description": "casos/case-17_04.jpg",
                                        "infoquestions": "d. 192 paquetes de café de 250 gramos equivalentes a 96 libras de café, cuyo  costo es de $1’490.496.",
                                        "correct": false
                                    }
                                }
                            }
                        }
                    },
                    "6": {
                        "index": "3.7",
                        "name": "4. Tácticas de aumento de consumo",
                        "image": "modulo9-7.png",
                        "time": "04:00",
                        "type": "3",
                        "intro": true,
                        "introtemplate": "intro-tacticas",
                        "content": [
                            "tacticas-consumo/infografico-0.png",
                            "tacticas-consumo/infografico-1.png",
                            "tacticas-consumo/infografico-2.png",
                            "tacticas-consumo/infografico-3.png",
                            "tacticas-consumo/infografico-4.png",
                            "tacticas-consumo/infografico-5.png",
                            "tacticas-consumo/infografico-6.png",
                            "tacticas-consumo/infografico-7.png",
                            "tacticas-consumo/infografico-8.png"
                        ]
                    },
                    "7": {
                        "index": "3.8",
                        "name": "5. Propuesta de Valor Colcafé ",
                        "image": "modulo9-8.png",
                        "time": "01:09",
                        "type": "10",
                        "intro": true,
                        "introtemplate": "intro-propuesta",
                        "content": {
                            "0": {
                                "type": "10-a",
                                "source": "colcafe-propuesta-de-valor.mp4"
                            },
                            "1": {
                                "type": "10-a",
                                "source": "colcafe-marcas-productos.mp4"
                            },
                            "2": {
                                "type": "10-c",
                                "source": [
                                    "modulo-8/propuesta-de-valor-intro.png",
                                    "modulo-8/propuesta-de-valor-1.jpg",
                                    "modulo-8/propuesta-de-valor-2.png",
                                    "modulo-8/propuesta-de-valor-3.png",
                                    "modulo-8/propuesta-de-valor-4.jpg",
                                    "modulo-8/propuesta-de-valor-5.jpg"
                                ]
                            }
                        }

                    },
                    "8": {
                        "index": "3.9",
                        "name": "Prueba tu conocimiento",
                        "image": "modulo9-1.png",
                        "time": "03:00",
                        "type": "9"
                    }
                }
            }];
        },
        machines: function() {
            return [{
                "id": 1,
                "name": "Cafetera Institucional por Goteo",
                "img": "images/equipos/cafetera-institucional.png",
                "type": 1,
                "slug":"Goteo",
                "color": "yellow",
                "drinks": [{
                    "id": 1
                }, {
                    "id": 2
                },{
                    "id": 3,
                    "brands":[{
                        "id":4,
                        "divider":180,
                        "insumos": [{
                            "id": 1
                        }, {
                            "id": 2
                        },{
                            "id": 3
                        },{
                            "id": 5
                        }],
                        "tostions":[{
                            "id":19
                            },{
                            "id":14
                            }]
                        }]
                }],
                "brands": [{
                    "id": 1,
                    "tostions":[{
                        "id":2
                        },
                        {
                        "id":3
                        }]
                }, {
                    "id": 2,
                    "tostions":[{
                        "id":1
                        },
                        {
                        "id":2
                        },
                        {
                        "id":3
                        }]
                },{
                    "id": 3,
                    "tostions":[{
                        "id":4
                        },
                        {
                        "id":5
                        },
                        {
                        "id":6
                        },
                        {
                        "id":7
                        }]
                }],
                "glasses": [{
                    "id": 1
                }, {
                    "id": 2
                },{
                    "id": 3
                }],
                "insumos": [{
                    "id": 1
                }, {
                    "id": 2
                },{
                    "id": 3
                },{
                    "id": 4
                }]
            }, {
                "id": 2,
                "name": "Greca",
                "img": "images/equipos/graca.png",
                "type": 1,
                "slug":"Greca",
                "color": "green",
                "drinks": [{
                    "id": 1
                }, {
                    "id": 2
                },{
                    "id": 3,
                    "brands":[{
                        "id":4,
                        "tostions":[{
                            "id":13
                            },{
                            "id":14
                            }]
                        }]
                }],
                "brands": [{
                    "id": 1,
                    "tostions":[{
                        "id":2
                        },
                        {
                        "id":3
                        }]
                }, {
                    "id": 2,
                    "tostions":[{
                        "id":1
                        },
                        {
                        "id":2
                        },
                        {
                        "id":3
                        }]
                }],
                "glasses": [{
                    "id": 1
                }, {
                    "id": 2
                },{
                    "id": 3
                }],
                "insumos": [{
                    "id": 1
                }, {
                    "id": 2
                },{
                    "id": 3
                },{
                    "id": 4
                }]
            }, {
                "id": 3,
                "name": "Máquina Espresso con Molino",
                "img": "images/equipos/maquina-espresso-molido.png",
                "type": 2,
                "slug":"Espresso molino",
                "color": "coffe",
                "drinks": [{
                    "id": 4,
                    "glasses":null,
                    "express":[{
                        "id":4
                        },{
                        "id":5
                        }]
                }, {
                    "id": 5,
                    "express":[{
                        "id":4
                        },{
                        "id":5
                        }]
                },{
                    "id": 2,
                    "express":[{
                        "id":4
                        },{
                        "id":5
                        }]
                },{
                    "id": 3,
                    "brands": [{
                    "id": 1,
                    "insumos": [{
                            "id": 1
                        }, {
                            "id": 2
                        },{
                            "id": 5
                        }],
                    "tostions":[{
                        "id":8
                        }]
                }, {
                    "id": 3,
                    "insumos": [{
                            "id": 1
                        }, {
                            "id": 2
                        },{
                            "id": 5
                        }],
                    "tostions":[{
                        "id":9
                        },
                        {
                        "id":10
                        }]
                }],
                    "express":[{
                        "id":4
                        },{
                        "id":5
                        }]
                }],
                "brands": [{
                    "id": 8,
                    "tostions":[{
                        "id":8
                        }]
                }, {
                    "id": 3,
                    "tostions":[{
                        "id":9
                        },
                        {
                        "id":10
                        }]
                }],
                "glasses": [{
                    "id": 1
                }, {
                    "id": 2
                }, {
                    "id": 3
                }],
                "insumos": [{
                    "id": 1
                }, {
                    "id": 2
                },{
                    "id": 3
                },{
                    "id": 4
                }]
            }, {
                "id": 4,
                "name": "Máquina Espresso sin Molino",
                "img": "images/equipos/maquina-espresso.png",
                "type": 2,
                "slug":"Espreso sin molino",
                "color": "green",
                "drinks": [{
                    "id": 4,
                    "glasses":null,
                    "express":[{
                        "id":4
                        },{
                        "id":5
                        }]
                }, {
                    "id": 5,
                    "express":[{
                        "id":4
                        },{
                        "id":5
                        }]
                },{
                    "id": 2,
                    "express":[{
                        "id":4
                        },{
                        "id":5
                        }]
                },{
                    "id": 3,
                    "brands": [{
                        "id": 1,
                        "insumos": [{
                            "id": 1
                        }, {
                            "id": 2
                        },{
                            "id": 5
                        }],
                        "tostions":[{
                            "id":8
                            }]
                    }],
                    "express":[{
                        "id":4
                        },{
                        "id":5
                        }]
                }],
                "brands": [{
                    "id": 1,
                    "tostions":[{
                        "id":8
                        }]
                },{
                    "id": 3,
                    "tostions":[{
                        "id":18
                        }]
                }],
                "glasses": [{
                    "id": 1
                }, {
                    "id": 2
                }, {
                    "id": 3
                }],
                "insumos": [{
                    "id": 1
                }, {
                    "id": 2
                },{
                    "id": 3
                },{
                    "id": 4
                }]
            }, {
                "id": 5,
                "name": "Dispensador de Bebidas Frías",
                "img": "images/equipos/dispensador-frio.png",
                "slug":"Bed. Frías",
                "type": 3,
                "color": "coffe",
                "drinks": [{
                    "id": 7,
                    "brands":[{
                        "id":4,
                        "tostions":[{
                            "id":19
                            },{
                            "id":14
                            }]
                        }]
                }],
                "brands": [],
                "glasses": [{
                    "id": 6
                }, {
                    "id": 7
                }, {
                    "id": 8
                }, {
                    "id": 9
                }, {
                    "id": 10
                }],
                "insumos": [{
                    "id": 6
                }, {
                    "id": 2
                }]
            }, {
                "id": 6,
                "name": "Granizadora",
                "img": "images/equipos/granizado.png",
                "type": 3,
                "slug":"Granizadora",
                "color": "yellow",
                "drinks": [{
                    "id":8
                }],
                "brands": [{
                    "id":7,
                    "tostions":[{
                        "id":17
                        }]
                    }],
                "glasses": [{
                    "id": 6
                }, {
                    "id": 7
                }, {
                    "id": 8
                }, {
                    "id": 9
                }, {
                    "id": 10
                }],
                "insumos": [{
                    "id": 6
                }, {
                    "id": 2
                }, {
                    "id": 4
                }]
            }]
        },
        drinks: function() {
            return [{
                "id": 1,
                "name": "Café negro o tinto",
                "img": "images/cafe-negro-tinto.jpg",
                "slug":"Tinto",
                "monthProjection":true,
                "presentation":"images/cafe-venta-publico.jpg"
            }, {
                "id": 2,
                "name": "Café con leche",
                "img": "images/cafe-con-leche.jpg",
                "useMilk": true,
                "slug":"Cafe leche",
                "monthProjection":false,
                "presentation":"images/taza-cafe-con-leche.png"
            }, {
                "id": 3,
                "name": "Cappuccino",
                "img": "images/cafe-cappuccino.jpg",
                "slug":"Cappuccino",
                "monthProjection":false,
                "presentation":"images/taza-cappuccino.png"
            },{
                "id": 4,
                "name": "Espresso",
                "img": "images/espresso.png",
                "slug":"Espresso",
                "monthProjection":true,
                "presentation":"images/taza-espresso.png"
            },{
                "id": 5,
                "name": "Café Americano o Café Negro",
                "img": "images/cafe-americano.png",
                "slug":"Americano",
                "monthProjection":true,
                "presentation":"images/cafe-venta-publico.jpg"
            },{
                "id": 7,
                "name": "Cappuccino Frío",
                "img": "images/colcafe-cafe-cappuccino.png",
                "slug":"Cappuccino frío",
                "monthProjection":false,
                "presentation":"images/taza-cappuccino-frio.png"
            },{
                "id": 8,
                "name": "Granizado de Café",
                "img": "images/granizado-de-cafe.png",
                "slug":"Granizado",
                "monthProjection":false,
                "presentation":"images/taza-granizado.png"
            }]
        },
        brands: function() {
            return [{
                "id": 1,
                "name": "Café Sello Rojo",
                "img": "images/cafe-sello-rojo.jpg",
                "pack": "images/paquete-sello-rojo.png",
                "slug":"Sello Rojo"
            },
            {
                "id": 2,
                "name": "Café La Bastilla",
                "img": "images/cafe-la-bastilla.jpg",
                "pack": "images/cafe-la-bastilla-large.png",
                "slug":"Bastilla"
            },
            {
                "id": 3,
                "name": "Matiz",
                "img": "images/cafe-matiz.jpg",
                "pack": "images/paquete-matiz.png",
                "slug":"Matiz"
            },
            {
                "id": 4,
                "name": "Colcafé Cappuccino",
                "img": "images/colcafe-cappuccino.png",
                "pack": "images/gramaje-colcafe.png",
                "packFrio": "images/gramaje-colcafe.png",
                "slug":"Colcafé Cappuccino"
            },
            {
                "id": 5,
                "name": "Colcafé Café Helado",
                "img": "images/colcafe-cafe-helado.jpg",
                "pack": "images/gramaje-colcafe.png",
                "slug":"Colcafé Café Helado"
            },
            {
                "id": 6,
                "name": "Colcafé Iced Cappuccino",
                "img": "images/colcafe-iced.jpg",
                "pack": "images/gramaje-colcafe.png",
                "slug":"Colcafé Iced Cappuccino"
            },
            {
                "id": 7,
                "name": "Colcafé Granizado",
                "img": "images/colcafe-cappuccino.png",
                "pack": "images/gramaje-colcafe.png",
                "slug":"Colcafé Granizado"
            },
            {
                "id": 8,
                "name": "Café Sello Rojo Espresso",
                "img": "images/cafe-sello-rojo.jpg",
                "pack": "images/gramaje-colcafe.png",
                "slug":"Sello Rojo"
            }]
        },
        tostions: function() {
            return [{
                "id": 1,
                "name": "Tostión clara /<br>café suave",
                "img": "images/tostion-clara.jpg",
                "value": 5,
                "slug": "Tostión clara"
            }, {
                "id": 2,
                "name": "Tostión media /<br>café balanceado",
                "img": "images/tostion-media.jpg",
                "value": 5,
                "slug": "Tostión media"
            }, {
                "id": 3,
                "name": "Tostión oscura /<br>café fuerte",
                "img": "images/tostion-oscura.jpg",
                "value": 5,
                "slug": "Tostión oscura"
            },{
                "id": 4,
                "name": "Marfil",
                "img": "images/matiz-marfil-suave.jpg",
                "value": 7,
                "slug": "Marfil"
            },{
                "id": 5,
                "name": "Ámbar",
                "img": "images/matiz-ambar-balanceado.jpg",
                "value": 7,
                "slug": "Ámbar"
            },{
                "id": 6,
                "name": "Escarlata",
                "img": "images/matiz-escarlata-intenso.jpg",
                "value": 7,
                "slug": "Escarlata"
            },{
                "id": 7,
                "name": "Ébano",
                "img": "images/matiz-ebano.jpg",
                "value": 7,
                "slug": "Ébano"
            },{
                "id": 8,
                "name": "Espresso",
                "img": "images/tostion-espresso.png",
                "value": 26.67,
                "slug": "Espresso"
            },{
                "id": 9,
                "name": "Escarlata<br>en grano",
                "img": "images/matiz-escarlata-intenso.jpg",
                "value": 26.67,
                "slug": "Escarlata grano"
            },{
                "id": 10,
                "name": "Ébano<br>en grano",
                "img": "images/matiz-ebano.jpg",
                "value": 26.67,
                "slug": "Ébano grano"
            },{
                "id": 11,
                "name": "Vainilla",
                "img": "img/tostions/tostion-vainilla.png",
                "value": 10,
                "slug": "Vainilla"
            },{
                "id": 12,
                "name": "Caramelo",
                "img": "img/tostions/tostion-caramelo.png",
                "value":10,
                "slug": "Caramelo"
            },{
                "id": 13,
                "name": "Clásico",
                "img": "images/tostion-cappucino-clasico.png",
                "value": 18,
                "slug": "Clásico"
            },{
                "id": 14,
                "name": "Light",
                "img": "images/tostion-cappucino-light.png",
                "value": 6,
                "slug": "Light"
            },{
                "id": 17,
                "name": "Café",
                "img": "images/colcafe-granizado-cafe.png",
                "value": 31.5,
                "slug": "Café"
            },{
                "id": 18,
                "name": "Ébano<br>Espresso molido",
                "img": "images/matiz-ebano.jpg",
                "value": 26.67,
                "slug": "Ébano Espresso molido"
            },{
                "id": 19,
                "name": "Clásico, Vainilla,<br>Mocca, Caramelo",
                "img": "images/cappuccinos.png",
                "value": 9,
                "slug": "Cappucino varios"
            }]
        },
        glasses: function() {
            return [{
                "id": 1,
                "name": "4 onzas (100 ml)",
                "ml": 100,
                "img": "images/4-onzas.png",
                "slug": "4 onzas"
            }, {
                "id": 2,
                "name": "6-7 onzas (200 ml)",
                "ml": 200,
                "img": "images/6-onzas.png",
                "slug": "6-7 onzas"
            }, {
                "id": 3,
                "name": "8-9 onzas (250 ml)",
                "ml": 250,
                "img": "images/8-onzas.png",
                "slug": "8 onzas"
            }, {
                "id": 4,
                "name": "1 onza (30 ml)",
                "ml": 30,
                "img": "images/ristreto.png",
                "express":true,
                "gramCoffee":8,
                "slug": "Sencillo"
            }, {
                "id": 5,
                "name": "2 onzas (60 ml)",
                "ml": 60,
                "img": "images/doppio.png",
                "express":true,
                "gramCoffee":16,
                "slug": "Doble"
            }, {
                "id": 6,
                "name": "7 onzas (200 ml)",
                "ml": 200,
                "img": "images/t-7-onzas.png",
                "slug": "7 onzas"
            }, {
                "id": 7,
                "name": "8 onzas (240 ml)",
                "ml": 240,
                "img": "images/t-8-onzas.png",
                "slug": "8 onzas"
            }, {
                "id": 8,
                "name": "10 onzas (300 ml)",
                "ml": 300,
                "img": "images/t-10-onzas.png",
                "slug": "10 onzas"
            }, {
                "id": 9,
                "name": "12 onzas (360 ml)",
                "ml": 360,
                "img": "images/t-12-onzas.png",
                "slug": "12 onzas"
            }, {
                "id": 10,
                "name": "16 onzas (480 ml)",
                "ml": 480,
                "img": "images/t-16-onzas.png",
                "slug": "16 onzas"
            }]
        },
        insumos: function() {
            return [{
                "id": 1,
                "name": "Vasos",
                "img": "images/vasos-plastico.png",
                "quantityLabel":"Vasos",
                "quantityPerCoffee":1
            }, {
                "id": 2,
                "name": "Mezcladores de café",
                "img": "images/mezcladores-cafe.png",
                "quantityLabel":"Unidades",
                "quantityPerCoffee":1
            }, {
                "id": 3,
                "name": "Azúcar",
                "img": "images/azucar.png",
                "quantityLabel":"Sobres",
                "quantityPerCoffee":2
            },{
                "id": 4,
                "name": "Agua",
                "img": "images/agua.png",
                "quantityLabel":"Litros",
                "quantityPerCoffee":-1,
                "water":true
            },{
                "id": 5,
                "name": "Leche",
                "img": "images/leche-ico.png",
                "quantityLabel":"Litros",
                "quantityPerCoffee":-1
            },{
                "id": 6,
                "name": "Vasos",
                "img": "images/vasos-plastico-generico.png",
                "quantityLabel":"Vasos",
                "quantityPerCoffee":1
            }]
        }
    };
}]);