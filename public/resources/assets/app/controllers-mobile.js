var machinesJSON    = 'Hola',
    drinksJSON      = 'Hola',
    brandsJSON      = 'Hola',
    tostionsJSON    = 'Hola',
    glassesJSON     = 'Hola',
    insumosJSON     = 'Hola';
angular.module('scormmomentos.controllers', [
    'com.2fdevs.videogular',
    'com.2fdevs.videogular.plugins.controls'
])
.controller('AppCtrl',
    [
    '$scope',
    '$timeout',
    '$sce',
    '$window',
    'Service',
    function($scope, $timeout, $sce, $window, Service ) {

        var ajaxurl = ajax_elearning.url;
		var ajaxnonce = ajax_elearning.nonce;

        // Watching window size
        $scope.videoIntroPlay = false;

        if ($window.innerWidth>1170) {
            $scope.showOverlay = true;
            window.location = "/e-learning";
        } else {
            $scope.showOverlay = false;
            $scope.videoIntroPlay = true;
        }

        jQuery(window).resize(function(){
            $scope.$apply(function(){
                if ($window.innerWidth>1170) {
                    $scope.showOverlay = true;
                    window.location = "/e-learning";
                } else {
                    $scope.showOverlay = false;
                    $scope.videoIntroPlay = true;
                }
            });
        });

        $scope.assets_url = assets_url;
        $scope.home_url = home_url;
        //console.log(assets_url);
        // Get current SCORM location
        // ScormProcessInitialize();
        var scormProgress = scorm_data;
        // Temporal location scorm data
        var temp_data = "";
        if (scormProgress===undefined || scormProgress=="")
            temp_data = '{"lesson":0,"section":0,"evaluations":[-1,-1,-1,-1,-1,-1,-1,-1,-1,-1]}';
        else
            temp_data = scormProgress;
        // assign scorm data
        var scorm = jQuery.parseJSON( temp_data );
        // Global variables
        var progress,
            data,
            slides,
            tempProgress,
            answerCorrectFirst,
            // evaluations,
            totalCourses,
            course_questions,
            course_content,
            reinit;
        // Get all data for the course from a json file
        data = Service.all();
        user_type = Service.public();
        $scope.user_public = user_type[0].name;
        // evaluations = [];
        totalCourses = 0;
        // for (course_id in data) {
        //     evaluations.push(-1);
        // }
        // evaluations = [-1,-1,-1,-1,-1];
        // totalCourses = Object.keys(evaluations).length;
        totalCourses = 3;
        // if (angular.equals(scorm.evaluations, [-1,-1,-1,-1,-1])) {
        //     scorm.evaluations = evaluations;
        // }
        // console.log("local");
        // console.log(evaluations);
        // Initialize variables

        var acumulatedScore = 0;
        for(var i=0; i<=scorm.evaluations.length;i++){
            if (scorm.evaluations[i] > -1) {
                acumulatedScore += scorm.evaluations[i];
            }
        }
        $scope.score = (acumulatedScore / totalCourses);

        progress = {
            lesson: scorm.lesson,
            section: scorm.section,
            evaluations: scorm.evaluations
        };
        slides = $scope.slides = [];
        firstAnswer = true;
        // $scope variables
        $scope.totalProgress = {
            lesson: scorm.lesson,
            section: scorm.section,
            evaluations: scorm.evaluations
        };
        $scope.showVideo            = true;
        $scope.showIntro            = false;
        $scope.showGlosario         = false;
        $scope.lessonLoaded         = true;
        $scope.activeNext           = false;
        $scope.currentQuestions     = [];
        $scope.currentQuestion      = "0";
        $scope.showedQuestion       = 0;
        $scope.allowNextQuestion    = false;
        $scope.correctAnswers       = 0;
        $scope.showResult           = false;
        $scope.showMenuPopup        = false;
        $scope.showPortfolioPopup   = false;
        $scope.portfolio_index      = 0;
        $scope.showMap              = false;
        $scope.columns_message      = "";
        $scope.showFinalView        = false;
        $scope.showFinalTemplate    = false;
        $scope.showSoftware         = false;
        $scope.diamong_team         = "";
        $scope.diamong_portfolio    = "";
        $scope.diamong_menu         = "";
        $scope.diamong_tactics      = "";
        $scope.diamong_colcafe      = "";
        $scope.case_active          = false;
        $scope.finalQuestionCase    = false;
        $scope.show_yz_feedback     = false;
        $scope.showIntroCourse      = false;
        $scope.showInstructionsView = true;
        $scope.showDiamondIntro     = false;
        $scope.skipIntro            = false;
        $scope.logout               = logout;
        /**
         * Set video configuration
         * @param {string} video name
         */
        $scope.getVideoConfig = function( video ) {
            var config = {
                preload:    "none",
                autoplay:   true,
                theme:      $scope.assets_url + "/resources/assets/bower_components/videogular-themes-default/videogular.css",
                source:     [
                    { src: $sce.trustAsResourceUrl( "https://colcafe-apasionados.s3.amazonaws.com/videos_e-learning/"+video ), type: "video/mp4" }
                    // { src: $sce.trustAsResourceUrl( $scope.assets_url + "/videos/"+video ), type: "video/mp4" }
                ],
                tracks: [
                    {
                        src: "bower_components/subtitles.vtt",
                        kind: "subtitles",
                        srclang: "en",
                        label: "English",
                        default: ""
                    }
                ]
            };
            return config;
        }
        $scope.controller = this;
        $scope.controller.API = null;
        $scope.onPlayerReady = function(API) {
            $scope.controller.API = API;
        }
        // Intro video
        // $scope.videoConfig = $scope.getVideoConfig( "intro.mp4" );
        // Slick params
        $scope.slickConfig = {
            variableWidth: true,
            infinite: false,
            slidesToShow: 2,
            slidesToScroll: 1
        };
        // Slick params
        $scope.slickConfigContent = {
            variableWidth: false,
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            event: {
                afterChange: function (event, slick, currentSlide, nextSlide) {
                    var slides = Object.keys($scope.slides).length;
                    if( (slides-1) == currentSlide ) {
                        $scope.activeNext = true;
                    }
                }
            }
        };
        // Intro.js params
        $scope.IntroOptions = {
            exitOnOverlayClick: false,
            exitOnEsc: true,
            nextLabel: 'Siguiente',
            prevLabel: 'Atrás',
            skipLabel: 'Omitir',
            doneLabel: 'Cerrar'
        };
        // How to use the platform initialization trigger for Intro.js
		$scope.showInstructions = function() {}

        $scope.sessionClose = function() {
            
            return location.href = $scope.logout;
            
        }

        $scope.endIntro = function() {
            $scope.skipTutorial = false;
           
        }

        /**
         * Restart an evaluation
         * @param {number} Current course id
         */
        $scope.resetEvaluation = function() {
            $scope.showResult = false;
            $scope.activeNext = false;
            $scope.correctAnswers = 0;
            $scope.showedQuestion = 0;
            $scope.currentQuestions = $scope.lesson.content.questions;
            $scope.currentQuestionsGroup = Object.keys($scope.currentQuestions).length;
            $scope.evaluationProgress = $scope.showedQuestion / $scope.currentQuestionsGroup;
            $scope.messageClass = null;
            $scope.message = null;
            setTimeout(function() {
                jQuery("input, input[type=checkbox] ").uniform();
            }, 100);
        }

        $scope.hideIntroWelcome = function() {
            $scope.showWelcome = false;
            // Activar para mostrar el video general de introducción y quitar función de skipIntro()
            $scope.showVideo = false;
            $scope.introSkip();
        }

        /**
        * Helpers
        */
        $scope.getCourse = function( progressLesson ){
        return data.find( function( course ) {
            return course.index == parseInt( progressLesson );
        });
        }

        $scope.getCourseIndex = function( progressLesson ){
        return data.indexOf( progressLesson );
        }

        $scope.saveProgress = function( lesson, section ){
        $.ajax({
            method: "POST",
            url: ajaxurl,
            // datatype: 'json',
            data: {
                action: 'update_progress',
                nonce: ajaxnonce,
                lesson: lesson, 
                section: section
            }
        }).done(function( response ){
            console.log( response );
        }).fail(function( error ){
            console.log( error );
        });
        return;
        }

        $scope.saveProgressTest = function( testID, note ){
        $.ajax({
            method: "POST",
            url: ajaxurl,
            data: {
                action  : 'update_test',
                nonce   : ajaxnonce,
                test_id : testID,
                note    : note
            }
        }).done(function( response ){
            console.log( response.data );
        }).fail(function( error ){
            console.log( error );
        });
        return;
        }

        $scope.finishCourse = function(){
        $.ajax({
            method: "POST",
            url: ajaxurl,
            data: {
                action  : 'finish_course',
                nonce   : ajaxnonce,
            }
        }).done( function( response ) {
            console.log( response.data );
        }).fail( function( error ) {
                console.log( error );
        });
        return;
        }

        /**
         * Load data dynamically
         * @param {object} progress
         * @param {boolean} re-init slider
         */
        $scope.loadData = function( progress, reinit ) {
          
            $scope.evaluationFinished = false;
            $scope.showMap = false;
            if (reinit)
                $scope.lessonLoaded = false;
            // Get general data
            tempProgress = {
                lesson: progress.lesson,
                section: progress.section
            }

            var indexesMenoresA80 = [];
			
            for(var index in progress.evaluations) {
             if ( progress.evaluations[index] < 80) {
                indexesMenoresA80.push(index);
              }
            }

            //$scope.coursesmenoresporcentaje = indexesMenoresA80.join(',');
            $scope.coursesmenoresporcentaje = indexesMenoresA80.length;
            $scope.courses      = data;

            setTimeout(function() {
              var loadingIcon = document.getElementById('loadingIcon');

              var video = document.querySelector('.iframe-video video');
              
              if(video){
				            
  				        

              video.onloadedmetadata = function() {
				        loadingIcon.style.display = 'none';	
               //video.currentTime = 2;
               //alert("Se inicializo");

                //video.preload = 'auto';
                video.play();
                
              };
              
              video.onloadedmetadata();
              video.onended = function() {
                 video.currentTime = 2; 
                 //alert("Se finalizo");
               };
              
                }     
            }, 500);
            if(progress.lesson >= Object.keys($scope.courses).length) {
                $scope.showFinalView = true;
                $scope.activeNext = false;
                $scope.showFinalTemplate = true;
                if($scope.score >= 80) {
                  
                  setTimeout(function () {

                    jQuery(".capitulos-nav ul li a:contains('Prueba tu conocimiento')").css({'opacity': '0.3', 'pointer-events': 'none'});
                     jQuery(".capitulos-nav ul li a").attr("ng-click", "");
                }, 30);
                    $scope.finalPassed = true;
                    $scope.finalFailed = false;
                } else {
                    $scope.finalPassed = false;
                    $scope.finalFailed = true;
                }
                progress.lesson = (Object.keys($scope.courses).length) - 1;
            }
            $scope.course       = data[ parseInt(progress.lesson) ];
            $scope.nextCourse   = data[ parseInt(progress.lesson) + 1 ];
            $scope.lessons      = $scope.course.content;
            $scope.lesson       = $scope.course.content[progress.section];
            if( $scope.totalProgress.section == 0 ) {
                $scope.introModule  = $scope.course.intro;
                $scope.introModuleTemplate = $scope.course.template;
            }
            //$scope.nextLesson   = $scope.course.content[($scope.totalProgress.section + 1)];

            if($scope.course==undefined) {
                $scope.nextLesson = undefined;
            } else {
                $scope.nextLesson = $scope.course.content[(progress.section + 1)];
            }
            $scope.progress     = progress;
            $scope.videosWatched = [];
            $scope.menuElementsWatched = [];
            $scope.portfolioElementsWatched = [];
            // Content types
            switch($scope.lesson.type){
                case "1":
                    if (tempProgress.lesson < $scope.totalProgress.lesson || ((tempProgress.lesson == $scope.totalProgress.lesson) && (tempProgress.section < $scope.totalProgress.section))) {
                        $scope.activeNext = true;
                    } else {
                        $scope.activeNext = false;
                    }
                    $scope.showDiamondIntro = $scope.lesson.intro;
                    $scope.introDiamondTemplate = "";
                    if($scope.showDiamondIntro) {
                        $scope.introDiamondTemplate = $scope.lesson.introtemplate;
                    }
                    $scope.case_active = false;
                    if($scope.showVideo) {
                        $scope.video_selected = $scope.lesson.content;
                        $scope.videoConfig = $scope.getVideoConfig( $scope.lesson.content );
                    } else {
                        $scope.videoConfig = $scope.getVideoConfig( $scope.lesson.content );
                    }
                break;
                case "2":
                    if( $scope.lesson.content.type == "a" ) {
                        if (scorm.evaluations[$scope.course.index] != -1) {
                            $scope.showResult = true;
                            $scope.activeNext = true;
                            $scope.allowNextQuestion = false;
                            $scope.messageClass = "";
                            $scope.message = null;
                            $scope.showedQuestion = -1;
                            var _questions;
                            for(question in $scope.courses[$scope.course.index].content) {
                                if ($scope.courses[$scope.course.index].content[question].type == "2")
                                    _questions = Object.keys($scope.courses[$scope.course.index].content[question].content.questions).length;
                            }

                            $scope.currentQuestionsGroup = _questions;
                            $scope.correctAnswers = scorm.evaluations[$scope.course.index] / 100 * _questions;
                        }
                        else {
                            $scope.intro_active = $scope.lesson.content.intro;
                            $scope.template_intro = $scope.lesson.content.template;
                            $scope.resetEvaluation();
                        }
                    } else if ( $scope.lesson.content.type == "b" ) {
                        if (tempProgress.lesson < $scope.totalProgress.lesson || ((tempProgress.lesson == $scope.totalProgress.lesson) && (tempProgress.section < $scope.totalProgress.section))) {
                            $scope.activeNext = true;
                        } else {
                            $scope.activeNext = false;
                        }
                        $scope.intro_active = $scope.lesson.content.intro;
                        $scope.template_intro = $scope.lesson.content.template;
                        $scope.showedQuestion = 0;
                        $scope.questions = $scope.lesson.content.questions;
                        $scope.class_column_b = "";
                        getColumnQuestions();
                    }
                    $scope.case_active = false;
                break;
                case "3":
                    if (tempProgress.lesson < $scope.totalProgress.lesson || ((tempProgress.lesson == $scope.totalProgress.lesson) && (tempProgress.section < $scope.totalProgress.section))) {
                        $scope.activeNext = true;
                    } else {
                        $scope.activeNext = false;
                    }
                    $scope.case_active = false;
                    $scope.showDiamondIntro = $scope.lesson.intro;
                    $scope.introDiamondTemplate = "";
                    if($scope.showDiamondIntro) {
                        $scope.introDiamondTemplate = $scope.lesson.introtemplate;
                    }
                    $timeout(function () {
                        $scope.slickLoaded = true;
                    }, 10);
                    $scope.slides = $scope.lesson.content;
                break;
                case "4":
                    if (tempProgress.lesson < $scope.totalProgress.lesson || ((tempProgress.lesson == $scope.totalProgress.lesson) && (tempProgress.section < $scope.totalProgress.section))) {
                        $scope.activeNext = true;
                    } else {
                        $scope.activeNext = false;
                    }
                    $scope.case_active = false;
                    $scope.video_gallery = false;
                    $scope.videoIndex = -1;
                    $scope.videos = $scope.lesson.content.playlist;
                break;
                case "5":
                    if (tempProgress.lesson < $scope.totalProgress.lesson || ((tempProgress.lesson == $scope.totalProgress.lesson) && (tempProgress.section < $scope.totalProgress.section))) {
                        $scope.activeNext = true;
                    } else {
                        $scope.activeNext = false;
                    }
                    $scope.showDiamondIntro = $scope.lesson.intro;
                    $scope.introDiamondTemplate = "";
                    if($scope.showDiamondIntro) {
                        $scope.introDiamondTemplate = $scope.lesson.introtemplate;
                    }
                    $scope.case_active = false;
                    $scope.menudrinks = $scope.lesson.content;
                break;
                case "6":
                    if (tempProgress.lesson < $scope.totalProgress.lesson || ((tempProgress.lesson == $scope.totalProgress.lesson) && (tempProgress.section < $scope.totalProgress.section))) {
                        $scope.activeNext = true;
                    } else {
                        $scope.activeNext = false;
                    }
                    $scope.showDiamondIntro = $scope.lesson.intro;
                    $scope.introDiamondTemplate = "";
                    if($scope.showDiamondIntro) {
                        $scope.introDiamondTemplate = $scope.lesson.introtemplate;
                    }
                    $scope.showPortfolioPopup = false;
                    $scope.portfolio = $scope.lesson.portfolio;
                    $scope.portfolio_items = $scope.portfolio.items;
                    $scope.portfolio_back = false;
                    $scope.portfolio_next = false;
                    $scope.case_active = false;
                break;
                case "7":
                    $scope.activeNext = true;
                    $scope.html_template = $scope.lesson.template;
                    $scope.case_active = false;
                break;
                case "8":
                    if (tempProgress.lesson < $scope.totalProgress.lesson || ((tempProgress.lesson == $scope.totalProgress.lesson) && (tempProgress.section < $scope.totalProgress.section))) {
                        $scope.activeNext = true;
                    } else {
                        $scope.activeNext = false;
                    }
                    $scope.total_case_index = 0;
                    $scope.current_case_index = 0;
                    $scope.case_active = true;
                    $scope.template = $scope.lesson.final;
                    $scope.finalQuestionCase = false;
                    $scope.loadCaseData();
                break;
                case "9":
                    if (tempProgress.lesson < $scope.totalProgress.lesson || ((tempProgress.lesson == $scope.totalProgress.lesson) && (tempProgress.section < $scope.totalProgress.section))) {
                        $scope.activeNext = true;
                    } else {
                        $scope.activeNext = false;
                    }
                    $scope.intro_active = true;
                    $scope.template_intro = "diamante";
                    $scope.case_active = false;
                break;
                case "10":
                    if (tempProgress.lesson < $scope.totalProgress.lesson || ((tempProgress.lesson == $scope.totalProgress.lesson) && (tempProgress.section < $scope.totalProgress.section))) {
                        $scope.activeNext = true;
                    } else {
                        $scope.activeNext = false;
                    }
                    $scope.showDiamondIntro = $scope.lesson.intro;
                    $scope.introDiamondTemplate = "";
                    if($scope.showDiamondIntro) {
                        $scope.introDiamondTemplate = $scope.lesson.introtemplate;
                    }
                    $scope.case_active = false;
                    $scope.multiple_current_index = 0;
                    $scope.loadMultipleContent();
                break;
                case "11":
                    if (tempProgress.lesson < $scope.totalProgress.lesson || ((tempProgress.lesson == $scope.totalProgress.lesson) && (tempProgress.section < $scope.totalProgress.section))) {
                        $scope.activeNext = true;
                    } else {
                        $scope.activeNext = false;
                    }
                    $scope.case_active = false;
                    $scope.showDiamondIntro = $scope.lesson.intro;
                    $scope.introDiamondTemplate = "";
                    if($scope.showDiamondIntro) {
                        $scope.introDiamondTemplate = $scope.lesson.introtemplate;
                    }
                    $timeout(function () {
                        $scope.slickLoaded = true;
                    }, 10);
                    $scope.slides = $scope.lesson.content;
                break;
                case "12":
                    if (tempProgress.lesson < $scope.totalProgress.lesson || ((tempProgress.lesson == $scope.totalProgress.lesson) && (tempProgress.section < $scope.totalProgress.section))) {
                        $scope.activeNext = true;
                    } else {
                        $scope.activeNext = false;
                    }
                    $scope.case_active = false;
                    $scope.showDiamondIntro = $scope.lesson.intro;
                    $scope.introDiamondTemplate = "";
                    if($scope.showDiamondIntro) {
                        $scope.introDiamondTemplate = $scope.lesson.introtemplate;
                    }
                    $timeout(function () {
                        $scope.slickLoaded = true;
                    }, 10);
                    $scope.slides = $scope.lesson.content;
                break;
                default:
                    $scope.case_active = false;
                    // Asking for the last section
                    if (($scope.nextLesson == undefined) && ($scope.nextCourse == null)) {
                        $scope.activeNext = false;
                    } else {
                        $scope.activeNext = true;
                    }
                break;
            }
            $scope.hideIntro = function() {$scope.intro_active = false;}
            $scope.hideIntroModule = function() {$scope.introModule = false;}
            $scope.hideIntroCourse = function() {
                $scope.showIntroCourse = false;
                $scope.showIntro = false;
                $scope.skipTutorial = true;
                $scope.showInstructionsView = false;
                setTimeout(function() {
                 
                  var video = document.querySelector('.iframe-video video');
                  var loadingIcon = document.getElementById('loadingIcon');
    
                  if(video){
                 //alert('video');
                      loadingIcon.style.display = 'block';
    
                  video.onloadedmetadata = function() {
                    loadingIcon.style.display = 'none';				
                   //video.currentTime = 2;
                   //alert("Se inicializo");
    
                    //video.preload = 'auto';
                    video.play();
                  };
                  video.onloadedmetadata();
                  video.onended = function() {
                     video.currentTime = 2; 
                     //alert("Se finalizo");
                   };
                  
                    }     
                }, 200);                /*if (progress.lesson==0 && progress.section==0) {
                    $scope.showIntro = false;
                    $scope.showInstructionsView = false;
                } else {
                    $scope.showIntro = false;
                    $scope.showInstructionsView = false;
                }*/
            }
            $scope.hideCaseIntro = function() {
                $scope.intro_active = false;
                if($scope.lesson.content[$scope.current_case_index +1] != undefined) {
                    if($scope.current_case_index == $scope.total_case_index) {
                        $scope.current_case_index++;
                        $scope.total_case_index++;
                    }
                    if($scope.current_case_index < $scope.total_case_index) {
                        $scope.current_case_index++;
                    }
                    $scope.loadCaseData();
                } else {
                    $scope.activeNext = true;
                }
            }
            $scope.nextCaseQuestion = function() {
                if($scope.lesson.content[$scope.current_case_index +1] != undefined) {
                    if($scope.current_case_index == $scope.total_case_index) {
                        $scope.current_case_index++;
                        $scope.total_case_index++;
                    }
                    if($scope.current_case_index < $scope.total_case_index) {
                        $scope.current_case_index++;
                    }
                    $scope.loadCaseData();
                    $scope.finalQuestionCase = false;
                } else {
                    $scope.finalQuestionCase = true;
                    $scope.activeNext = true;
                }
            }
            $scope.nextCaseQuestionZ = function() {
                var assumed = true,
                    reference = $scope.case.content,
                    corrects = {},
                    counterC = 0,
                    counterU = 0;
                for (var index in reference) {
                    if( reference[index].correct ) {
                        corrects[index] = reference[index].correct;
                        counterC++;
                    }
                }
                if(Object.keys($scope.matched).length <= 0) {
                    assumed = false;
                }
                if(assumed) {
                    for (var index in $scope.matched) {
                        if($scope.matched[index]) {
                            if($scope.matched[index] != $scope.case.content[index].correct) {
                                assumed = false;
                            } else {
                                counterU++;
                            }
                        }
                    }
                }
                if(assumed && (counterC != counterU)) {
                    assumed = false;
                }
                $scope.case_question_z_feedback = "";
                $scope.show_yz_feedback = true;
                if(assumed) {
                    $scope.active_sec_button = true;
                    jQuery("input[type=checkbox]").attr("disabled", "disabled");
                    jQuery("input[type=radio]").attr("disabled", "disabled");
                    $scope.case_question_z_feedback = "La respuesta es correcta, para continuar da clic al botón siguiente.";
                    $scope.messageClass = "answer-correct";
                } else {
                    $scope.case_question_z_feedback = "La respuesta es incorrecta";
                    $scope.messageClass = "answer-incorrect";
                    $timeout(function () {
                        $scope.show_yz_feedback = false;
                    }, 2000);
                }
            }
            $scope.DiamondSelected = function( value ) {
                switch (value) {
                    case 1:
                        $scope.diamong_team = "Equipos";
                    break;
                    case 3:
                        $scope.diamong_portfolio = "Portafolio";
                    break;
                    case 7:
                        $scope.diamong_menu = "Carta de bebidas y costeo";
                    break;
                    case 9:
                        $scope.diamong_tactics = "Tácticas de aumento de consumo";
                    break;
                    case 10:
                        $scope.diamong_colcafe = "Propuesta de valor Colcafé S.A.S";
                    break;
                }
                if($scope.diamong_team != "" && $scope.diamong_portfolio != "" && $scope.diamong_menu != "" && $scope.diamong_tactics != "" && $scope.diamong_colcafe != "") {
                    $scope.activeNext = true;
                }
            }
            // Get current progress
            $scope.status = {
                isOpen: new Array( $scope.courses.length ),
                sections: Object.keys($scope.courses[progress.lesson].content).length
            };
            // Set video from gallery
            $scope.setVideo = function( video_name, index ) {
                $scope.videoIndex = index;
                $scope.videoConfig = $scope.getVideoConfig( video_name );
                $scope.video_gallery = true;

                setTimeout(function() {
            
                  var video = document.querySelector('.iframe-video video');
                  var loadingIcon = document.getElementById('loadingIcon');
    
                  if(video){
                 //alert('video');
                      loadingIcon.style.display = 'block';
    
                  video.onloadedmetadata = function() {
                    loadingIcon.style.display = 'none';				
                   //video.currentTime = 2;
                   //alert("Se inicializo");
    
                    //video.preload = 'auto';
                    video.play();
                  };
                  video.onloadedmetadata();
                  video.onended = function() {
                     video.currentTime = 2; 
                     //alert("Se finalizo");
                   };
                  
                    }     
                }, 200);
            };
            $scope.hideVideo = function() {
                $scope.video_gallery = false;
            };
            // Count watched videos in gallery
            $scope.countVideos = function( index ) {
                $scope.videosWatched[index] = true;
                if(Object.keys($scope.videosWatched).length == Object.keys($scope.videos).length ) {
                    $scope.activeNext = true;
                }
                $scope.hideVideo();
            }
            // Set current progress
            $scope.status.isOpen[ progress.lesson ] = true;
            if ( progress.lesson < $scope.totalProgress.lesson )
                $scope.localProgress = 100;
            else
                $scope.localProgress = ($scope.totalProgress.section / $scope.status.sections) * 100;
            if (reinit) {
                $timeout(function () {
                    $scope.lessonLoaded = true;
                }, 10);
            }
        }

        $scope.hideDiamondIntro = function() {
            $scope.showDiamondIntro = false;
        }

        $scope.goBackCase = function() {
            $scope.current_case_index--;
            $scope.loadCaseData();
        }

        $scope.loadMultipleContent = function() {
            $scope.multiple_current = $scope.lesson.content[$scope.multiple_current_index];
            if($scope.multiple_current.type == "10-a") {
                $scope.videoConfig = $scope.getVideoConfig( $scope.multiple_current.source );
            }
            if($scope.multiple_current.type == "10-b") {
                if($scope.lesson.content[($scope.multiple_current_index+1)] == undefined) {
                    $scope.activeNext = true;
                }
            }
            if($scope.multiple_current.type == "10-c") {
                $scope.slides = $scope.multiple_current.source;
                $timeout(function () {
                    $scope.slickLoaded = true;
                }, 10);
            }
        }

        $scope.loadCaseData = function() {

            // Show go back button
            if($scope.current_case_index > 0) {
                $scope.showCaseBackButton = true;
            } else {
                $scope.showCaseBackButton = false;
            }

            $scope.case = $scope.lesson.content[$scope.current_case_index];
            $scope.active_next_y = false;
            $scope.show_yz_feedback = false;
            $scope.active_sec_button = false;
            $scope.hideSoftwareBanner = false;
            jQuery("input[type=radio]").removeAttr("disabled");
            jQuery("input[type=radio]").prop("checked", false);
            jQuery("input[type=checkbox]").removeAttr("disabled");
            jQuery("input[type=checkbox]").prop("checked", false);
            $scope.costeoSoftwareTemplate = $scope.lesson.template;
            switch ($scope.case.type) {
                case "u":
                    $scope.z_options = $scope.case.content;
                    $scope.matched = {};
                    if($scope.total_case_index > $scope.current_case_index) {
                        $scope.active_sec_button = true;
                    }
                break;
                case "v":
                    $scope.z_options = $scope.case.content;
                    $scope.matched = {};
                    if($scope.total_case_index > $scope.current_case_index) {
                        $scope.active_sec_button = true;
                    }
                break;
                case "w":
                    if($scope.case.hideSoftwareBanner) {
                        $scope.hideSoftwareBanner = true;
                    }
                    $scope.matched = {};
                    if($scope.total_case_index > $scope.current_case_index) {
                        $scope.active_sec_button = true;
                    }
                break;
                case "x":
                    $scope.intro_active = true;
                    $scope.template_content = $scope.case.template;
                break;
                case "y":
                    $scope.case_question_y_feedback = "";
                    $scope.y_options = $scope.case.content;
                    if($scope.total_case_index > $scope.current_case_index) {
                        $scope.active_next_y = true;
                    }
                break;
                case "z":
                    $scope.case_question_z_feedback = "";
                    $scope.z_options = $scope.case.content;
                    $scope.matched = {};
                    if($scope.total_case_index > $scope.current_case_index) {
                        $scope.active_sec_button = true;
                    }
                break;
            }
            setTimeout(function() {
                jQuery("input, input[type=checkbox] ").uniform();
            }, 100);
        }
        $scope.validateCaseQuestionZ = function ( index ) {
            if($scope.matched[index] == undefined) {
                $scope.matched[index] = true;
            } else if(!$scope.matched[index]) {
                $scope.matched[index] = true;
            } else {
                $scope.matched[index] = false;
            }
        }
        $scope.validateCaseQuestionZ2 = function ( index ) {
            $scope.matched = [];
            $scope.matched[index] = true;
        }
        $scope.getMenuDrinks = function( index ) {
            $scope.machine = $scope.menudrinks.menu[index].machine;
            $scope.menu_drinks_options = $scope.menudrinks.menu[index].collection;
            $scope.menu_text = $scope.menudrinks.menu[index].text;
            $scope.showMenuPopup = true;
            $scope.menuElementsWatched[index] = true;
            if(Object.keys($scope.menuElementsWatched).length == Object.keys($scope.menudrinks.menu).length) {
                $scope.activeNext = true;
            }
        }
        $scope.getPortfolioDetails = function( index ) {
            $scope.portfolio_index = index;
            $scope.portfolio_back = true;
            $scope.portfolio_next = true;
            if($scope.portfolio_index == 0) {
                $scope.portfolio_back = false;
            }
            if($scope.portfolio_index == (Object.keys($scope.portfolio_items).length)-1) {
                $scope.portfolio_next = false;
            }
            $scope.portfolio_details = $scope.portfolio_items[index];
            $scope.showPortfolioPopup = true;
            $scope.portfolioElementsWatched[index] = true;
            if(Object.keys($scope.portfolioElementsWatched).length == Object.keys($scope.portfolio_items).length) {
                $scope.activeNext = true;
            }
        }
        $scope.hideMenuPopup = function() {
            $scope.showMenuPopup = false;
        }
        $scope.hidePortfolioPopup = function() {
            $scope.showPortfolioPopup = false;
        }
        function getColumnQuestions() {
            $scope.columns_message = "";
            if( $scope.showedQuestion < Object.keys($scope.questions).length ) {
                $scope.showNextColumnButton = false;
                $scope.progressColumn = [];
                $scope.column_a = $scope.lesson.content.questions[$scope.showedQuestion].a;
                $scope.column_b = $scope.lesson.content.questions[$scope.showedQuestion].b;
                if($scope.showedQuestion == (Object.keys($scope.questions).length - 1)) {
                    $scope.lastColumnQuestions = true;
                } else {
                    $scope.lastColumnQuestions = false;
                }
            } else {
                $scope.class_column_b = "";
                $scope.next();
            }
        }
        $scope.validateCaseQuestionY = function( index ) {
            var result = $scope.case.content[index].correct;
            $scope.case_question_y_feedback = "";
            $scope.show_yz_feedback = true;
            if( result ) {
                $scope.active_next_y = true;
                jQuery("input[type=radio]").attr("disabled", "disabled");
                $scope.case_question_y_feedback = "La respuesta es correcta";
                $scope.messageClass = "answer-correct";
            } else {
                $timeout(function () {
                    $scope.show_yz_feedback = false;
                }, 2000);
                $scope.case_question_y_feedback = "La respuesta es incorrecta";
                $scope.messageClass = "answer-incorrect";
            }
        }
        $scope.validateColumnsAnswer = function( index, value ) {
            $scope.progressColumn[index] = value;
            if(Object.keys($scope.progressColumn).length == Object.keys($scope.column_b).length ) {
                $scope.showNextColumnButton = true;
            }
        }
        $scope.nextColumn = function() {
            var flag = true;
            if(Object.keys($scope.progressColumn).length == Object.keys($scope.column_b).length ) {
                for (var answer in $scope.progressColumn) {
                    if( !$scope.progressColumn[answer] ) {
                        flag = false;
                        break;
                    }
                }
                if( flag ) {
                    $scope.showedQuestion++;
                    $scope.class_column_b = "info-colums-2";
                    $scope.columns_message = "¡Felicitaciones! Puedes pasar al siguiente módulo.";
                    // Lugar para sumar el progreso de esta evaluación.
                    getColumnQuestions();
                } else {
                    $scope.columns_message = "Respuestas equivocadas";
                }
            } else {
                $scope.columns_message = "Faltan preguntas por asociar";
            }
        }
        $scope.showColcafeMap = function() {
            $scope.showMap = true;
            $scope.introModule = false;
            $scope.slickLoaded = true;
            $scope.controller.API.pause();
        }
        /**
         * INIT APP DATA
         */
        $scope.loadData( progress, false );

        /**
         * General welcome!
         */
        $scope.showWelcome = false;
        $scope.showIntroCourse = true;
        /*if((scorm.lesson == "0") && (scorm.section == "0")) {
            $scope.showWelcome = true;
        } else {
            $scope.showWelcome = false;
            $scope.skipTutorial = true;
        }*/

        /**
         * Go to next step
         */
        $scope.next = function() {
            $scope.slickLoaded = false;
            var temp = progress;
            progress.section = tempProgress.section + 1;
            progress.lesson = tempProgress.lesson;
            var reinit = false;
            var isUpgradeable = false;
            if ($scope.status.sections == progress.section) {
                reinit = true;
                progress.section = 0;
                progress.lesson++;
            }

             /**
            * Check if is a correct saveProgress call.
            */
              if ( (
                ( parseInt( temp.lesson ) == parseInt( $scope.totalProgress.lesson ) ) && 
                ( parseInt( temp.section ) > parseInt( $scope.totalProgress.section ) ) ) || 
                ( parseInt( temp.lesson ) > parseInt( $scope.totalProgress.lesson ) )
            ) isUpgradeable = true;

            if (temp.lesson > $scope.totalProgress.lesson) {
                $scope.totalProgress.section = 0;
                $scope.totalProgress.lesson = temp.lesson;
            }
            if ((temp.lesson == $scope.totalProgress.lesson) && (temp.section > $scope.totalProgress.section)) {
                $scope.totalProgress.section = temp.section;
            }
            // Asking for the last section
            // setTimeout(function () {
                /*if (($scope.nextLesson == undefined) && ($scope.nextCourse == null)) {
                    $scope.showFinalView = true;
                    $scope.activeNext = false;
                }*/
            if ( ( $scope.nextLesson === undefined ) && ( $scope.nextCourse === undefined ) ) {
                //last condition comercial case
                if( $scope.user_public == 'comercial' ){
                    $scope.score = 100;
                    $scope.saveProgressTest( $scope.course.index, $scope.score );
                }

                $scope.showFinalView = true;
                $scope.activeNext = false;
            }                
            // }, 30);

            // ScormProcessSetValue( "cmi.core.lesson_location", JSON.stringify($scope.totalProgress) );
            
            if( isUpgradeable ) $scope.saveProgress( $scope.totalProgress.lesson, $scope.totalProgress.section );
            if(!$scope.showFinalView) {
                $scope.loadData( progress, reinit );
            } else {
                $scope.showFinalTemplate = true;
                if($scope.score >= 80) {
             
                    $scope.finalPassed = true;
                    $scope.finalFailed = false;
                    $scope.localProgress = 100;
                    //send mail finish course
                    $scope.finishCourse();
                    

                } else {
                    $scope.finalPassed = false;
                    $scope.finalFailed = true;
                    $scope.localProgress = 100;
                }
            }
        }
        /**
         * Go to a specific lesson-section
         * @param {number} lesson
         * @param {number} section
         */
        $scope.goTo = function( lesson, section ) {
            $scope.showFinalTemplate = false;
            $scope.showMap = false;
            $scope.slickLoaded = false;
            progress.lesson = lesson;
            progress.section = section;
            reinit = false;
            if ($scope.course.index!=lesson) {
                reinit = true;
            }
            if (( lesson < $scope.totalProgress.lesson ) || ( (lesson == $scope.totalProgress.lesson) && (section <= $scope.totalProgress.section) )) {
                tempProgress = {
                    lesson: lesson,
                    section: section
                }
                $scope.loadData( tempProgress, reinit );
            } else {
                if( $scope.lesson.type == "3" ) {
                    $timeout(function () {
                        $scope.slickLoaded = true;
                    }, 10);
                }
                if( $scope.lesson.type == "1" ) {
                    $timeout(function () {
                        $scope.videoConfig = $scope.getVideoConfig( $scope.lesson.content );
                    }, 10);
                }
                if ( (tempProgress.lesson == $scope.totalProgress.lesson && section == ($scope.totalProgress.section + 1)) && ($scope.activeNext) )
                    $scope.next();
            }
        }
        $scope.verifyNextStatus = function() {
            if($scope.evaluationFinished) {
                $scope.activeNext = true;
            }
        }
        $scope.closeMap = function() {
            $scope.showMap = false;
            $scope.controller.API.play();
        }
        /**
         * Activate next link after video finish
         */
        $scope.nextStep = function() {$scope.activeNext = true;}
        $scope.nextStepMultiple = function() {
            $scope.multiple_current_index++;
            if($scope.lesson.content[$scope.multiple_current_index] == undefined) {
                $scope.activeNext = true;
            } else {
                $scope.loadMultipleContent();
            }
        }
        /**
         * Skip video to introduce the course
         */
        $scope.introSkip = function() {
            $scope.showVideo = false;
            $scope.videoConfig = $scope.getVideoConfig( $scope.video_selected );
            if (progress.section==0) {
                $scope.showIntroCourse = true;
            } else {
                $scope.skipTutorial = true;
                $scope.showIntroCourse = false;
            }
            
        }
        /**
         * Skip instructions to use the platform
         */
        $scope.instructionsSkip = function() {
          
            $scope.showIntro = false;
            $scope.skipTutorial = true;
            $scope.showInstructionsView = false;
        }

        $scope.showVideoStopped = function() {
            // $timeout(function () {
            //     $scope.controller.API.pause();
            // }, 100);
        }
        /**
         * Answer to question type a
         * @param {number} answer index
         */
        $scope.validateAnswer = function( index ) {
            var answer = $scope.currentQuestions[$scope.showedQuestion].answers[index];
            $scope.message = answer.feedback;
            if (answer.correct) {
                if (firstAnswer)
                    $scope.correctAnswers++;
                jQuery("input[type=radio]").attr("disabled", "disabled");
                $scope.allowNextQuestion = true;
                $scope.messageClass = "answer-correct";
            } else {
                firstAnswer = false;
                $scope.allowNextQuestion = false;
                $scope.messageClass = "answer-incorrect";
            }
        }
        /**
         * Go to next question
         */
        $scope.nextQuestion = function() {
          
            $scope.showedQuestion++;
            $scope.evaluationProgress = ($scope.showedQuestion / $scope.currentQuestionsGroup)*100;
            $scope.allowNextQuestion = false;
            $scope.messageClass = "";
            $scope.message = null;
            $scope.evaluationFinished = false;
            if ($scope.currentQuestionsGroup == $scope.showedQuestion) {
                $scope.evaluationFinished = true;
                $scope.showResult = true;
                $scope.activeNext = true;
                // Asking for the last section
                setTimeout(function () {
                    if (($scope.nextLesson == undefined) && ($scope.nextCourse == null)) {
                        $scope.activeNext = false;
                    } else {
                        $scope.activeNext = true;
                    }
                }, 30);
                var percentageCorrect = ($scope.correctAnswers / $scope.currentQuestionsGroup)*100;
                if(percentageCorrect >= 80) {
                  $scope.activeNext = true;
                    $scope.showResultMessage_approved = true;
                    $scope.showResultMessage_failed = false;
                } else {
                  $scope.activeNext = false;

                    $scope.showResultMessage_approved = false;
                    $scope.showResultMessage_failed = true;
                }
                if ((percentageCorrect > scorm.evaluations[$scope.course.index]) || (scorm.evaluations[$scope.course.index] == -1))
                    scorm.evaluations[$scope.course.index] = percentageCorrect;
                var acumulatedScore = 0,
                    countSums = 0;
                for(value in scorm.evaluations){
                    if (scorm.evaluations[value] > -1) {
                        countSums++;
                        acumulatedScore += scorm.evaluations[value];
                    }
                }
                $scope.score = (acumulatedScore / totalCourses);
                // ScormProcessSetValue( "cmi.core.lesson_location", JSON.stringify(progress) );
                /**
                 * Update Progress
                */
                $scope.saveProgress( $scope.totalProgress.lesson, $scope.totalProgress.section );
                // ScormProcessSetValue( "cmi.core.score.min", "0" );
                // ScormProcessSetValue( "cmi.core.score.max", "100" );
                // ScormProcessSetValue( "cmi.core.score.raw", $scope.score );
                var aficionadoColaborador = [0, 1, 3, 5, 8, 9];
                var arrayComercial = [0, 6]
                var emprendedorBarista = [0, 2, 5, 8, 9]

                switch($scope.user_public){
                    case 'aficionado':
                    case 'colaborador':
                        if ($scope.course.index == 3) {
                            $scope.course.index = aficionadoColaborador[2];
                        }else if($scope.course.index == 4){
                            $scope.course.index = aficionadoColaborador[3];
                        }else if($scope.course.index == 5){
                            $scope.course.index = aficionadoColaborador[4];
                        }
                        break;
                    case 'barista-chef':
                    case 'emprendedor':
                        if ($scope.course.index == 1) {
                            $scope.course.index = emprendedorBarista[1];
                        }else if($scope.course.index == 3){
                            $scope.course.index = emprendedorBarista[2];
                        }else if($scope.course.index == 4){
                            $scope.course.index = emprendedorBarista[3];
                        }else if($scope.course.index == 5){
                            $scope.course.index = emprendedorBarista[4];
                        }
                        break;
                    case 'comercial':
                        if ($scope.course.index == 1) {
                            $scope.course.index = arrayComercial[1];
                        }
                        break;
                }
                /**
                 * Update Test Ajax
                 */
                // console.log( 'update test');
                // debugger;
                $scope.saveProgressTest( $scope.course.index, percentageCorrect );

                if ((countSums == 5) && ($scope.score < 80)) {
                    // ScormProcessSetValue( "cmi.core.lesson_status", "failed" );
                }
                if ((countSums == 5) && ($scope.score >= 80)) {
                    // ScormProcessSetValue( "cmi.core.lesson_status", "complete" );
                    // ScormProcessSetValue( "cmi.core.lesson_status", "passed" );
                }
                if (countSums < 5) {
                    // ScormProcessSetValue( "cmi.core.lesson_status", "incomplete" );
                }
            }
            firstAnswer = true;
            setTimeout(function() {
                jQuery("input, input[type=checkbox] ").uniform();
            }, 100);
        }
        /**
         * Close course in SCORM
         */
        $scope.closeCourse = function() {
            // ScormProcessFinish();
        }

        $scope.$on('$destroy', function() {
            delete window.onbeforeunload;
        });

        window.onbeforeunload = function() {
            $scope.closeCourse();
        }
        /*
         * Software de costeo
         */
        $scope.showSoftwareCosteo = function() {
            $scope.showSoftware = true;
        }
        $scope.hideSoftwareCosteo = function() {
            $scope.showSoftware = false;
        }
}])
.controller('CosteoCtrl',
    [
    '$scope',
    'Service',
    function($scope, Service) {
        machinesJSON    = Service.machines();
        drinksJSON      = Service.drinks();
        brandsJSON      = Service.brands();
        tostionsJSON    = Service.tostions();
        glassesJSON     = Service.glasses();
        insumosJSON     = Service.insumos();
}]);
