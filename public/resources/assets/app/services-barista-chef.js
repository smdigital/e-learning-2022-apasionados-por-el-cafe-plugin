angular.module('scormmomentos.services', [])
.factory('Service',[
    '$q',
    '$resource',
    '$location',
    function(
        $q,
        $resource,
        $location
    ){
    return {
        public: function() {
            return [{
                "name": "barista-chef"
            }]
        },
        all : function() {
            return [{
                // MODULO 1
                "index": "0",
                "name": "Café y Cultura",
                "unique": "cultura",
                "time": "1h",
                "intro": false,
                "template": "intro-1",
                "content": {
                    "0": {
                        "index": "1.1",
                        "name": "Introducción Café y Cultura",
                        "image": "modulo1-1.png",
                        "time": "00:33",
                        "type": "1",
                        "content": "intro_cafe_y_cultura.mp4"
                    },
                    "1": {
                        "index": "1.2",
                        "name": "Historia del Café",
                        "image": "modulo1-1.png",
                        "time": "03:33",
                        "type": "1",
                        "content": "historia-interactiva-del-cafe.mp4"
                    },
                    "2": {
                        "index": "1.3",
                        "name": "El café en Colombia",
                        "image": "modulo1-2.png",
                        "time": "04:30",
                        "type": "3",
                        "content": [
                            "modulo-1/infografico-1.png",
                            "modulo-1/infografico-2.png",
                            "modulo-1/infografico-3.png",
                            "modulo-1/infografico-4.png",
                            "modulo-1/infografico-5.png",
                            "modulo-1/infografico-7.png",
                            "modulo-1/infografico-6.png",
                            "modulo-1/infografico-8.png",
                            "modulo-1/infografico-9.png"
                        ]
                    },
                    "3": {
                        "index": "1.4",
                        "name": "Proceso agrícola e industrial",
                        "image": "modulo2-1.png",
                        "time": "08:45",
                        "type": "1",
                        "content": "los-procesos-colcafe.mp4"
                    },
                    "4": {
                        "index": "1.5",
                        "name": "Proceso del café tostado y molido",
                        "image": "modulo2-3.png",
                        "time": "02:30",
                        "type": "3",
                        "content": [
                            "modulo-2/infografico-1.png",
                            "modulo-2/infografico-6.png",
                            "modulo-2/infografico-7.jpg",
                            "modulo-2/infografico-tostado-molido-3.png",
                            "modulo-2/infografico-tostado-molido-4.png",
                            "modulo-2/infografico-tostado-molido-5.png",
                            "modulo-2/infografico-8.jpg",
                            "modulo-2/infografico-5.png"
                        ]
                    },
                    "5": {
                        "index": "1.6",
                        "name": "Proceso del café soluble o instantáneo",
                        "image": "modulo2-5.png",
                        "time": "03:30",
                        "type": "3",
                        "content": [
                            "modulo-2/infografico-2-1.png",
                            "modulo-2/infografico-6.png",
                            "modulo-2/infografico-tostado-molido-6.jpg",
                            "modulo-2/infografico-ind-soluble-3.png",
                            "modulo-2/infografico-ind-soluble-4.png",
                            "modulo-2/infografico-ind-soluble-5.png",
                            "modulo-2/infografico-ind-soluble-6.png",
                            "modulo-2/infografico-ind-soluble-7.png",
                            "modulo-2/infografico-2-5-1.jpg",
                            "modulo-2/infografico-2-6.png"
                        ]
                    },
                    // "6": {
                    //     "index": "1.7",
                    //     "name": "Nuestros productos",
                    //     "image": "modulo2-4.png",
                    //     "time": "03:00",
                    //     "type": "3",
                    //     "content": [
                    //         "mac/mapa-sitio-intro.png",
                    //         "mac/mapa-sitio-1.jpg",
                    //         "mac/mac.jpeg",
                    //         "mac/mapa-sitio-4.jpg"
                    //     ]
                    // },
                    "6": {
                        "index": "1.7",
                        "name": "Nuestros productos",
                        "image": "modulo2-4.png",
                        "time": "03:00",
                        "type": "11",
                        "content": [
                                    "mac/mapa-sitio-intro.png",
                                    "mac/mapa-sitio-1.jpg",
                                    "mac/mac.jpeg",
                                    "mac/mapa-sitio-4.jpg"
                                ]
                    },
                    "7": {
                        "index": "1.8",
                        "name": "Claves para la preparación I",
                        "image": "modulo2-4.png",
                        "time": "01:00",
                        "type": "12",
                        "content": [
                            "ResumenCafe-intro.png",
                            "ResumenCafe-1.png",
                            "ResumenCafe-2.png",
                            "ResumenCafe-3.png",
                            "ResumenCafe-4.png",
                            "ResumenCafe-5.png",
                            "ResumenCafe-6.png",
                           /*  "ResumenCafe-7.png", */
                           /*  "mac/mac.jpeg", */
                            "ResumenCafe-8.png",
                        ]
                    },
                    "8": {
                        "index": "1.9",
                        "name": "Claves para la preparación II",
                        "image": "modulo2-6.png",
                        "time": "03:17",
                        "type": "1",
                        "content": "tips-preparar-un-buen-cafe.mp4"
                    },
                    "9": {
                        "index": "1.10",
                        "name": "Prueba tu conocimiento",
                        "titlemodulo": "Ejercicio de conocimiento de Café y Cultura",
                        "image": "modulo1-3.png",
                        "time": "11:00",
                        "type": "2",
                        "content": {
                            "type": "a",
                            "intro" : true,
                            "template" : "intro-1",
                            "questions": {
                                "0": {
                                    "numberquestions": "1",
                                    "description": "Se dice que el café se descubrió hace más de 1.300 años en:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-1/thumbnail-1-1.jpg",
                                            "infoquestions": "a. El eje cafetero en Colombia.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-1/thumbnail-1-2.jpg",
                                            "infoquestions": "b. El occidente de Brasil.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "2": {
                                            "description": "modulo-1/thumbnail-1-3.jpg",
                                            "infoquestions": "c. La ciudad de Kaffa en Etiopía (África).",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "3": {
                                            "description": "modulo-1/thumbnail-1-4.jpg",
                                            "infoquestions": "d. Los bares de café en Italia.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "1": {
                                    "numberquestions": "2",
                                    "description": "Existen diferentes tipos de arbustos de café, de los cuales las 2 especies más comerciales a nivel mundial son:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-1/thumbnail-2-1.jpg",
                                            "infoquestions": "a. Caturra y Bourbón.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-1/thumbnail-2-2.jpg",
                                            "infoquestions": "b. Arábica y Robusta.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "2": {
                                            "description": "modulo-1/thumbnail-2-3.jpg",
                                            "infoquestions": "c. Brasilera y Colombiana.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-1/thumbnail-2-4.jpg",
                                            "infoquestions": "d. Arabusta y Robica.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "2": {
                                    "numberquestions": "3",
                                    "description": "El café llega a Colombia:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-1/thumbnail-3-1.jpg",
                                            "infoquestions": "a. En el año 1.732, por los Santanderes, Orinoco y Costa Norte.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "1": {
                                            "description": "modulo-1/thumbnail-3-2.jpg",
                                            "infoquestions": "b. En el año 1.600, por el Amazonas.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "2": {
                                            "description": "modulo-1/thumbnail-3-3.jpg",
                                            "infoquestions": "c. En el año 1.900, por la Costa Pacífica.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-1/thumbnail-3-4.jpg",
                                            "infoquestions": "d. En el año 1.850, por Panamá.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "3": {
                                    "numberquestions": "4",
                                    "description": "El café que se produce en Colombia, pertenece a la especie:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-1/thumbnail-4-1.jpg",
                                            "infoquestions": "a. Robusta.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-1/thumbnail-4-2.jpg",
                                            "infoquestions": "b. Arábica.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "2": {
                                            "description": "modulo-1/thumbnail-4-3.jpg",
                                            "infoquestions": "c. 50% Arábica y 50% Robusta.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-1/thumbnail-4-4.jpg",
                                            "infoquestions": "d. 80% Arábica y 20% Robusta.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "4": {
                                    "numberquestions": "5",
                                    "description": "¿Cuántas familias de caficultores colombianos están detrás de una deliciosa taza de café?",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-1/thumbnail-5-1.jpg",
                                            "infoquestions": "a. 340.000",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-1/thumbnail-5-2.jpg",
                                            "infoquestions": "b. 1´000.000",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "2": {
                                            "description": "modulo-1/thumbnail-5-3.jpg",
                                            "infoquestions": "c. 250.000",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-1/thumbnail-5-4.jpg",
                                            "infoquestions": "d. Más de 500.000",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        }
                                    }
                                },
                                "5": {
                                    "numberquestions": "6",
                                    "description": "Uno de los secretos del mejor café suave del mundo, se basa en la recolección de granos: ",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-1/thumbnail-6-1.jpg",
                                            "infoquestions": "a. Pintones + maduros.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-1/thumbnail-6-2.jpg",
                                            "infoquestions": "b. Solo maduros.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "2": {
                                            "description": "modulo-1/thumbnail-6-3.jpg",
                                            "infoquestions": "c. Verdes + Maduros.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-1/thumbnail-6-4.jpg",
                                            "infoquestions": "d. Pintones + verdes.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "6": {
                                    "numberquestions": "7",
                                    "description": "El orden correcto del proceso detrás de una taza de café es: ",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-1/thumbnail-7-1.jpg",
                                            "infoquestions": "a. Cultivo, cosecha, beneficio, trilla, tostión, preparación.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "1": {
                                            "description": "modulo-1/thumbnail-7-2.jpg",
                                            "infoquestions": "b. Cultivo, cosecha, beneficio, tostión, trilla, preparación.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "2": {
                                            "description": "modulo-1/thumbnail-7-3.jpg",
                                            "infoquestions": "c. Cultivo, cosecha, trilla, beneficio, tostión, preparación.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-1/thumbnail-7-4.jpg",
                                            "infoquestions": "d. Cultivo, cosecha, trilla, tostión, beneficio, preparación.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "7": {
                                    "numberquestions": "8",
                                    "description": "Para preparar un delicioso café, es necesario tener en cuenta:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-1/thumbnail-8-1.jpg",
                                            "infoquestions": "a.Utilizar un café vencido, agua de color amarillo, una cafetera  oxidada y agregarle mucha agua para que quede clarito.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-1/thumbnail-8-2.jpg",
                                            "infoquestions": "b. Café fresco y bien conservado, cantidades de café y agua recomendadas, cafeteras e implementos limpios y preparar únicamente el que se va a  consumir de inmediato o  máximo en una hora. ",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "2": {
                                            "description": "modulo-1/thumbnail-8-3.jpg",
                                            "infoquestions": "c. Mantener el paquete abierto y preparar café para todo el día.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-1/thumbnail-8-4.jpg",
                                            "infoquestions": "d. Aplicarse perfume o loción, no lavarse las manos y recalentar el café.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "8": {
                                    "numberquestions": "9",
                                    "description": "Las variables clave para la preparación, forman la palabra CAFE, cuyo significado es: ",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-1/thumbnail-9-1.jpg",
                                            "infoquestions": "a. Cereza, Almendra Familias y El proceso de tostión.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-1/thumbnail-9-2.jpg",
                                            "infoquestions": "b. Colino, Almácigo, Fruto y El proceso de beneficio.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "2": {
                                            "description": "modulo-1/thumbnail-9-3.jpg",
                                            "infoquestions": "c. Café, Agua, Funcionamiento del equipo y Elaboración del café.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "3": {
                                            "description": "modulo-1/thumbnail-9-4.jpg",
                                            "infoquestions": "d. Colombia, Arábica, Fermentación y Exportaciones.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "9": {
                                    "numberquestions": "10",
                                    "description": "Para obtener un café aromático y de sabor intenso, la medida recomendada  para un pocillo tintero, si se prepara con café tostado y molido es: ",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-1/thumbnail-10-1.jpg",
                                            "infoquestions": "a. 1 cucharada sopera rasa (5 gramos).",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "1": {
                                            "description": "modulo-1/thumbnail-10-2.jpg",
                                            "infoquestions": "b. ½ cucharada sopera (3 gramos).",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "2": {
                                            "description": "modulo-1/thumbnail-10-3.jpg",
                                            "infoquestions": "c. 2 cucharadas soperas (12 gramos).",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-1/thumbnail-10-4.jpg",
                                            "infoquestions": "d. 1 cucharadita mediana  (3 gramos).",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "10": {
                                    "numberquestions": "11",
                                    "description": "Para obtener un café aromático y de sabor intenso, la medida recomendada para un pocillo tintero, si se prepara con café instantáneo es:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-1/thumbnail-11-1.jpg",
                                            "infoquestions": "a. 1 cucharadita pequeña colmada (1.5 gramos).",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "1": {
                                            "description": "modulo-1/thumbnail-11-2.jpg",
                                            "infoquestions": "b. ½ cucharada sopera (3 gramos).",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "2": {
                                            "description": "modulo-1/thumbnail-11-3.jpg",
                                            "infoquestions": "c. 2 cucharadas soperas (12 gramos).",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-1/thumbnail-11-4.jpg",
                                            "infoquestions": "d. 1 cucharadita mediana  (3 gramos).",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            {
                // MODULO 2
                "index": "1",
                "name": "Café Filtrado Institucional",
                "unique": "filtrado",
                "time": "2h",
                "content": {
                    "0": {
                        "index": "2.1",
                        "name": "Introducción Café Filtrado Institucional",
                        "image": "modulo1-1.png",
                        "time": "00:29",
                        "type": "1",
                        "content": "intro_cafe_filtrado_institucional.mp4"
                    },
                    "1": {
                        "index": "2.2",
                        "name": "Definición",
                        "image": "modulo2-4.png",
                        "time": "02:00",
                        "type": "3",
                        "content": [
                            "recuerda/cafe-filtrado-institucional-1.png",
                            "recuerda/cafe-filtrado-institucional-2.png",
                        ]
                    },
                    "2": {
                        "index": "2.3",
                        "name": "Recuerda que...",
                        "image": "modulo2-4.png",
                        "time": "02:00",
                        "type": "3",
                        "content": [
                            "recuerda/cafe-filtrado-institucional-3.png",
                            "recuerda/cafe-filtrado-institucional-4.png",
                            "recuerda/cafe-filtrado-institucional-5.png",
                            "recuerda/cafe-filtrado-institucional-6.png",
                            "recuerda/cafe-filtrado-institucional-7.png",
                            "recuerda/cafe-filtrado-institucional-8.png",
                            "recuerda/cafe-filtrado-institucional-9.png",
                            "recuerda/cafe-filtrado-institucional-10.png",
                            "recuerda/cafe-filtrado-institucional-11.png",
                        ]
                    },
                    "3": {
                        "index": "2.4",
                        "name": "Preparaciones",
                        "image": "modulo4-1.png",
                        "time": "07:50",
                        "type": "4",
                        "content": {
                            "title": "Café Filtrado Institucional",
                            "thumbnail": "modulo-1/thumbnail-video-0.jpg",
                            "playlist": {
                                "0": {
                                    "title": "Greca",
                                    "branch": "nuestras-marcas-cafe-la-bastilla-small.png",
                                    "thumbnail": "video-filtrado-inst-1.jpg",
                                    "video": "greca.mp4"
                                },
                                "1": {
                                    "title": "Cafetera Institucional por Goteo",
                                    "branch": "nuestras-marcas-cafe-sello-rojo-small.png",
                                    "thumbnail": "video-filtrado-inst-2.jpg",
                                    "video": "cafetera-institucional-por-goteo.mp4"
                                }
                            }
                        }
                    },
                    "4": {
                        "index": "2.5",
                        "name": "Recetas",
                        "image": "modulo4-2.png",
                        "time": "02:05",
                        "type": "4",
                        "content": {
                            "title": "Café Filtrado Institucional",
                            "thumbnail": "modulo-1/thumbnail-video-0.jpg",
                            "playlist": {
                                "0": {
                                    "title": "Café Campesino",
                                    "branch": "nuestras-marcas-cafe-la-bastilla-small.png",
                                    "thumbnail": "receta-video-filtrado-inst-1.jpg",
                                    "video": "cafe-campesino.mp4"
                                },
                                "1": {
                                    "title": "Café Carajillo",
                                    "branch": "nuestras-marcas-cafe-sello-rojo-small.png",
                                    "thumbnail": "receta-video-filtrado-inst-2.jpg",
                                    "video": "cafe-carajillo.mp4"
                                },
                                "2": {
                                    "title": "Malteada con Café Filtrado",
                                    "branch": "nuestras-marcas-cafe-sello-rojo-small.png",
                                    "thumbnail": "receta-video-filtrado-inst-3.jpg",
                                    "video": "malteada-con-cafe-filtrado.mp4"
                                },
                                "3": {
                                    "title": "Café Refrescante",
                                    "branch": "nuestras-marcas-cafe-la-bastilla-small.png",
                                    "thumbnail": "receta-video-filtrado-inst-4.jpg",
                                    "video": "cafe-refrescante.mp4"
                                }
                            }
                        }
                    },
                    "5": {
                        "index": "2.6",
                        "name": "Prueba tu conocimiento",
                        "titlemodulo": "Ejercicio de conocimiento Café Filtrado Institucional",
                        "image": "modulo4-3.png",
                        "time": "7:00",
                        "type": "2",
                        "content": {
                            "type": "a",
                            "intro" : true,
                            "template" : "intro-2",
                            "questions": {
                                "0": {
                                    "numberquestions": "1",
                                    "description": "Para preparar café filtrado se debe utilizar:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-3/thumbnail-3-1.jpg",
                                            "infoquestions": "a. Café tostado y molido.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "1": {
                                            "description": "modulo-3/thumbnail-1-2.jpg",
                                            "infoquestions": "b. Café instantáneo. ",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "2": {
                                            "description": "modulo-3/thumbnail-1-3.jpg",
                                            "infoquestions": "c. Café tostado en grano y sin moler.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-3/thumbnail-1-4.jpg",
                                            "infoquestions": "d. Licor de café.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "1": {
                                    "numberquestions": "2",
                                    "description": "Las marcas de café recomendadas para preparar café filtrado, son:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-3/thumbnail-2-1.jpg",
                                            "infoquestions": "a. Colcafé.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-3/thumbnail-2-2.jpg",
                                            "infoquestions": "b. Sello Rojo, La Bastilla y Matiz.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "2": {
                                            "description": "modulo-3/thumbnail-2-3.jpg",
                                            "infoquestions": "c. Coffee Crem.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-3/thumbnail-2-4.jpg",
                                            "infoquestions": "d. Colcafé Cappuccino.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "2": {
                                    "numberquestions": "3",
                                    "description": "Para obtener un café fuerte se debe: ",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-3/thumbnail-3-1.jpg",
                                            "infoquestions": "a. Agregar mayor  cantidad de café  que la recomendada,  para que quede cargado.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-3/thumbnail-3-2.jpg",
                                            "infoquestions": "b. Seleccionar un café con tostión oscura.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "2": {
                                            "description": "modulo-3/thumbnail-3-3.jpg",
                                            "infoquestions": "c. Recalentar el café.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-3/thumbnail-3-4.jpg",
                                            "infoquestions": "d. Moler muy fino el café.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "3": {
                                    "numberquestions": "4",
                                    "description": "Para obtener un café suave se debe:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-3/thumbnail-4-1.jpg",
                                            "infoquestions": "a. Agregar menor cantidad de café   que la recomendada.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-3/thumbnail-4-2.jpg",
                                            "infoquestions": "b. Adicionar más agua para que quede clarito.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "2": {
                                            "description": "modulo-3/thumbnail-4-3.jpg",
                                            "infoquestions": "c. Seleccionar un café con tostión clara.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "3": {
                                            "description": "modulo-3/thumbnail-4-4.jpg",
                                            "infoquestions": "d. Agregar  mucho azúcar.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "4": {
                                    "numberquestions": "5",
                                    "description": "Para obtener un café de sabor balanceado (ni muy suave, ni muy fuerte) se debe:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-3/thumbnail-5-1.jpg",
                                            "infoquestions": "a. Servir la mitad del café que usualmente se consume.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-3/thumbnail-5-2.jpg",
                                            "infoquestions": "b. Adicionar más agua para que quede clarito.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "2": {
                                            "description": "modulo-3/thumbnail-5-3.jpg",
                                            "infoquestions": "c. Agregar media cuchara de azúcar.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-3/thumbnail-5-4.jpg",
                                            "infoquestions": "d. Seleccionar un café con tostión media.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        }
                                    }
                                },
                                "5": {
                                    "numberquestions": "6",
                                    "description": "La medida recomendada para preparar 10 bebidas de un delicioso café filtrado es:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-3/thumbnail-66-1.jpg",
                                            "infoquestions": "a. Un litro de agua y 50 gramos de café.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-3/thumbnail-66-2.jpg",
                                            "infoquestions": "b. Un litro de agua y 4 onzas de café.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "2": {
                                            "description": "modulo-3/thumbnail-66-3.jpg",
                                            "infoquestions": "c. La respuesta es a y b.",
                                             "correct": true,
                                            "feedback": "Correcta"

                                        },
                                        "3": {
                                            "description": "modulo-3/thumbnail-66-4.jpg",
                                            "infoquestions": "d. Un litro de agua y tres cucharadas de café.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "6": {
                                    "numberquestions": "7",
                                    "description": "Para conservar el aroma y el sabor, después de abrir el paquete de café, se debe:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-3/thumbnail-7-1.jpg",
                                            "infoquestions": "a. Dejarlo abierto para que ingrese mucho oxígeno.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-3/thumbnail-7-2.jpg",
                                            "infoquestions": "b. Guardarlo en un recipiente bien tapado y preferiblemente refrigerado, para que no se oxide.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "2": {
                                            "description": "modulo-3/thumbnail-7-3.jpg",
                                            "infoquestions": "c. Doblar el paquete y cerrarlo con la pinza de la ropa.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-3/thumbnail-7-4.jpg",
                                            "infoquestions": "d. Dejarlo abierto, al lado los jabones  y elementos de aseo.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "7": {
                                    "numberquestions": "8",
                                    "description": "El agua para la preparación de café debe:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-2/thumbnail-8-1.jpg",
                                            "infoquestions": "a. Estar tibia.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-2/thumbnail-8-2.jpg",
                                            "infoquestions": "b. Ser pura, potable y estar a punto de ebullición (primer hervor).",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "2": {
                                            "description": "modulo-2/thumbnail-8-3.jpg",
                                            "infoquestions": "c. Hervir durante 20 minutos.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-2/thumbnail-8-4.jpg",
                                            "infoquestions": "d. Tener olor  a cloro.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "8": {
                                    "numberquestions": "9",
                                    "description": "El café filtrado se puede preparar en los siguientes equipos:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-2/thumbnail-99-1.jpg",
                                            "infoquestions": "a. Greca.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-2/thumbnail-99-2.jpg",
                                            "infoquestions": "b. Cafetera Institucional por goteo.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "2": {
                                            "description": "modulo-2/thumbnail-99-3.jpg",
                                            "infoquestions": "c. Olla con filtro.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-2/thumbnail-9-4.jpg",
                                            "infoquestions": "d. Todas las anteriores.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        }
                                    }
                                },
                                "9": {
                                    "numberquestions": "10",
                                    "description": "Después de preparado el café, se debe:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-2/thumbnail-10-1.jpg",
                                            "infoquestions": "a. Recalentar.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-2/thumbnail-10-2.jpg",
                                            "infoquestions": "b. Consumir inmediatamente, o almacenar en un termo, por una hora máximo.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "2": {
                                            "description": "modulo-2/thumbnail-10-3.jpg",
                                            "infoquestions": "c. Dejar enfriar y recalentar.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-2/thumbnail-10-4.jpg",
                                            "infoquestions": "d. Adicionar más agua para que quede clarito.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            {
                // MODULO 3
                "index": "2",
                "name": "Café Espresso Institucional",
                "unique": "espresso-institucional",
                "time": "4h",
                "content": {
                    "0": {
                        "index": "3.1",
                        "name": "Introducción Café Espresso Institucional",
                        "image": "modulo1-1.png",
                        "time": "00:34",
                        "type": "1",
                        "content": "intro_espresso_Institucional.mp4"
                    },
                    "1": {
                        "index": "3.2",
                        "name": "Definición",
                        "image": "modulo2-4.png",
                        "time": "02:00",
                        "type": "3",
                        "content": [
                            "definicion/cafe-espresso-1.png",
                            "definicion/cafe-espresso-2.png"
                        ]
                    },
                    "2": {
                        "index": "3.3",
                        "name": "Las 4M's del espresso",
                        "image": "modulo2-4.png",
                        "time": "02:00",
                        "type": "3",
                        "content": [
                            "recuerda/cafe-espresso-1.png",
                            "recuerda/cafe-espresso-2.png",
                            "recuerda/cafe-espresso-3.png",
                            "recuerda/cafe-espresso-4.png",
                            "recuerda/cafe-espresso-5.png",
                            //"recuerda/cafe-espresso-6.png",
                        ]
                    },
                    "3": {
                        "index": "3.4",
                        "name": "Preparaciones",
                        "image": "modulo6-1.png",
                        "time": "04:45",
                        "type": "4",
                        "content": {
                            "title": "Café Espresso Institucional",
                            "thumbnail": "modulo-1/thumbnail-video-0.jpg",
                            "playlist": {
                                "0": {
                                    "title": "Máquina Espresso",
                                    "branch": "nuestras-marcas-matiz-small.png",
                                    "thumbnail": "video-espresso-inst-1.jpg",
                                    "video": "espresso-Institucional.mp4"
                                },
                                "1": {
                                    "title": "Preparación de Cappuccino",
                                    "branch": "nuestras-marcas-matiz-small.png",
                                    "thumbnail": "video-espresso-inst-2.jpg",
                                    "video": "preparacion-de-un-capuccino.mp4"
                                },
                                "2": {
                                    "title": "Preparación de un Café Americano",
                                    "branch": "nuestras-marcas-cafe-sello-rojo-small.png",
                                    "thumbnail": "video-espresso-inst-3.jpg",
                                    "video": "americano.mp4"
                                }
                            }
                        }
                    },
                    "4": {
                        "index": "3.5",
                        "name": "Recetas",
                        "image": "modulo6-2.png",
                        "time": "02:20",
                        "type": "4",
                        "content": {
                            "title": "Café Espresso Institucional",
                            "thumbnail": "modulo-1/thumbnail-video-0.jpg",
                            "playlist": {
                                "0": {
                                    "title": "Cappuccino Amaretto",
                                    "branch": "nuestras-marcas-cafe-sello-rojo-small.png",
                                    "thumbnail": "receta-video-espresso-inst-1.jpg",
                                    "video": "cappuccino-amaretto.mp4"
                                },
                                "1": {
                                    "title": "Espresso Macchiato",
                                    "branch": "nuestras-marcas-matiz-small.png",
                                    "thumbnail": "receta-video-espresso-inst-2.jpg",
                                    "video": "espresso-machiato.mp4"
                                },
                                "2": {
                                    "title": "Affogato",
                                    "branch": "nuestras-marcas-cafe-sello-rojo-small.png",
                                    "thumbnail": "receta-video-espresso-inst-3.jpg",
                                    "video": "affogato.mp4"
                                },
                                "3": {
                                    "title": "Café Irlandés",
                                    "branch": "nuestras-marcas-matiz-small.png",
                                    "thumbnail": "receta-video-espresso-inst-4.jpg",
                                    "video": "cafe-irlandes.mp4"
                                }
                            }
                        }
                    },
                    "5": {
                        "index": "3.6",
                        "name": "Prueba tu conocimiento",
                        "titlemodulo": "Ejercicio de conocimiento Café Espresso Institucional",
                        "image": "modulo6-3.png",
                        "time": "2:40",
                        "type": "2",
                        "content": {
                            "type": "b",
                            "intro" : true,
                            "template" : "intro-8",
                            "image" : "modulo-1/title-colums-1.png",
                            "questions": {
                                "0" : {
                                    "a" : [
                                        "1. Las 4 M’s del espresso",
                                        "2. Presión de la caldera de la máquina",
                                        "3. Presión de la bomba",
                                        "4. Portafiltros doble",
                                        "5. Tipo de café para espresso",
                                        "6. Marcas para café espresso",
                                        "7. Relación café – agua",
                                        "8. Mano de barista",
                                        "9. El tiempo de extracción de un espresso"
                                    ],
                                    "b" : [
                                        {
                                            "match" : "8",
                                            "answer" : "Debe asegurar que el café se compacta de forma uniforme"
                                        },
                                        {
                                            "match" : "2",
                                            "answer" : "Controla la temperatura del agua y es de 1 bar"
                                        },
                                        {
                                            "match" : "6",
                                            "answer" : "Matiz en grano y/o Sello Rojo Espresso"
                                        },
                                        {
                                            "match" : "1",
                                            "answer" : "Máquina espresso, Mezcla de Café, Molino y Molienda, Mano del Barista"
                                        },
                                        {
                                            "match" : "9",
                                            "answer" : "20 a 30 segundos"
                                        },
                                        {
                                            "match" : "7",
                                            "answer" : "7 a 11 gramos de café y 30 mililitros de agua"
                                        },
                                        {
                                            "match" : "3",
                                            "answer" : "Controla la velocidad de salida del agua para la extracción. Está entre 9 y 10 bares"
                                        },
                                        {
                                            "match" : "4",
                                            "answer" : "Se usa cuando se van a preparar simultáneamente 2 espressos ó 1 espresso doble"
                                        },
                                        {
                                            "match" : "5",
                                            "answer" : "En grano, tostión oscura y molienda fina justa"
                                        }
                                    ]
                                },
                                "1" : {
                                    "a" : [
                                        "1. Vaporización",
                                        "2. Características de la leche",
                                    ],
                                    "b" : [
                                        {
                                            "match" : "2",
                                            "answer" : "Fresca y fría"
                                        },
                                        {
                                            "match" : "1",
                                            "answer" : "Proceso mediante el cual se da textura cremosa a la leche, a través de vapor de agua a presión"
                                        }
                                    ]
                                }
                            }
                        }
                    }
                }
            },
            {
                // MODULO 4
                "index": "3",
                "name": "Café Instantáneo",
                "unique": "cafe-instantaneo",
                "time": "4h",
                "content": {
                    "0": {
                        "index": "4.1",
                        "name": "Introducción Café Instantáneo",
                        "image": "modulo1-1.png",
                        "time": "00:26",
                        "type": "1",
                        "content": "intro_cafe_instantaneo.mp4"
                    },
                    "1": {
                        "index": "4.2",
                        "name": "Definición",
                        "image": "modulo2-4.png",
                        "time": "02:00",
                        "type": "3",
                        "content": [
                            "definicion/cafe-instantaneo-1.png",
                            "definicion/cafe-instantaneo-2.png"
                        ]
                    },
                    "2": {
                        "index": "4.3",
                        "name": "Recuerda que...",
                        "image": "modulo2-4.png",
                        "time": "02:00",
                        "type": "3",
                        "content": [
                            "recuerda/cafe-instantaneo-1.png",
                            "recuerda/cafe-instantaneo-2.png",
                            "recuerda/cafe-instantaneo-3.png",
                            "recuerda/cafe-instantaneo-4.png",
                            "recuerda/cafe-instantaneo-5.png",
                            "recuerda/cafe-instantaneo-6.png",
                            "recuerda/cafe-instantaneo-7.png",
                            "recuerda/cafe-instantaneo-8.png",
                        ]
                    },
                    "3": {
                        "index": "4.4",
                        "name": "Preparaciones",
                        "image": "modulo7-1.png",
                        "time": "03:16",
                        "type": "4",
                        "content": {
                            "title": "Café Instantáneo",
                            "thumbnail": "modulo-1/thumbnail-video-0.jpg",
                            "playlist": {
                                "0": {
                                    "title": "Café Negro",
                                    "branch": "nuestras-marcas-colcafe-te-consiente-small.png",
                                    "thumbnail": "video-colcafe-1.jpg",
                                    "video": "colcafe-clasico.mp4"
                                },
                                "1": {
                                    "title": "Café con Leche",
                                    "branch": "nuestras-marcas-colcafe-te-consiente-small.png",
                                    "thumbnail": "video-colcafe-2.jpg",
                                    "video": "colcafe-Granulado.mp4"
                                }
                            }
                        }
                    },
                    "4": {
                        "index": "4.5",
                        "name": "Recetas",
                        "image": "modulo7-2.png",
                        "time": "02:20",
                        "type": "4",
                        "content": {
                            "title": "Café Instantáneo",
                            "thumbnail": "modulo-1/thumbnail-video-0.jpg",
                            "playlist": {
                                "0": {
                                    "title": "Café Mocca",
                                    "branch": "nuestras-marcas-colcafe-te-consiente-small.png",
                                    "thumbnail": "receta-video-instantaneo-1.jpg",
                                    "video": "cafe-mocca.mp4"
                                },
                                "1": {
                                    "title": "Café Tentación",
                                    "branch": "nuestras-marcas-colcafe-te-consiente-small.png",
                                    "thumbnail": "receta-video-instantaneo-2.jpg",
                                    "video": "cafe-tentacion.mp4"
                                },
                                "2": {
                                    "title": "Granizado de Café",
                                    "branch": "nuestras-marcas-colcafe-te-consiente-small.png",
                                    "thumbnail": "receta-video-instantaneo-3.jpg",
                                    "video": "granizado-de-cafe.mp4"
                                },
                                "3": {
                                    "title": "Malteada Mocca",
                                    "branch": "nuestras-marcas-colcafe-te-consiente-small.png",
                                    "thumbnail": "receta-video-instantaneo-4.jpg",
                                    "video": "malteada-de-cafe-mocca.mp4"
                                },
                                "4": {
                                    "title": "Postre de Café Limón",
                                    "branch": "nuestras-marcas-colcafe-te-consiente-small.png",
                                    "thumbnail": "receta-video-instantaneo-5.jpg",
                                    "video": "postre-de-limon.mp4"
                                },
                                "5": {
                                    "title": "Arroz Dulce de Café",
                                    "branch": "nuestras-marcas-colcafe-te-consiente-small.png",
                                    "thumbnail": "receta-video-instantaneo-6.jpg",
                                    "video": "arroz-con-leche-cafe.mp4"
                                }
                            }
                        }
                    },
                    "5": {
                        "index": "4.6",
                        "name": "Prueba tu conocimiento",
                        "titlemodulo": "Ejercicio de conocimiento Café Instantáneo",
                        "image": "modulo7-3.png",
                        "time": "06:00",
                        "type": "2",
                        "content": {
                            "type": "a",
                            "intro" : true,
                            "template" : "intro-6",
                            "questions": {
                                "0": {
                                    "numberquestions": "1",
                                    "description": "Según el proceso industrial, el café instantáneo se puede clasificar en:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-6/thumbnail-1-1.jpg",
                                            "infoquestions": "a. Tostado claro,  medio, oscuro.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-6/thumbnail-1-2.jpg",
                                            "infoquestions": "b. Polvo, granulado, liofilizado.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "2": {
                                            "description": "modulo-6/thumbnail-1-3.jpg",
                                            "infoquestions": "c. Molienda gruesa, media, fina.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-6/thumbnail-1-4.jpg",
                                            "infoquestions": "d. Ninguna de las anteriores.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "1": {
                                    "numberquestions": "2",
                                    "description": "Para cada pocillo tintero (100 mililitros), la cantidad para preparar un café instantáneo es:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-6/thumbnail-2-1.jpg",
                                            "infoquestions": "a. 1 cucharadita pequeña de café (1.5 gramos).",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "1": {
                                            "description": "modulo-6/thumbnail-2-2.jpg",
                                            "infoquestions": "b. 1 cucharada sopera rasa de café (5 gramos).",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "2": {
                                            "description": "modulo-6/thumbnail-2-3.jpg",
                                            "infoquestions": "c. 2 cucharaditas pequeñas de café (3 gramos).",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-6/thumbnail-2-4.jpg",
                                            "infoquestions": "d. ½ cucharada sopera de café (3 gramos).",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "2": {
                                    "numberquestions": "3",
                                    "description": "Para preparar café instantáneo se debe:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-6/thumbnail-3-1.jpg",
                                            "infoquestions": "a. Adicionar primero el agua y luego el café.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-6/thumbnail-3-2.jpg",
                                            "infoquestions": "b. Agregar primero el café y luego el agua.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "2": {
                                            "description": "modulo-6/thumbnail-3-3.jpg",
                                            "infoquestions": "c. Agregar el agua, después el café y luego filtrar.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-6/thumbnail-3-4.jpg",
                                            "infoquestions": "d. Ninguna de las anteriores.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "3": {
                                    "numberquestions": "4",
                                    "description": "Las marcas de café recomendadas para preparar café instantáneo, son: ",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-6/thumbnail-4-1.jpg",
                                            "infoquestions": "a. Sello Rojo Descafeinado. ",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-6/thumbnail-4-2.jpg",
                                            "infoquestions": "b. Sello Rojo, La Bastilla y Matiz.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "2": {
                                            "description": "modulo-6/thumbnail-4-3.jpg",
                                            "infoquestions": "c. Colcafé.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "3": {
                                            "description": "modulo-6/thumbnail-4-4.jpg",
                                            "infoquestions": "d. Todas las anteriores.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "4": {
                                    "numberquestions": "5",
                                    "description": "Para una celebración especial en la que se quiere preparar una receta a base de café, en la cual los invitados son sensibles a la cafeína, ¿cuál de los siguientes productos de Colcafé recomendarías:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-6/thumbnail-6-1.jpg",
                                            "infoquestions": "a. Colcafé Clásico.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-6/thumbnail-6-2.jpg",
                                            "infoquestions": "b. Colcafé Descafeinado.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "2": {
                                            "description": "modulo-6/thumbnail-6-3.jpg",
                                            "infoquestions": "c. Colcafé Vainilla.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-6/thumbnail-6-4.jpg",
                                            "infoquestions": "d. Colcafé Neutralizado.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            {
                // MODULO MEZCLAS INSTANTANEAS
                "index": "4",
                "name": "Mezclas Instantáneas",
                "unique": "mezclas-instantaneas",
                "time": "4h",
                "content": {
                    "0": {
                        "index": "5.1",
                        "name": "Introducción Mezclas Instantáneas",
                        "image": "modulo1-1.png",
                        "time": "00:33",
                        "type": "1",
                        "content": "intro_mezclas_instantaneas.mp4"
                    },
                    "1": {
                        "index": "5.2",
                        "name": "Definición",
                        "image": "modulomezclas-1.jpg",
                        "time": "01:00",
                        "type": "3",
                        "content": [
                          "modulo-mezclas/infografico-1.png",
                          "modulo-mezclas/infografico-2.png"
                        ]
                    },
                    "2": {
                        "index": "5.3",
                        "name": "Recuerda que...",
                        "image": "modulomezclas-1.jpg",
                        "time": "01:00",
                        "type": "3",
                        "content": [
                          "modulo-mezclas/infografico-3.png",
                          "modulo-mezclas/infografico-4.png"
                        ]
                    },
                    "3": {
                        "index": "5.4",
                        "name": "Preparaciones",
                        "image": "modulo3-1.png",
                        "time": "10:55",
                        "type": "4",
                        "content": {
                            "title": "Mezclas Instantáneas",
                            "thumbnail": "modulo-1/thumbnail-video-0.jpg",
                            "playlist": {
                                "0": {
                                    "title": "Colcafé 3 en 1",
                                    "branch": "nuestras-marcas-colcafe-te-consiente-small.png",
                                    "thumbnail": "modulo-mezclas/preparaciones-colcafe-3-en-1.png",
                                    "video": "2colcafe-3-en-1.mp4"
                                },
                                "1": {
                                    "title": "Capuccino Caliente",
                                    "branch": "nuestras-marcas-colcafe-te-consiente-small.png",
                                    "thumbnail": "modulo-mezclas/preparaciones-cappuccino-caliente.png",
                                    "video": "capuccino-caliente.mp4"
                                },
                                "2": {
                                    "title": "Capuccino Frío",
                                    "branch": "nuestras-marcas-colcafe-te-consiente-small.png",
                                    "thumbnail": "modulo-mezclas/preparaciones-cappuccino-frio.png",
                                    "video": "capuccino-frio.mp4"
                                },
                                "3": {
                                    "title": "Granizado en Máquina",
                                    "branch": "nuestras-marcas-colcafe-te-consiente-small.png",
                                    "thumbnail": "modulo-mezclas/preparaciones-granizado-en-maquina.png",
                                    "video": "granizado-maquina.mp4"
                                },
                                "4": {
                                    "title": "Cappucino en agua",
                                    "branch": "nuestras-marcas-colcafe-te-consiente-small.png",
                                    "thumbnail": "cappucino-en-clasico.png",
                                    "video": "cappucino-clasico.mp4"
                                },
                            }
                        }
                    },
                    "4": {
                        "index": "5.5",
                        "name": "Recetas",
                        "image": "modulo3-2.png",
                        "time": "02:55",
                        "type": "4",
                        "content": {
                            "title": "Mezclas Instantáneas",
                            "thumbnail": "modulo-1/thumbnail-video-0.jpg",
                            "playlist": {
                                "0": {
                                    "title": "Café Tentación",
                                    "branch": "nuestras-marcas-colcafe-te-consiente-small.png",
                                    "thumbnail": "modulo-mezclas/recetas-capuccino-con-amaretto.png",
                                    "video": "capuccino-con-amaretto.mp4"
                                },
                                "1": {
                                    "title": "Malteada Capuccino",
                                    "branch": "nuestras-marcas-colcafe-te-consiente-small.png",
                                    "thumbnail": "modulo-mezclas/recetas-malteada-capuccino.png",
                                    "video": "malteada-capucciono.mp4"
                                },
                                "2": {
                                    "title": "Nevado de Café y Arándanos",
                                    "branch": "nuestras-marcas-colcafe-te-consiente-small.png",
                                    "thumbnail": "modulo-mezclas/nevado-cafe-arandanos.png",
                                    "video": "nevado-cafe-arandanos.mp4"
                                },
                                "3": {
                                    "title": "Frappuccino de Avellana",
                                    "branch": "nuestras-marcas-colcafe-te-consiente-small.png",
                                    "thumbnail": "modulo-mezclas/frappuccino-avellana.png",
                                    "video": "frappuccino-avellana.mp4"
                                }
                            }
                        }
                    },
                    "5": {
                        "index": "5.6",
                        "name": "Prueba tu conocimiento",
                        "titlemodulo": "Ejercicio de conocimiento Mezclas Instantaneas",
                        "image": "modulomezclas-2.jpg",
                        "time": "10:00",
                        "type": "2",
                        "content": {
                            "type": "a",
                            "intro" : true,
                            "template" : "intro-mezcla-image",
                            "questions": {
                                "0": {
                                    "numberquestions": "1",
                                    "description": "Son mezclas instantáneas del portafolio Colcafé:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-mezclas/thumbnail-1-1.jpg",
                                            "infoquestions": "a. Colcafé Capuccino, Colcafé 3 en 1 y Colcafé Granizado.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "1": {
                                            "description": "modulo-mezclas/thumbnail-1-2.jpg",
                                            "infoquestions": "b. Café Matiz, Café La Bastilla y Café Sello Rojo.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "2": {
                                            "description": "modulo-mezclas/thumbnail-1-3.jpg",
                                            "infoquestions": "c. Café Matiz y Colcafé Granulado.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-mezclas/thumbnail-1-4.jpg",
                                            "infoquestions": "d. Todas las anteriores.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "1": {
                                    "numberquestions": "2",
                                    "description": "Las mezclas instantáneas de café se pueden preparar con:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-mezclas/thumbnail-2-1.jpg",
                                            "infoquestions": "a. Solamente agua.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-mezclas/thumbnail-2-2.jpg",
                                            "infoquestions": "b. Solamente leche.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "2": {
                                            "description": "modulo-mezclas/thumbnail-2-3.jpg",
                                            "infoquestions": "c. Agua o leche.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "3": {
                                            "description": "modulo-mezclas/thumbnail-2-4.jpg",
                                            "infoquestions": "d. Ninguna de las anteriores.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                // "2": {
                                //     "numberquestions": "3",
                                //     "description": "Las mezclas instantáneas del portafolio Colcafé son:",
                                //     "answers": {
                                //         "0": {
                                //             "description": "modulo-mezclas/thumbnail-3-1.jpg",
                                //             "infoquestions": "a. Colcafé Capuccino, Colcafé 3 en 1 y Colcafé Granizado.",
                                //             "correct": true,
                                //             "feedback": "Correcta"
                                //         },
                                //         "1": {
                                //             "description": "modulo-mezclas/thumbnail-3-2.jpg",
                                //             "infoquestions": "b. Café Matiz, Café La Bastilla y Café Sello Rojo.",
                                //             "correct": false,
                                //             "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                //         },
                                //         "2": {
                                //             "description": "modulo-mezclas/thumbnail-3-3.jpg",
                                //             "infoquestions": "c. Café Matiz y Colcafé Granulado.",
                                //             "correct": false,
                                //             "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                //         },
                                //         "3": {
                                //             "description": "modulo-mezclas/thumbnail-3-4.jpg",
                                //             "infoquestions": "d. Todas las anteriores.",
                                //             "correct": false,
                                //             "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                //         }
                                //     }
                                // },
                                "2": {
                                    "numberquestions": "3",
                                    "description": "Los cuidados para conservar una mezcla instantánea son:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-mezclas/thumbnail-4-1.jpg",
                                            "infoquestions": "a. Dejar en un recipiente.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-mezclas/thumbnail-4-2.jpg",
                                            "infoquestions": "b. Almacenar el producto con cucharas húmedas.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "2": {
                                            "description": "modulo-mezclas/thumbnail-4-3.jpg",
                                            "infoquestions": "c. Almacenar el producto al lado de jabones de limpieza.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-mezclas/thumbnail-4-4.jpg",
                                            "infoquestions": "d. Conservar el producto dentro del empaque original y en un recipiente hermético.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        }
                                    }
                                },
                            }
                        }
                    }
                }
            },
            {
                // MODULO CEN
                "index": "5",
                "name": "Cápsulas Centto",
                "unique": "capsulas-express",
                "time": "4h",
                "content": {
                    "0": {
                        "index": "6.1",
                        "name": "Introducción Centto",
                        "image": "modulo1-1.png",
                        "time": "00:33",
                        "type": "1",
                        "content": "introduccion-cen.mp4"
                    },
                    "1": {
                        "index": "6.2",
                        "name": "Definición",
                        "image": "modulo1-2.png",
                        "time": "04:30",
                        "type": "3",
                        "content": [
                            "modulo-cen/infografico-1.png",
                            "modulo-cen/infografico-2.png"
                        ]
                    },
                    "2": {
                        "index": "6.3",
                        "name": "Cápsulas Centto",
                        "image": "modulocen-1.jpg",
                        "time": "07:00",
                        "type": "10",
                        "content": {
                            "0": {
                                "type": "10-c",
                                "source": [
                                    "modulo-cen/infografico-3.png",
                                    "modulo-cen/infografico-4.png",
                                    "modulo-cen/infografico-5.png",
                                    "modulo-cen/infografico-6.png",
                                    "modulo-cen/infografico-7.png",
                                    /* "modulo-cen/infografico-8.png" */
                                ]
                            }
                        }
                    },
                    "3": {
                        "index": "6.4",
                        "name": "Funcionamiento del Sistema",
                        "image": "modulo1-2.png",
                        "time": "04:30",
                        "type": "4",
                        "content": {
                          "title": "CÁPSULAS Centto",
                          "thumbnail": "modulo-1/thumbnail-video-0.jpg",
                          "playlist": {
                            //   "0": {
                            //       "title": "Cápsula Centto",
                            //       "branch": "nuestras-marcas-cen-small.png",
                            //       "thumbnail": "funcionamiento-sistema-1.jpg",
                            //       "video": "intro_modulo_cen.mp4"
                            //   },
                            //   "1": {
                            //       "title": "máquina Eva",
                            //       "branch": "nuestras-marcas-cen-small.png",
                            //       "thumbnail": "funcionamiento-sistema-2.jpg",
                            //       "video": "maquina_eva.mp4"
                            //   },

                                "0": {
                                    "title": "Primer uso Máquina Centto",
                                    "branch": "nuestras-marcas-cen-small.png",
                                    "thumbnail": "maquina-centto-primer-uso.webp",
                                    "video": "Maquina-Centto-Primer-uso.mp4"
                                },
                                "1": {
                                    "title": "Limpieza general Máquina Centto",
                                    "branch": "nuestras-marcas-cen-small.png",
                                    "thumbnail": "maquina-centto-limpieza-general.webp",
                                    "video": "Limpieza-maquina.mp4"
                                },
                                "2": {
                                    "title": "Mantenimiento luz roja intermitente",
                                    "branch": "nuestras-marcas-cen-small.png",
                                    "thumbnail": "maquina-centto-luz-roja-intermitente.webp",
                                    "video": "Boton-rojo.mp4"
                                },
                                "3": {
                                    "title": "Limpieza por descalcificación",
                                    "branch": "nuestras-marcas-cen-small.png",
                                    "thumbnail": "maquina-centto-limpieza-por-descalcificacion.webp",
                                    "video": "Limpieza-descalcificacion.mp4"
                                }, 
                                "4": {
                                    "title": "Máquina Vera",
                                    "branch": "nuestras-marcas-cen-small.png",
                                    "thumbnail": "maquina-vera.png",
                                    "video": "maquina-vera.mp4"
                                },
                          }
                         },
                    },
                    "4": {
                        "index": "6.5",
                        "name": "Preparación",
                        "image": "modulo3-2.png",
                        "time": "02:55",
                        "type": "4",
                        "content": {
                            "title": "Cápsulas Centto",
                            "thumbnail": "modulo-1/thumbnail-video-0.jpg",
                            "playlist": {
                                // "0": {
                                //     "title": "Café Espresso",
                                //     "branch": "nuestras-marcas-cen-small.png",
                                //     "thumbnail": "preparacion-boton-01.png",
                                //     "video": "preparacion-espresso-capsulas.mp4"
                                // },
                                // "1": {
                                //     "title": "Capuccinos y Chocolates",
                                //     "branch": "nuestras-marcas-cen-small.png",
                                //     "thumbnail": "preparacion-boton-02.png",
                                //     "video": "preparacion-capuccinos-chocolates.mp4"
                                // },
                                "0": {
                                    "title": "Preparación de bebidas Máquina Centto",
                                    "branch": "nuestras-marcas-cen-small.png",
                                    "thumbnail": "maquina-centto-preparacion-de-bebidas.webp",
                                    "video": "Preparacion-bebidas.mp4"
                                },
                             
                               
                                
                            }
                        }
                    },
                    "5": {
                        "index": "6.6",
                        "name": "Recetas",
                        "image": "modulo3-2.png",
                        "time": "02:55",
                        "type": "4",
                        "content": {
                            "title": "Cápsulas Centto",
                            "thumbnail": "modulo-1/thumbnail-video-0.jpg",
                            "playlist": {
                                "0": {
                                    "title": "Café Martini",
                                    "branch": "nuestras-marcas-cen-small.png",
                                    "thumbnail": "capsulas-mocca-frio.png",
                                    "video": "capsulas-mocca-frio.mp4"
                                },
                                "1": {
                                    "title": "Pannacotta de Café",
                                    "branch": "nuestras-marcas-cen-small.png",
                                    "thumbnail": "capsulas-capuccino-nueces.png",
                                    "video": "capsulas-capuccino-nueces.mp4"
                                },
                                "2": {
                                    "title": "Café Latte",
                                    "branch": "nuestras-marcas-cen-small.png",
                                    "thumbnail": "capsulas-frutos-rojo.png",
                                    "video": "capsulas-frutos-rojos.mp4"
                                }
                            }
                        }
                    },
                    "6": {
                        "index": "6.7",
                        "name": "Prueba tu conocimiento",
                        "titlemodulo": "Ejercicio de conocimiento Cápsulas Centto",
                        "image": "modulocen-2.jpg",
                        "time": "11:00",
                        "type": "2",
                        "content": {
                            "type": "a",
                            "intro" : true,
                            "template" : "intro-cen",
                            "questions": {
                                "0": {
                                    "numberquestions": "1",
                                    "description": "El sistema de Cápsulas Centto te permite preparar:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-cen/thumbnail-1-1.jpg",
                                            "infoquestions": "a. Solamente cafés filtrados.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-cen/thumbnail-1-2.jpg",
                                            "infoquestions": "b. Solamente cappuccinos.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "2": {
                                            "description": "modulo-cen/thumbnail-1-3.jpg",
                                            "infoquestions": "c. Cafés filtrados, espressos, cappuccinos y chocolates.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "3": {
                                            "description": "modulo-cen/thumbnail-1-4.jpg",
                                            "infoquestions": "d. Ninguna de las anteriores.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "1": {
                                    "numberquestions": "2",
                                    "description": "El sistema de Cápsulas Centto, se demora en preparar una bebida:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-cen/thumbnail-2-1.jpg",
                                            "infoquestions": "a. 1 hora.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-cen/thumbnail-2-2.jpg",
                                            "infoquestions": "b. 1/2 hora.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "2": {
                                            "description": "modulo-cen/thumbnail-2-3.jpg",
                                            "infoquestions": "c. 30 segundos.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "3": {
                                            "description": "modulo-cen/thumbnail-2-4.jpg",
                                            "infoquestions": "d. Ninguna de las anteriores.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "2": {
                                    "numberquestions": "3",
                                    "description": "Define si la siguiente afirmación es falsa o verdadera: El sistema de Cápsulas Centto es difícil de utilizar porque se necesitan ollas, filtros, y estufa para preparar las bebidas.",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-cen/thumbnail-3-1.jpg",
                                            "infoquestions": "a. Falso.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "1": {
                                            "description": "modulo-cen/thumbnail-3-2.jpg",
                                            "infoquestions": "b. Verdadero.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                                "3": {
                                    "numberquestions": "4",
                                    "description": "La máquina de cápsulas tiene 3 botones en la parte frontal para:",
                                    "answers": {
                                        "0": {
                                            "description": "modulo-cen/thumbnail-4-1.jpg",
                                            "infoquestions": "a. Adornar la maquina.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "1": {
                                            "description": "modulo-cen/thumbnail-4-2.jpg",
                                            "infoquestions": "b. Elegir el botón adecuado para preparar correctamente la bebida.",
                                            "correct": true,
                                            "feedback": "Correcta"
                                        },
                                        "2": {
                                            "description": "modulo-cen/thumbnail-4-3.jpg",
                                            "infoquestions": "c. Dar la hora.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        },
                                        "3": {
                                            "description": "modulo-cen/thumbnail-4-4.jpg",
                                            "infoquestions": "d. Todas la anteriores.",
                                            "correct": false,
                                            "feedback": "Tu respuesta ha sido incorrecta, inténtalo de nuevo para poder continuar con la evaluación"
                                        }
                                    }
                                },
                            }
                        }
                    }
                }
            }];
        },
        machines: function() {
            return [{
                "id": 1,
                "name": "Cafetera Institucional por Goteo",
                "img": "images/equipos/cafetera-institucional.png",
                "type": 1,
                "slug":"Goteo",
                "color": "yellow",
                "drinks": [{
                    "id": 1
                }, {
                    "id": 2
                },{
                    "id": 3,
                    "brands":[{
                        "id":4,
                        "divider":180,
                        "insumos": [{
                            "id": 1
                        }, {
                            "id": 2
                        },{
                            "id": 3
                        },{
                            "id": 5
                        }],
                        "tostions":[{
                            "id":19
                            },{
                            "id":14
                            }]
                        }]
                }],
                "brands": [{
                    "id": 1,
                    "tostions":[{
                        "id":2
                        },
                        {
                        "id":3
                        }]
                }, {
                    "id": 2,
                    "tostions":[{
                        "id":1
                        },
                        {
                        "id":2
                        },
                        {
                        "id":3
                        }]
                },{
                    "id": 3,
                    "tostions":[{
                        "id":4
                        },
                        {
                        "id":5
                        },
                        {
                        "id":6
                        },
                        {
                        "id":7
                        }]
                }],
                "glasses": [{
                    "id": 1
                }, {
                    "id": 2
                },{
                    "id": 3
                }],
                "insumos": [{
                    "id": 1
                }, {
                    "id": 2
                },{
                    "id": 3
                },{
                    "id": 4
                }]
            }, {
                "id": 2,
                "name": "Greca",
                "img": "images/equipos/graca.png",
                "type": 1,
                "slug":"Greca",
                "color": "green",
                "drinks": [{
                    "id": 1
                }, {
                    "id": 2
                },{
                    "id": 3,
                    "brands":[{
                        "id":4,
                        "tostions":[{
                            "id":13
                            },{
                            "id":14
                            }]
                        }]
                }],
                "brands": [{
                    "id": 1,
                    "tostions":[{
                        "id":2
                        },
                        {
                        "id":3
                        }]
                }, {
                    "id": 2,
                    "tostions":[{
                        "id":1
                        },
                        {
                        "id":2
                        },
                        {
                        "id":3
                        }]
                }],
                "glasses": [{
                    "id": 1
                }, {
                    "id": 2
                },{
                    "id": 3
                }],
                "insumos": [{
                    "id": 1
                }, {
                    "id": 2
                },{
                    "id": 3
                },{
                    "id": 4
                }]
            }, {
                "id": 3,
                "name": "Máquina Espresso con Molino",
                "img": "images/equipos/maquina-espresso-molido.png",
                "type": 2,
                "slug":"Espresso molino",
                "color": "coffe",
                "drinks": [{
                    "id": 4,
                    "glasses":null,
                    "express":[{
                        "id":4
                        },{
                        "id":5
                        }]
                }, {
                    "id": 5,
                    "express":[{
                        "id":4
                        },{
                        "id":5
                        }]
                },{
                    "id": 2,
                    "express":[{
                        "id":4
                        },{
                        "id":5
                        }]
                },{
                    "id": 3,
                    "brands": [{
                    "id": 1,
                    "insumos": [{
                            "id": 1
                        }, {
                            "id": 2
                        },{
                            "id": 5
                        }],
                    "tostions":[{
                        "id":8
                        }]
                }, {
                    "id": 3,
                    "insumos": [{
                            "id": 1
                        }, {
                            "id": 2
                        },{
                            "id": 5
                        }],
                    "tostions":[{
                        "id":9
                        },
                        {
                        "id":10
                        }]
                }],
                    "express":[{
                        "id":4
                        },{
                        "id":5
                        }]
                }],
                "brands": [{
                    "id": 8,
                    "tostions":[{
                        "id":8
                        }]
                }, {
                    "id": 3,
                    "tostions":[{
                        "id":9
                        },
                        {
                        "id":10
                        }]
                }],
                "glasses": [{
                    "id": 1
                }, {
                    "id": 2
                }, {
                    "id": 3
                }],
                "insumos": [{
                    "id": 1
                }, {
                    "id": 2
                },{
                    "id": 3
                },{
                    "id": 4
                }]
            }, {
                "id": 4,
                "name": "Máquina Espresso sin Molino",
                "img": "images/equipos/maquina-espresso.png",
                "type": 2,
                "slug":"Espreso sin molino",
                "color": "green",
                "drinks": [{
                    "id": 4,
                    "glasses":null,
                    "express":[{
                        "id":4
                        },{
                        "id":5
                        }]
                }, {
                    "id": 5,
                    "express":[{
                        "id":4
                        },{
                        "id":5
                        }]
                },{
                    "id": 2,
                    "express":[{
                        "id":4
                        },{
                        "id":5
                        }]
                },{
                    "id": 3,
                    "brands": [{
                        "id": 1,
                        "insumos": [{
                            "id": 1
                        }, {
                            "id": 2
                        },{
                            "id": 5
                        }],
                        "tostions":[{
                            "id":8
                            }]
                    }],
                    "express":[{
                        "id":4
                        },{
                        "id":5
                        }]
                }],
                "brands": [{
                    "id": 1,
                    "tostions":[{
                        "id":8
                        }]
                },{
                    "id": 3,
                    "tostions":[{
                        "id":18
                        }]
                }],
                "glasses": [{
                    "id": 1
                }, {
                    "id": 2
                }, {
                    "id": 3
                }],
                "insumos": [{
                    "id": 1
                }, {
                    "id": 2
                },{
                    "id": 3
                },{
                    "id": 4
                }]
            }, {
                "id": 5,
                "name": "Dispensador de Bebidas Frías",
                "img": "images/equipos/dispensador-frio.png",
                "slug":"Bed. Frías",
                "type": 3,
                "color": "coffe",
                "drinks": [{
                    "id": 7,
                    "brands":[{
                        "id":4,
                        "tostions":[{
                            "id":19
                            },{
                            "id":14
                            }]
                        }]
                }],
                "brands": [],
                "glasses": [{
                    "id": 6
                }, {
                    "id": 7
                }, {
                    "id": 8
                }, {
                    "id": 9
                }, {
                    "id": 10
                }],
                "insumos": [{
                    "id": 6
                }, {
                    "id": 2
                }]
            }, {
                "id": 6,
                "name": "Granizadora",
                "img": "images/equipos/granizado.png",
                "type": 3,
                "slug":"Granizadora",
                "color": "yellow",
                "drinks": [{
                    "id":8
                }],
                "brands": [{
                    "id":7,
                    "tostions":[{
                        "id":17
                        }]
                    }],
                "glasses": [{
                    "id": 6
                }, {
                    "id": 7
                }, {
                    "id": 8
                }, {
                    "id": 9
                }, {
                    "id": 10
                }],
                "insumos": [{
                    "id": 6
                }, {
                    "id": 2
                }, {
                    "id": 4
                }]
            }]
        },
        drinks: function() {
            return [{
                "id": 1,
                "name": "Café negro o tinto",
                "img": "images/cafe-negro-tinto.jpg",
                "slug":"Tinto",
                "monthProjection":true,
                "presentation":"images/cafe-venta-publico.jpg"
            }, {
                "id": 2,
                "name": "Café con leche",
                "img": "images/cafe-con-leche.jpg",
                "useMilk": true,
                "slug":"Cafe leche",
                "monthProjection":false,
                "presentation":"images/taza-cafe-con-leche.png"
            }, {
                "id": 3,
                "name": "Cappuccino",
                "img": "images/cafe-cappuccino.jpg",
                "slug":"Cappuccino",
                "monthProjection":false,
                "presentation":"images/taza-cappuccino.png"
            },{
                "id": 4,
                "name": "Espresso",
                "img": "images/espresso.png",
                "slug":"Espresso",
                "monthProjection":true,
                "presentation":"images/taza-espresso.png"
            },{
                "id": 5,
                "name": "Café Americano o Café Negro",
                "img": "images/cafe-americano.png",
                "slug":"Americano",
                "monthProjection":true,
                "presentation":"images/cafe-venta-publico.jpg"
            },{
                "id": 7,
                "name": "Cappuccino Frío",
                "img": "images/colcafe-cafe-cappuccino.png",
                "slug":"Cappuccino frío",
                "monthProjection":false,
                "presentation":"images/taza-cappuccino-frio.png"
            },{
                "id": 8,
                "name": "Granizado de Café",
                "img": "images/granizado-de-cafe.png",
                "slug":"Granizado",
                "monthProjection":false,
                "presentation":"images/taza-granizado.png"
            }]
        },
        brands: function() {
            return [{
                "id": 1,
                "name": "Café Sello Rojo",
                "img": "images/cafe-sello-rojo.jpg",
                "pack": "images/paquete-sello-rojo.png",
                "slug":"Sello Rojo"
            },
            {
                "id": 2,
                "name": "Café La Bastilla",
                "img": "images/cafe-la-bastilla.jpg",
                "pack": "images/cafe-la-bastilla-large.png",
                "slug":"Bastilla"
            },
            {
                "id": 3,
                "name": "Matiz",
                "img": "images/cafe-matiz.jpg",
                "pack": "images/paquete-matiz.png",
                "slug":"Matiz"
            },
            {
                "id": 4,
                "name": "Colcafé Cappuccino",
                "img": "images/colcafe-cappuccino.png",
                "pack": "images/gramaje-colcafe.png",
                "packFrio": "images/gramaje-colcafe.png",
                "slug":"Colcafé Cappuccino"
            },
            {
                "id": 5,
                "name": "Colcafé Café Helado",
                "img": "images/colcafe-cafe-helado.jpg",
                "pack": "images/gramaje-colcafe.png",
                "slug":"Colcafé Café Helado"
            },
            {
                "id": 6,
                "name": "Colcafé Iced Cappuccino",
                "img": "images/colcafe-iced.jpg",
                "pack": "images/gramaje-colcafe.png",
                "slug":"Colcafé Iced Cappuccino"
            },
            {
                "id": 7,
                "name": "Colcafé Granizado",
                "img": "images/colcafe-cappuccino.png",
                "pack": "images/gramaje-colcafe.png",
                "slug":"Colcafé Granizado"
            },
            {
                "id": 8,
                "name": "Café Sello Rojo Espresso",
                "img": "images/cafe-sello-rojo.jpg",
                "pack": "images/gramaje-colcafe.png",
                "slug":"Sello Rojo"
            }]
        },
        tostions: function() {
            return [{
                "id": 1,
                "name": "Tostión clara /<br>café suave",
                "img": "images/tostion-clara.jpg",
                "value": 5,
                "slug": "Tostión clara"
            }, {
                "id": 2,
                "name": "Tostión media /<br>café balanceado",
                "img": "images/tostion-media.jpg",
                "value": 5,
                "slug": "Tostión media"
            }, {
                "id": 3,
                "name": "Tostión oscura /<br>café fuerte",
                "img": "images/tostion-oscura.jpg",
                "value": 5,
                "slug": "Tostión oscura"
            },{
                "id": 4,
                "name": "Marfil",
                "img": "images/matiz-marfil-suave.jpg",
                "value": 7,
                "slug": "Marfil"
            },{
                "id": 5,
                "name": "Ámbar",
                "img": "images/matiz-ambar-balanceado.jpg",
                "value": 7,
                "slug": "Ámbar"
            },{
                "id": 6,
                "name": "Escarlata",
                "img": "images/matiz-escarlata-intenso.jpg",
                "value": 7,
                "slug": "Escarlata"
            },{
                "id": 7,
                "name": "Ébano",
                "img": "images/matiz-ebano.jpg",
                "value": 7,
                "slug": "Ébano"
            },{
                "id": 8,
                "name": "Espresso",
                "img": "images/tostion-espresso.png",
                "value": 26.67,
                "slug": "Espresso"
            },{
                "id": 9,
                "name": "Escarlata<br>en grano",
                "img": "images/matiz-escarlata-intenso.jpg",
                "value": 26.67,
                "slug": "Escarlata grano"
            },{
                "id": 10,
                "name": "Ébano<br>en grano",
                "img": "images/matiz-ebano.jpg",
                "value": 26.67,
                "slug": "Ébano grano"
            },{
                "id": 11,
                "name": "Vainilla",
                "img": "img/tostions/tostion-vainilla.png",
                "value": 10,
                "slug": "Vainilla"
            },{
                "id": 12,
                "name": "Caramelo",
                "img": "img/tostions/tostion-caramelo.png",
                "value":10,
                "slug": "Caramelo"
            },{
                "id": 13,
                "name": "Clásico",
                "img": "images/tostion-cappucino-clasico.png",
                "value": 18,
                "slug": "Clásico"
            },{
                "id": 14,
                "name": "Light",
                "img": "images/tostion-cappucino-light.png",
                "value": 6,
                "slug": "Light"
            },{
                "id": 17,
                "name": "Café",
                "img": "images/colcafe-granizado-cafe.png",
                "value": 31.5,
                "slug": "Café"
            },{
                "id": 18,
                "name": "Ébano<br>Espresso molido",
                "img": "images/matiz-ebano.jpg",
                "value": 26.67,
                "slug": "Ébano Espresso molido"
            },{
                "id": 19,
                "name": "Clásico, Vainilla,<br>Mocca, Caramelo",
                "img": "images/cappuccinos.png",
                "value": 9,
                "slug": "Cappucino varios"
            }]
        },
        glasses: function() {
            return [{
                "id": 1,
                "name": "4 onzas (100 ml)",
                "ml": 100,
                "img": "images/4-onzas.png",
                "slug": "4 onzas"
            }, {
                "id": 2,
                "name": "6-7 onzas (200 ml)",
                "ml": 200,
                "img": "images/6-onzas.png",
                "slug": "6-7 onzas"
            }, {
                "id": 3,
                "name": "8-9 onzas (250 ml)",
                "ml": 250,
                "img": "images/8-onzas.png",
                "slug": "8 onzas"
            }, {
                "id": 4,
                "name": "1 onza (30 ml)",
                "ml": 30,
                "img": "images/ristreto.png",
                "express":true,
                "gramCoffee":8,
                "slug": "Sencillo"
            }, {
                "id": 5,
                "name": "2 onzas (60 ml)",
                "ml": 60,
                "img": "images/doppio.png",
                "express":true,
                "gramCoffee":16,
                "slug": "Doble"
            }, {
                "id": 6,
                "name": "7 onzas (200 ml)",
                "ml": 200,
                "img": "images/t-7-onzas.png",
                "slug": "7 onzas"
            }, {
                "id": 7,
                "name": "8 onzas (240 ml)",
                "ml": 240,
                "img": "images/t-8-onzas.png",
                "slug": "8 onzas"
            }, {
                "id": 8,
                "name": "10 onzas (300 ml)",
                "ml": 300,
                "img": "images/t-10-onzas.png",
                "slug": "10 onzas"
            }, {
                "id": 9,
                "name": "12 onzas (360 ml)",
                "ml": 360,
                "img": "images/t-12-onzas.png",
                "slug": "12 onzas"
            }, {
                "id": 10,
                "name": "16 onzas (480 ml)",
                "ml": 480,
                "img": "images/t-16-onzas.png",
                "slug": "16 onzas"
            }]
        },
        insumos: function() {
            return [{
                "id": 1,
                "name": "Vasos",
                "img": "images/vasos-plastico.png",
                "quantityLabel":"Vasos",
                "quantityPerCoffee":1
            }, {
                "id": 2,
                "name": "Mezcladores de café",
                "img": "images/mezcladores-cafe.png",
                "quantityLabel":"Unidades",
                "quantityPerCoffee":1
            }, {
                "id": 3,
                "name": "Azúcar",
                "img": "images/azucar.png",
                "quantityLabel":"Sobres",
                "quantityPerCoffee":2
            },{
                "id": 4,
                "name": "Agua",
                "img": "images/agua.png",
                "quantityLabel":"Litros",
                "quantityPerCoffee":-1,
                "water":true
            },{
                "id": 5,
                "name": "Leche",
                "img": "images/leche-ico.png",
                "quantityLabel":"Litros",
                "quantityPerCoffee":-1
            },{
                "id": 6,
                "name": "Vasos",
                "img": "images/vasos-plastico-generico.png",
                "quantityLabel":"Vasos",
                "quantityPerCoffee":1
            }]
        }
    };
}]);
