module.exports = function(grunt) { 
    grunt.initConfig({
        //pkg: grunt.file.readJSON('package.json'),
        scorm_manifest: {
            options: {
                version: '1.2',
                courseId: 'MomentosDeCafe-MDC-01',
                SCOtitle: 'Momentos de Cafe',
                moduleTitle: 'MDI101',
                launchPage: 'index.html',
                path: './'
            },
            files: [{
                expand: true,       // required
                cwd: './',          // start looking for files to list in the same dir as Gruntfile
                src: ['**/*.*'],    // file selector (this example includes subdirectories)
                filter: 'isFile'    // required
            }],
        },
        jshint: {
            all: ['app/**/*.js']
        },
        sass: {
            dist: {
                files: {
                    'css/style.css': 'sass/style.scss',
                    'css/style-mobile.css': 'sass/style-mobile.scss'
                }
            }
        },
        watch: {
            css: {
                files: ['sass/**/*.scss'],
                tasks: ['sass']
            },
            js: {
                files: ['js/**.js'],
                tasks: ['jshint']
            }
        },
        nodemon: {
            dev: {
                script: 'server.js'
            }
        },
        concurrent: {
            options: {
                logConcurrentOutput: true
            },
            tasks: ['nodemon', 'watch']
        }
    });

    grunt.loadNpmTasks('grunt-scorm-manifest');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-nodemon');
    grunt.loadNpmTasks('grunt-concurrent');

    grunt.registerTask('default', ['scorm_manifest']);
    grunt.registerTask('default', ['concurrent']);
};
