<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Elearning_Apasionados
 * @subpackage Elearning_Apasionados/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Elearning_Apasionados
 * @subpackage Elearning_Apasionados/admin
 * @author     Cristian Torres <catorres@smdigital.com.co>
 */
class Elearning_Apasionados_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $elearning_apasionados    The ID of this plugin.
	 */
	private $elearning_apasionados;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $elearning_apasionados       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $elearning_apasionados, $version ) {

		$this->elearning_apasionados = $elearning_apasionados;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Elearning_Apasionados_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Elearning_Apasionados_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->elearning_apasionados, plugin_dir_url( __FILE__ ) . 'css/elearning-apasionados-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Elearning_Apasionados_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Elearning_Apasionados_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->elearning_apasionados, plugin_dir_url( __FILE__ ) . 'js/elearning-apasionados-admin.js', array( 'jquery' ), $this->version, false );

	}

}
