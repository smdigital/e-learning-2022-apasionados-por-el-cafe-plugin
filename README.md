
[![SM Digital](https://smdigital.com.co/wp-content/themes/sm-digital/library/images/sm-digital-logo.png)](https://smdigital.com.co/)

# Plugin Apasionados por el cafe 2022

Plugin actualizado en en infraestructura por temas de rendimiento y funciones obsoletas con la anterior versión.

**Colaboradores**

 - **Cristian Torres**: catorres@smdigital.com.co **(Programación)**
 - **Cristian Villada** : ccvillada@smdigital.com.co **(Programación)**

**Versiones**

* Versión Wordpress estable: 5.8.4 
* Versión plugin : 1.0

**Dependencias JS**
- jQuery 3.6.0
- bootstrap.min.js v4.1.3 
- jQuery UI v1.12.1
- Swiper 4.5.0
- Modernizr 2.8.3
- Prefixfree 1.0.3
- Uniform

**Componentes**
- Bower install
- grunt

**Plugins**

### Instalación

Para su instalación, realice los siguientes pasos:

1. Descargue el repositorio `https://bitbucket.org/smdigital/e-learning-2022-apasionados-por-el-cafe-plugin/src/development/` en la ruta **/wp-content/plugins**, con este comando 
**git clone git clone git@bitbucket.org:smdigital/e-learning-2022-apasionados-por-el-cafe-plugin.git --branch development**
2. A continuación active el plugin desde el administrador, en la opción **PLUGINS**
3. En la raiz de la ruta : **/public/resources/assets** , instalar bower: **https://bower.io/*
y todo lo que necesitamos con grunt.
NOTA: Si no te funciona con la instalación de los bower_components por favor agregar en la raíz de : **/public/resources/assets** el comprimido que se encuentra en la raíz del proyecto **bower_components.zip** para su correcto funcionamiento.

### Paginas
 - Inicio de sesión
 - E-learning
 - Registro
### Shortcode
Estos son los shortcodes que se utilizan:
 - **[elearning-content]** 
 - **[elearning-content-mobile]**
 - **[elearning-login-form]**
 - **[elearning-register-form]**

### ¿tiene preguntas?

Recurra a cualquier miembro de los colaboradores listados al inicio de la sección.

 ### Changelog

 - 2.0.0 - Versión inicial plugin actualizado en estructura.